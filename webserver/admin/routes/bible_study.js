var express = require ('express'),
    router  = express.Router();

router.get('/api/bible_study',(req,res)=>{
  res.send("you want to view all bible studies..");
})

router.post('/api/bible_study',(req,res)=>{
  res.send ("you want to create a new bible study..")
});

router.put('/api/bible_study/:id',(req,res)=>{
  res.send("you want to edit the bible study")
});

router.get('/:id',(req,res)=>{
  res.send("you want to delete a bible study here")
})
  module.exports = router;
