/**
 * @api {get} https://apostlite-mongo.herokuapp.com/api/news  Retrieve all News
 * @apiDescription When found, the api will return every news record found in the database
 * @apiName GetNews
 * @apiGroup News
 * @apiVersion 1.0.0
 * @apiSuccess {String} title   The Headline or Title for the news
 * @apiSuccess {String} content The actual content of the news *
 * @apiSuccess {Date}   createdAt The date when the news was created
 * @apiSuccess {Date}   updatedAt The date for the most current update of the news.
 * @apiSuccessExample Example data on success
 * {
 *  message:"success",
 *  {
 *    title     :"Leavers' service",
 *    content   :"You are being encourged to attend the leavers' service this sunday at ccb. come and be blessed.", *
 *    createdAt :"24-04-2018",
 *    updatedAt :"30-30-2018"
 *  },
 *  statusCode :200
 * }
 *
 * @apiError DatabaseError An error occured while fetching the records from the database
 *
 */
router.get('/',function(req,res){
    newsController.getAllNews(function(err,news){
      if(err){
        res.json({
            message:"an error occured while fetcing news",
            error:err,
            status:Boolean(false),
            statusCode:res.statusCode
        })
      }else{
        res.json({
          message:"success",
          news:news,
          statusCode:res.statusCode
        });
      //   res.write(JSON.stringify({
      //     message:"success",
      //     news:news,
      //     statusCode:res.statusCode
      // }));
      }
    });
  });
  /**
   *
   * @api {get} https://apostlite-mongo.herokuapp.com/api/news/limit/last_id  Retrieve  News by pagination
   * @apiDescription When found, the api will return every news record found in the database
   * @apiName GetNewsByPagination
   * @apiGroup News
   * @apiVersion 1.0.0
   * @apiParam (limit) {Number} limit The size of search result. e.g a limit of 10 will only return 10 results from the database.
   * @apiParam (last_id) {Object_Id} last_id  The id of the last item in the previous search result. e.g if you are at page 2 and need to go to page 3,
   * then the last_id is the id of the last item in page 2.
   * This parameter is used to help the database ignore/skip items without having to loop through the previous items.
   * @apiSuccess {String} title   The Headline or Title for the news
   * @apiSuccess {String} content The actual content of the news *
   * @apiSuccess {Date}   createdAt The date when the news was created
   * @apiSuccess {Date}   updatedAt The date for the most current update of the news.
   * @apiSuccessExample Example data on success
   * {
   *  {
   *    title:"Leavers' service",
   *    content :"You are being encourged to attend the leavers' service this sunday at ccb. come and be blessed.", *
   *    createdAt:"24-04-2018",
   *    updatedAt:"30-30-2018"
   *  }
   * }
   *
   * @apiError DatabaseError An error occured while fetching the records from the database
   *
   */
  router.get('/:limit/:last_id',(req,res)=>{
    newsController.getNewsPagination(parseInt(req.params.limit),req.params.last_id).then(data=>{
      res.json({
        'status':Boolean(true),
        'message':"success",
        'news':data
      })
    }).catch(err=>{
      res.json({
        'message':"an error occured while fetcing news",
        'error':err,
        'status':Boolean(false)
      })
    });
  });

  /**
   * @api {post} https://apostlite-mongo.herokuapp.com/api/news  Create News
   * @apiName CreateNews
   * @apiGroup News
   * @apiVersion 1.0.0
   * @apiSuccess {object} bible_Study The News to create
   * @apiParamExample  {json} Sample-Request:
   *
   *  {
   *    title :"SCC SUNDAY",
   *
   *   content :"Coming sunday is SCC sunday.There will be no service at CCB.Thank you",
   *
   *  }
   *
   *
   * @apiDescription Do not provide Date with the request. Date is provided by
   * default by the api.
   *
   */
  router.post('/', (req,res)=>{
    let news =req.body;
    newsController.createNews(news,(err,cb)=>{
        if(err){
          res.json({
            'message':"could not create news",
            'error':err,
            'status':Boolean(false)
          })
        }else{
          res.json({
            'message':"success! news has been created",
            'status':Boolean(true)
          })
        }
    });
  });

  /**
   * @api {put} https://apostlite-mongo.herokuapp.com/api/news/{id} Edit News
   * @apiName EditNews
   * @apiGroup News
   * @apiVersion 1.0.0
   * @apiParam (News) {Number} id The News' unique id
   * @apiDescription To update a News, please provide the new bible_study in a json
   * object and parse it to the api.
   * @apiParam (News) {object} bible_study - The new news to be replaced
   * @apiParamExample Example Data The new news
   *
   *{
   *
   *    title :"Something to describe the Bible study",
   *    content:"Walking in a spirit filled life. Life that will cause you to impact in the lives of other. That is what the bible teaches and encourages.",
   *
   *  }
   *
   */
  router.put('/:id',(req,res)=>{
    newsController.updateNews(req.params.id,req.body.news,(err,cb)=>{
      if(err){
        res.json({
          'message':"an error occured while updating the news",
          'error':err,
          'status':Boolean(false)
        });
      }else{
        res.json({
          'message': "success",
          'status':Boolean(true)
        });
      }
    })
  });


  /**
   * @api {delete} https://apostlite-mongo.herokuapp.com/api/news/{id} Delete News
   * @apiName DeleteNews
   * @apiGroup News
   * @apiVersion 1.0.0
   * @apiDescription To delete an object, you need to provide the unique id of the object.
   * @apiSampleRequestExample /api/news/df45adghs5h14
   * @apiSuccess {String} message Returns success if the delete was successful
   * @apiSuccess {Boolean} status Returns true if the delete was successful.
   * @apiSuccessExample {json} success response
   * {
   *      message:"success",
   *      status: "true"
   * }
   * @apiError Error An error occured while adding the News.
   * @apiErrorExample {object} Error-Response: an error occured while deleting the news.
   *
   * {
   *    message : "an error occured while adding the news",
   *    error   : "the type  of error that occured",
   *    status  : "false"
   * }
   */
  router.delete ('/:id',(req,res)=>{
     newsController.deleteNews(req.params.id,(err,cb)=>{
       if(err){
         res.json({
           'message':"an error occured while removing the news",
           'error' :err,
           'status':Boolean(false)
         });
       }else{
         res.json({
           'message':"success",
            'status':cb
         });
       }
     })
  });

/**
 * @api {get} https://apostlite-mongo.herokuapp.com/api/v1/programs/{id}  Retrieve  program information by Id
 * @apiDescription Get information for only one program using the bible_study id.This routes will always return one object if found
 * else it will return an object of program (i.e one object containing several program).
 * @apiName GetNews
 * @apiGroup Program
 * @apiVersion 1.0.0
 * @apiError DatabaseError An error occured while fetching the records from the database
 */

 /**
 * @api {get} https://apostlite-mongo.herokuapp.com/api/v1/programs/{id}  Retrieve  program information by Id
 * @apiDescription Get information for only one program using the bible_study id.This routes will always return one object if found
 * else it will return an object of program (i.e one object containing several program).
 * @apiName GetNews
 * @apiGroup Program
 * @apiVersion 1.0.0
 * @apiError DatabaseError An error occured while fetching the records from the database
 */
router.get('/v1/programs/:id', (req, res) => {
  const query = req.query;

  const options = {
    id: req.params.id,
    includeOwner: false
  };

  if (query.include && query.include != '' && query.include != null) {
    if (query.include.toLowerCase() == 'owner') {
      options.includeOwner = true;
    }
  }

  programController.getProgramById(options, (err, program) => {
    if (err) {
      res.json({
        message: 'an error occured while updating the program' + err,
        error: err,
        status: Boolean(false),
      });
    } else {
      res.json({
        message: 'success',
        status: Boolean(true),
        program: program,
      });
    }
  });
});
