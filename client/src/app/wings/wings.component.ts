import { Component, OnInit } from '@angular/core';
import{WingsService} from '../wings.service';
import {Wing} from  '../wings'

@Component({
  selector: 'app-wings',
  templateUrl: './wings.component.html',
  styleUrls: ['./wings.component.css'],
  providers : [WingsService]
})

export class WingsComponent implements OnInit { 
  constructor(private wingService : WingsService) { }
  public _wings;

  ngOnInit() {    
    this.getWings();   
  }

  getWings(){
    this.wingService.getWings().subscribe(data =>
      {        
        this._wings=data['wings'];
        // console.log(JSON.stringify(data))
      },
      err=>console.log(err),
      ()=>console.log("done loading...")
    )
 }
}
