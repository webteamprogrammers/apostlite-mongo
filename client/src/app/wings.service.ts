import { Injectable } from '@angular/core';
import {Headers} from '@angular/http';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {Wing} from './wings';
import 'rxjs/add/operator/map';


@Injectable()
export class WingsService {
  constructor( private http : HttpClient) { }

  //retriving the wings 
  getWings(){    
    return this.http.get('http://localhost:7000/api/wing');       
  }

  //add wings 
  addWings(newWing){
    var headers  =  new  HttpHeaders();
    headers.append('Content-Type','application/json')
    return this.http.post('http://localhost:7000/api/wing', newWing,{headers:headers});   
  }

  //delete a wing
  deleteWing(id){
    return this.http.delete('http://localhost:7000/api/wing/'+id);     
  }    

  //create a wing
  createWing(){
    
  }
}
