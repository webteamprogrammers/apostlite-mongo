
import { NgModule } from '@angular/core';
import { BrowserModule, platformBrowser } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { WingsComponent } from './wings/wings.component';
import {HttpClientModule} from '@angular/common/http';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SermonComponent } from './sermon/sermon.component';


@NgModule({
  declarations: [
    AppComponent,
    WingsComponent,
    SermonComponent
    
  ],
  imports: [
    BrowserModule,   
    HttpClientModule
  ],
  providers: [
    AppComponent,
    WingsComponent,
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
// platformBrowser().bootstrapModule(AppModule)
