
export class Wing{
    _id?:string;
    name:string;
    slogan:string;
    description:string;
    logo:string;
    created:Date;
    updatedAt:Date
}
