'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const config = require('./api/config/config');
const cors = require('cors');
const path = require('path');
require('./api/workers/worker');
// const basicAuth = require('express-basic-auth');

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
require('./api/router')(app);
require('./api/config/db')(config);

// seed the database;
require('./api/utils/seed');

app.use(express.static(path.join(__dirname, '/static')));
global._basedir = __dirname;

app.listen(process.env.PORT || '7000', () => {
  console.log('Apostlite-mongo server has started on  7000...');
});

