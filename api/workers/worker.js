'use strict';
const News = require('../models/news');
const notify = require('../controllers/fcm/fcm');
let dueTime = 60 * 1000; // 60 seconds * scale factor = 1 minute

function init() {
  setInterval(() => {
    runScheduler();
  }, dueTime);
}

function runScheduler() {
  findPendingNews().then((news) => {
    // check if we can deliver the news
    for (const newsItem in news) {
      if (canDeliverNews(news[newsItem])) {
        news[newsItem].deliveryStatus = '1';
        news[newsItem].save();
        // notify the clients
        notify('News added', news[newsItem].title, 'news');
      }
    }
  });
}

function findPendingNews() {
  return new Promise((resolve, reject) => {
    News.find({deliveryStatus: '0'})
      .then((news) => {
        resolve(news);
      })
      .catch((err) => {
        reject((err));
      });
  });
}

function canDeliverNews(news) {
  const date = new Date();
  if (news.deliveryDate - date <= 0) { // it's time to send the mesage
    return true;
  } else {
    const newTime = news.deliveryDate.getTime();
    // check if the scheduller should be made to run earlier
    if (newTime < dueTime) {
      setDueTime(newTime);
    }
    return false;
  }
}

function setDueTime(newtime) {
  dueTime = newtime;
  runScheduler();
}

const workerObj = {
  'init': init(),
  'setDueTime': setDueTime()
};

module.exports = workerObj;
