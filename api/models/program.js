'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const wing = require('./wing');

const programSchema = new Schema({
  name: {
    type: String,
  },

  banner: {
    type: String,
  },

  theme: {
    type: String,
  },

  start_date: {
    type: Date,
  },

  end_date: {
    type: Date,
  },

  owner: {
    required: false,
    type: String,
    // type: mongoose.Schema.Types.ObjectId,
    ref: 'Wing',
    // consider changing owner to chruch
    default: 'church',
  },

  createdAt: {
    type: Date,
    default: Date.now(),
  },

  updatedAt: {
    type: Date,
  },
});

programSchema.statics.isExisting = function(program, cb) {
  if (program == null) {
    return cb('program is empty or undefined', null, null);
  }
  this.findOne(
    {
      $and: [
        {name: new RegExp(program.name, 'i', 'g')},
        {theme: new RegExp(program.theme, 'i', 'g')},
      ],
    },
    function(err, found) {
      if (err) {
        cb(err, null, true);
      } else if (found && found != null) {
        cb(null, found, true);
      } else {
        cb(null, null, false);
      }
    }
  );
};

const Program = (module.exports = mongoose.model('Program', programSchema));

// Program.deleteMany({name: 'Freshers Orientation'})
//   .then(() => {
//     console.log('removed programs');
//   });

// wing.create({
//   name: 'Webteam',
//   slogan: 'testing',
//   description: 'testiing',
//   logo: ''
// }, (err, created) => {
//   if (!err && created) {
//   // create a program here
//     Program.create({
//       name: 'Freshers Orientation',
//       banner: 'http://uncyclopedia.wikia.com/wiki/File:Bible.png',
//       theme: 'All freshers are  entreated to come for this oreintation as it would help them to get more info about their stay here.',
//       start_date: Date.now(),
//       end_date: Date.now() + 1000,
//       createdAt: Date.now(),
//       updatedAt: Date.now(),
//       owner: created._id,
//     }, (err, created) => {
//       if (!err && created) {
//         console.log('we have created a program');
//       }
//     });
//   }
// });

