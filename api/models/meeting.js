'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const meetingSchema = new Schema({
  day: {
    type: String
  },

  venue: {
    type: String
  },

  time: {
    type: String
  },

  wing_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Wing'
  }
});

module.exports = mongoose.model('Meeting', meetingSchema);
