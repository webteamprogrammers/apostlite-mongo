'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

const userSchema = new Schema({
  email: {
    type: String,
  },

  password: {
    type: String,
  },

  currentToken: {
    type: String,
  },

  resetToken: {
    type: String,
  },
  createdAt: {
    type: Date,
  },
  updatedAt: {
    type: Date,
  },
});

// assign a function to the "statics" object of our animalSchema
userSchema.statics.findByName = function(name, cb) {
  return this.find(
    {
      $or: [
        {firstName: new RegExp(name, 'i')},
        {lastName: new RegExp(name, 'i')},
      ],
    },
    cb
  );
  // return this.find({ firstName: new RegExp(name, 'i') $or lasttName: new RegExp(name, 'i') }, cb);
};
//
// let user ={

//    'email':"eakbo23@gmail.com",
//    'password': "eben123"
//  }

// userSchema.methods.validPassword=function(password,callback){
//   bcrypt.compare(password,this.password,function (err,same){
//      if (err){
//          return callback (err,null);
//      }else if(same){
//        //the passwords match
//        return callback(null,same);
//      }else {
//        return callback(null,false);
//      }
//    });
//  }

const getHashPass = password => {
  return new Promise((resolve, reject) => {
    const salt_rounds = 10;
    bcrypt.genSalt(salt_rounds, (err, salt) => {
      if (!err) {
        bcrypt.hash(password, salt, (err, hash) => {
          if (err) reject(err);
          else resolve(hash);
        });
      }
    });
  });
};

userSchema.pre('save', function(next) {
  const user = this;
  if (!user.isModified('password')) {
    return next();
  }
  getHashPass(user.password)
    .then(hash => {
      user.password = hash;
      return next();
    })
    .catch(err => {
      return next();
    });
});

const User = mongoose.model('User', userSchema);
// User.create(user,(err,created)=>{
//   if(created){
//     console.log("We have created a new user.")
//   }
// })

module.exports = User;
