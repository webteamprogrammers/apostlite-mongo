'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bible_study = require('./study');

const bibleStudyBookSchema = new Schema({
  title: {
    type: String
  },

  scripture: {
    type: String
  },

  semester: {
    type: String
  },

  academic_year: {
    type: String
  },

  'studies': [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Study'
    }
  ]
});

bibleStudyBookSchema.statics.isExisting = function(bibleStudyBook, cb) {
  // check if bible study is existing or not
  if (bibleStudyBook == null) {
    return cb('bible study book is empty or undefined', null, null);
  }
  this.findOne(
    {
      $and: [
        {title: new RegExp(bibleStudyBook.title, 'i', 'g')},
        {academic_year: new RegExp(bibleStudyBook.academic_year, 'i', 'g')},
      ],
    },
    function(err, found) {
      if (err) {
        cb(err, null, true);
      } else if (found && found != null) {
        cb(null, found, true);
      } else {
        cb(null, null, false);
      }
    }
  );
};

const bb = module.exports = mongoose
  .model('Book', bibleStudyBookSchema);

const book = {
  title: 'New Test Book 2',
  scripture: 'Revelations 1 v 5',
  academic_year: '2018/2019',
  semester: '2018/2019',

};

const study = bible_study.create({
  title: 'In His vine yard 2',
  study_number: 4,
  content: 'This is a bible study',
  questions: ['john 4v3', 'Luke 4v5', 'Mathew 5v4'],
  memory_verse: 'Luke 2v13',
  objective: 'To be Like christ',
})
  .then((data) =>{
    bb.isExisting(book, (err, found, cb) => {
      if (!cb) {
        bb.create(book, (err, created) => {
          if (created) {
            created.studies.push(data);
            created.save();
            data.book_id = created._id;
            data.save();
          }
        });
      }
    });
  });

