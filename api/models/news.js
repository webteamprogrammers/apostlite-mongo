'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const newsSchema = new Schema({
  title: {
    type: String,
    required: true,
  },

  content: {
    type: String,
  },

  createdAt: {
    type: Date,
    default: Date.now(),
  },

  updatedAt: {
    type: Date,
  },

  // this tells whether the item has been deleted from the database or not 0 = deleted, 1 = active
  status: {
    type: String,
    default: 1
  },

  // this tell whether the news item has been delivered or not 0 = pending; 1 = delivered
  deliveryStatus: {
    type: String,
    default: 1
  },

  deliveryTime: {
    type: String,
  },

  deliveryDate: {
    type: Date
  }
});

newsSchema.statics.isExisting = function(news, cb) {
  if (news == null) {
    return cb('news is empty or undefined', null, null);
  }
  this.findOne(
    {
      $and: [
        {title: new RegExp(news.title, 'i', 'g')},
        {content: new RegExp(news.content, 'i', 'g')},
      ],
    },
    function(err, found) {
      if (err) {
        cb(err, null, true);
      } else if (found && found != null) {
        cb(null, found, true);
      } else {
        cb(null, null, false);
      }
    }
  );
};

const News = (module.exports = mongoose.model('News', newsSchema));
News.aggregate([
  {
    /* group by year and month of the subscription event */
    $group: {
      _id: {
        month: {
          $month: '$createdAt'
        }
      },
    }
  },
]);
// News.find()
//   .then((data) => {
//     for (const d in data) {
//       data[d].deliveryStatus = '1';
//       data[d].status = '1';
//       data[d].save();
//     }
//   });

// newsSchema.statics.isExisting  =(news,callback)=>{
//   if(news==null){
//     console.log("we are returning cuz news is empty...")
//     return callback("news is empty or undefined",null,null);
//   }

//   News.findOne({
//      $and: [{ title: new RegExp(news.title, 'i','g')}, {content: new RegExp(news.content, 'i','g')}]
//   },function(err,found){
//     if(err){
//       callback(err,null,true)
//     }else if(found && found!==null){
//       console.log("in news.It is existing..."+JSON.stringify(found))
//       callback(null,found,true)
//     }else{
//       console.log("in else")
//       callback(null,null,false)
//     }
//   });
// }
