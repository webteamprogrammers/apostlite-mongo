'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bible_studySchema = new Schema({

  title: {
    type: String,
  },

  study_number: {
    type: Number,
  },

  scriptures: [String],

  content: {
    type: String,
  },

  questions: [String],

  memory_verse: {
    type: String,
  },

  objective: {
    type: String
  },

  book_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Book'
  },

  createdAt: {
    type: Date,
    default: Date.now(),
  },

  updatedAt: {
    type: Date,
  },
});

bible_studySchema.statics.isExisting = function(bible_study, cb) {
  if (bible_study == null) {
    console.log('empty return');
    return cb('bible_study is empty or undefined', null, null);
  }
  this.findOne(
    {
      $and: [
        {study_number: bible_study.study_number},
        {title: new RegExp(bible_study.title, 'i', 'g')},
        {content: new RegExp(bible_study.content, 'i', 'g')},
      ],
    },
    function(err, found) {
      if (err) {
        cb(err, null, true);
      } else if (found && found != null) {
        cb(null, found, true);
      } else {
        cb(null, null, false);
      }
    }
  );
};

const Bible_Study = mongoose.model('Study', bible_studySchema);
// Bible_Study.aggregate([
//   {$group: {month: {$month: '$createdAt'}}}
// ]);
module.exports = Bible_Study;

// let bible_study = {
//     study_number :1,
//     scriptures :[
//       "Genesis 1v 2-10", "Mathew 5 v8", "1 John 3 -9"
//     ],
//     title:"The Master Brewer",
//     content : "We shall populate this with the actual content of the study. It could as well be a summary though.",
//     questions:[
//       "question one?", "question two?", "question three?"
//     ],
//     createdAt:Date.now(),
// }

// Bible_Study.isExisting(bible_study,(err,found,cb)=>{
//   if(!cb){
//     Bible_Study.create(bible_study,(err,created)=>{
//       if(created){
//         console.log("we have created a bible_study")
//       }
//     })
//   }
// })
