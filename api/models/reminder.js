'use strict';

const mongoose = require('mongoose');
const Schema = new mongoose.Schema;

const reminderSchema = new Schema({
  title: {
    type: String
  },

  date_due: {
    type: String
  },
});

const reminder = {
  title: 'Test reminder',
};

module.expports = mongoose.model('Reminder', reminderSchema);
