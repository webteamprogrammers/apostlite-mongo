'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const Faker = require('faker');
const patronSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: false,
  },
  phone: {
    type: String,
    required: false
  }
});

const Patron = mongoose.model('Patron', patronSchema);
// const newPatron = {
//   name: Faker.name.firstName(),
//   phone: Faker.phone.phoneNumber(),
//   email: Faker.internet.email(),
// };

// Patron.create(newPatron)
//   .then(() => {
//     console.log('we created a new patron');
//   });
module.exports = Patron;
