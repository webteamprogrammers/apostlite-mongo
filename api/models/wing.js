'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const wingController = require('../controllers/wings');
const wingSchema = new Schema({
  name: {
    type: String,
  },

  email: {
    type: String
  },

  slogan: {
    type: String,
  },

  // meeting_venue: {
  //   type: String
  // },

  // meeting_day: {
  //   type: String
  // },

  // meeting_time: {
  //   type: String
  // },

  meetings: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Meeting',
      default: [{
        day: '',
        time: '00:00',
        venue: '',
      }]
    }
  ],

  description: {
    type: String,
  },

  logo: {
    type: String,
  },

  executives: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Executive',
      default: []
    }
  ],

  programs: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Program',
      default: []
    }
  ],

  createdAt: {
    type: Date,
    default: Date.now(),
  },

  updatedAt: {
    type: Date,
  },

  status: {
    type: Number
  }
});

wingSchema.statics.isExisting = function(wing, cb) {
  if (wing == null) {
    console.log('empty return');
    return cb('wing is empty or undefined', null, null);
  }
  this.findOne(
    {
      $and: [
        {slogan: new RegExp(wing.slogan, 'i', 'g')},
        {name: new RegExp(wing.name, 'i', 'g')},
      ],
    },
    function(err, found) {
      if (err) {
        console.log('in  err');
        cb(err, null, true);
      } else if (found && found != null) {
        console.log('in wing');
        cb(null, found, true);
      } else {
        console.log('in else');
        cb(null, null, false);
      }
    }
  );
};

const Wing = (module.exports = mongoose.model('Wing', wingSchema));

// let newWing = {
//   'slogan': "new adfadfaadfdfadfayer",
//   'name': "new Pradfaadfadyer Wing",
//   'description': "this wing handles the churhc central organzing wing, web team, technical team and publicity team.",
//   'logo': "https://pngtree.com/free-logo-png",
//   'createdAt': Date.now()
// };

// Wing.isExisting(newWing, (err, found, cb) => {
//   if (!cb) {
//     Wing.create(newWing, (err, created) => {
//       if (created) {
//         console.log("we have created a new wing");
//       }
//     });
//   }
// });

