'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sermonSchema = new Schema({
  title: {
    type: String,
  },
  minister: {
    type: String,
  },
  scriptures: [String],

  message: {
    type: String,
  },
  banner: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  updatedAt: {
    type: Date,
  },
});

// var Sermon = mongoose.model('Sermon',sermonSchema);

sermonSchema.statics.findByTitle = function(title, cb) {
  return this.find({tile: new RegExp(title, 'i')}, cb);
};

sermonSchema.statics.isExisting = function(sermon, cb) {
  if (sermon == null) {
    console.log('empty return');
    return cb('sermon is empty or undefined', null, null);
  }
  this.findOne(
    {
      $and: [
        {title: new RegExp(sermon.title, 'i', 'g')},
        {minister: new RegExp(sermon.minister, 'i', 'g')},
        {message: new RegExp(sermon.message, 'i', 'g')},
      ],
    },
    function(err, found) {
      if (err) {
        console.log('in err');
        cb(err, null, true);
      } else if (found && found != null) {
        console.log('in sermon, it is existing: ' + found);
        cb(null, found, true);
      } else {
        console.log('in else');
        cb(null, null, false);
      }
    }
  );
};
const Sermon = (module.exports = mongoose.model('Sermon', sermonSchema));

// var sermon = {
//   title:"Leavers' Fund Raddddssising",
//   minister:"Mrs. Theodosia Jackson",
//   scriptures:[
//     "mathew 1 v4-6", "Colossians 1 v 3", "Genesis 12 v 8-15","Rev. 10 v 5"
//   ],
//   message:"Come one, come all, this is the utmost encounter",
//   banner:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAR8AAACvCAMAAADzNCq+AAAA+VBMVEX///8zMzNon2M+hj0uLi5rv0dZWVlyqWJxqGErKyt1rGR2rmN3sGNtpV94sWJhm1xbmFV5tGF3tl0jIyN3d3dmn1t1t1lkZGRyuVWpqamwy65jmlhwu1FVlE72+vaqx6e+07zs8+xOkEk5OTkaGhoUFBQODg5Wmkx+fn4xgTDJyckAAABYoklJSUnl5eV+rHqcnJxSUlLZ2dm7u7vc59tWmU2HsYOfwJxYpUjN3syvr6+WupPK3Mlra2uSkpLV1dV0pm+GunyFtn4nfSWbx4zExMSZwI6GunJmpFOz0Kzn8uRfuzSCxmid0Yy12anQ58hVqUBKlkJClzRFieJoAAANT0lEQVR4nO2deUPa2BrGw1Jb2jKJQ2lrNCFgUFEBlxCkoDLjdabjnWl1vv+HuVlOlrPmTYgXNHn+aF1IOPzy5jnb+0ZJetkaDc+P192GTdanRr2/dbHuVmyuPjUqlXr/02jd7dhUuXwqlUb/Zt0N2VD5fCqVVmtn3U3ZSAV8HEL79+tuzAYq4lMpjZqhGJ/SqBnC+DhGbZZGjYng4xr15brbtEmi+FQqJ+WAOhKDT+N63Y3aIDH41LfW3agNUslHrJKPWCUfsUo+YpV8xCr5iFXyEavkI1bJR6ySj1glH7FKPmKVfMQq+YhV8hGr5CNWyUesko9YJR+xSj5ilXzEKvmIVfIRq+QjVslHrPXyudj5pv/f3iyTVuazQsLQ6Lrfyiuvr/vHh+Y0lzNJ0vHRUZiisSKfG7OfOS3v5sB779Z+Dvkif33/8P69ohmrn0kabfUbjf4Wuu4r8fnWajkvN8+zxNBloxW8X//LillrP7e3HTzv3+5qVWu1M7lXre416uDIu/NX4HM7NNEB6FwpdHtuxt5xtfRi689ff/no8Hn7tr27O7G7K5zKv+KoUd6dn5mPYx71Cn4usEafYsd6arW+Zfo8kqT/5/P2NuLzdnd3V5l0Mp7JMZ6hiTVq//iCogPkc9PHwaZxkTOTvigVc5jJhn58/fxrxKe56xJSjCxnkkZfyKtWb5l0QyF8IvOIjoK6COPYdCeI6edXB0/E5927XU+ZbAh1F8lK5HM7ZGGtNA4ALnJxzjwWfoKYHrb3vn7G+TR9QO3UNhQznhX5jD4d1DlHJrqI4Fj/BA24Den/vHHx4HzetRGgdiob4lzxLHzO+qIwNIe3omNZxkOeAGhDP9682aP5NFEAOYCU9gJIB+tqVuNzWUkIQ0EK/mUdEsL1/jXAhn66dJh8ggByQ2gJsiGyq8nOR2QeoRrmWeZj/RMk2tDD3//l8gkDyAHUnswTB2a87iI9Hz3BPEK16lQKPvhY/wRCH9P/cejw+UR3mKuJKqRzkcZ4xHzExoPLPMcrgQDGQ5yA72M/PDrJfBCgdpNvQ/Q4FSImn/sk4yHOEZ9y3O+nDWG+Df3cQ3j4fLA7zA2h8QMbT+qrhppG87nYMtOCDqcco/TH+idgTMo840nk0yYAsW0osavhieKjH2UJQzTluDjJRMc7wTnREDWiA+ET4Gk325MZcaoRuLugRPK5T9n/RSfqX0vSl8x4HBfCfX42OITxaZN8ms0BPp4epekuyI+F8xmdrPABd6TsBzsBhI8U5j1tLxWf8A5rtgd4P3aU8ZK7Ivh8y3ibeqc6z5OP3dM0GB8SULM2wKcbq0Q1wWdnBT6VYd58HtPxQfGjFYaPBuNDWHTtZfKpm6YpOjuLz1U6Pt4dpqXk0xC6Uxo+zicUnkvIx5mr6yPRII3FR4Pxwe6wWio+zsBLOKCB86mbW7e31KpkXAI+wVqPYJBP8pm4fE5hfJQIUC8NH38ZUzSbBvNpVbzxybFg+sDlE5+kcxtD8BlPJpMogJL4RIBqKfiE6+j81Rggn0Y/bP0O9x7h8CHXmjmLDCSfw4lL6BTIJwDUg/NpmLF9mDPOgjSIT/0gvg6mH3HGo0Npn3VwhVokvGGFEMHnd4/PBA0SoXyUGpRPvY/v43HWFSF8yAUMdxbKeFnjmsmnxZianzHehuLjAzqE8dGUKHwgfFpbVLNuh4wQAvAJ16+6tmygLy/p19VPLlh8Gkc0HkkC8Hk69Aj5AZTMxwPkhw+AT/0Lq1VDxgsT+YSvmCpyVQlWea/JU7XcRS4GH/ZmK+uFDD4uocMUfGqIDz7/YvBhX7WtTHx81IYsV10pc29yTD0KZof3sVfj4xB6hPFxAGnr4zNVqkiyRvEJFxCfg8/hBManpwXhsw4+czngU1V0nI8zcAy8Lk8+pwGfwytY/PQcbSAfNHDMn08IqAfn4xPaID74Bli+fAJCtacUfFxCG8MHHzjmyue301NEyAmIvTR8HEIbwofauMqTz9UpIuTw8QLopfDpIT6Mh5c9Bx/PcR9hfGpr46PPUQcvV73dk/sTVhrhM/B58rskIJ/auvhIkjV2CMnhDGPE2vHMlc/VVRg+tdrVxvKpVIJfLqraVJwikTMfR0/BmG/vK4hPDcan0mA82HAn2/y0HmVjJuWx5c4nCJ9a7RQWP1A+dN/CXvmDxE+9Ty8GPD+fR5dPLRIwfmpAPsTYhM5nhfOBJ4XnzecpxucJyKemwfhgi6LctDLo+jMsKTxHPn8+4uHjBxCET08D8gnnRoJ8Vvj+BSQpPFc+j49PGJ8nIB8ngKB8vLVRYT5riv0vQE53vnzw8KnVgPeX40BwPu6mnvDXafZPGwfMYcNz8XmqUYBAfGpp+CQo5f4ytifyzHzI8HH6eCAfsP/kzifBhuB86CZTfCg8TgDB+CSvbzwfH6ENMficMB9zf0S/D8Hnx78MPt9hfO4M7FTXK+RHNfCNjvs+5BhuhRvFp37CzB+XpCEFiOCjDwYMQCA+Axl/r8uD7HwOiHkI3W4mIU5ON8mH2lmMRG1TkzeiPr6j+Rwm8xncUaW74GomUnU6u/8IlqDJtiGcDyO1Pv75iXdqUZFmyTSh7SQ+d2PGJJE3e0igY7L+BgawwIVpQ3E+nNIMbqtZRm7UyJtsIuYzUMjcXqQMaevcv6FyDKtTYNjQMMJ3APnrGvE5M7ujm94RhH4R8KkNDP57pUygj03OaAEL7Kh6/5CPwHhwRTbEmeB1CRvq8fnczYVvxc07Yci5vOIJOdDSTLz7Q3ziW2KJCmyIOwG2FIzQdw6fwTKxyvLiHFj9ALi8QBs6wQ7y+CQbD/FOvg0JFggMrK9n8unxjAcXqA4DeHkBdZpkdds+1HhweTYkXECZx2xoQvPREiq/Ykq0oRSXN6HczunCiDD80gIbD66dVutEeNG6y+gm+/iR4HOYXDkYSVw2nPLyiso1GdW1+k3W55foZ0nFzLNeEEITPH4mycaDS1BHKC45Zmh0zaGd/SERmaUGNhTnM2mDjAcXL0WUtauRJGZCbloPzkf63L/JeiEfQclp17IEdx3jxsj84BTK9Bv91M9wyUkPvg29Q3wO/+C1wxorjgTbdWT/DCs15wgz/dUf4bOKFu6Uo+fx+f47p9ZU6tqKly8gK4J+DeufUxsPrpjpZ3z2Sn5Snb6++f5tk1+r3FHCbAqULsBWOE3grUekEBp7ZrKwnKXbd4MP3//i/XqBMkmDhFLWhD6QZ0M5/UHY+4pprsWWaT2oBtd4ln6iibK0wpRboQ2ZZm52cXy/4Q/oC9Nw/DwTfRr/jnfIpn+mHKUGthwk0D+MEaFlhlHSa9Oiiu6n+POSZqwfFlFWGCrEg26CoNLyeqTkSxRuPPDfFUSh8bC7qiC2FNhDlF6bAB7D9KZiqBsYj2iozOjbiiF9qvl0Eh8UGdkQNjGZdaZJR75gGUpQlgYY5sXG1sGP9KXa7S5Wf9LtZsqqAuZYcS3CKj/0g2XXWiy6EvT4lyVLgxgPLjS3l5fed8ZCsqeqJunj52nhelVNXONhqGt7N5nsHWXrkm1Jmi69Rj4zBWo8uKylF0Lulw6WuaI5Pdr8Fd5gqvMx5UzzTrcaySvTckZDbvw4PpRv0zZCavApU8twj3QjZjaV7IXU0y3x1v3LFINP13qIddWzjr10e7il3ZlhrzPCI23D+Ue3qq9xEYjm0+0pnpu40jtyuAwty4oSd5iIj9RZqoZtv0Y8LD5uiZrPx5KjikefkRb1c0b8SGv2Cr3ZlYhPMDTy5ROKBgJGZud6SRLx8cc48niqqmpnOvbvNC3wpsLzcX/lxEv4S73jAQtGgYXnMw5HyIEW3mASOU3h+XjhgwOw3dGk4X9d8qH5uLMRGS3Rl3yqUbAEGo/HS/SjwvOxvQCyDU6CR+H5zNBSqiIv5+qCSpQqPB8/gIIhoqLIthpfQi35SHb4QDCESVGm4Tyi5OPcYs6gGZ+DyeGf5Cv5eN8tpuOqosQwBXtAJZ9Qetcy1Hk12AbyX17yIaUb3lYQ2hosOh91Pp9TuxrenMyHV3Q+XqdOvnzhTsD8deai87FdqyH3jF0oaAJWdD4dF4VNvNzd1EFrHkXnY7mDQxnPpzOUKKiKzkcae51VVQ1mXl3DSxKS0QJi4fl0g/mpMzpE/1bL9cP4/gW5vePRKtfnwyGOPldkgo5dzk/j4+euuvTmXrK/wjGN9fclH1+6tTBUtaMaxBZpMfh4Az5stsWff2Hqjunl+1cof5RTjWWjwvj4e4XL52vYpsgb5VSVcegsED6oKgz2vIWXLT1IXw1y7DqxPUC2whTfYhSrBAVw/lZ7fArBlG4zU8RfscLiAmU896NJ5r9Y1QpYYhAUp6D/ZG62b2FLVNTYSBnlfdMqcomTPlVkdOeQiz7BK4peImfYVUVeTjmxEZZYFsl4wCpLdDFZUwzDA69ut5iy5kq8YqXwxoOr668UhhVPKlomS/rDFIURqs3xvWaWtnyuCDKCrr4TTCbSlM8VQKHlBFOzdTdo4xSMleNT+1Jx+XW4cmxpqBQudVmt2htmPP8DINZNsq+0NLUAAAAASUVORK5CYII=",
//   createdAt:Date.now(),
//   updatedAt:Date.now()
// }

// Sermon.isExisting(sermon,(err,found,cb)=>{
//   if(!cb){
//     Sermon.create(sermon,(err,created)=>{
//       if(created){
//         console.log("we have created a new sermon...")
//       }
//     })
//   }
// })
