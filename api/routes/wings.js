'use strict';

const express = require('express');
const router = express.Router();
const apostliteS3 = require('../controllers/apostliteS3');
const wingController = require('../controllers/wings');

/**
 * @api {get} https://nupsg-apostlite.herokuapp.com/api/v1/wings  Retrieve all wings
 *
 * @apiDescription When found, the api will return every wings record found in the database.
 * If you want to retrieve only specific items, add query parameters to the api request.
 * You can obtain old wings items by specifying the limit, last id and adding the order=asc
 * This allows you to retrieve wings items up to the limit created before the ID that has been specified.
 * @apiName GetWing
 * @apiGroup Wings
 * @apiVersion 1.0.1
 * @apiParam (limit)   {Number} limit  (optional query param  This is the amount of records that should be returned by the api.
 * It defaults to 100 and the minimum limit that you can parse is 1.
 *
 * @apiParam (order)   {Number} order  (optional query param) This parameter tells the api to sort the records in ascending or descending order.
 * Use {asc} for ascending and {des} for descending
 *
 * When not provided the order defaults to ascending.
 * @apiParam (last_id) {Object_Id} last_id The id of the last item in the previous search result.
 * When you add the last_id option, the api starts the search from the last_id that you provide.
 *
 * You can use the {last_id} with {asc} or {des}.
 * When you use {asc} the api starts from the {last_id} you have provided and search for items created earlier than the last_id you have provided.
 * Wehn you use {des} the api starts from the {last_id} you have provided and search for items created after the last_id that you have provided.
 *
 *
 *
 * @apiExample {js} Get wings By Asc
 *https://nupsg-apostlite.herokuapp.com/api/v1/wings?order=asc
 *          Retrieves all items from the database in ascending order.
 *
 * @apiExample {js} Get wings By Des
 * https://nupsg-apostlite.herokuapp.com/api/v1/wings?order=des
 *          The request retrieves all items from the database in descending order.
 *
 * @apiExample {js}  Get wings By Limit
 * https://nupsg-apostlite.herokuapp.com/api/v1/wings?limit=20
 *          The request retrieves only the first twenty items from the database in ascending order.
 *
 * @apiExample {js}  Get wings By Last_id
 * https://nupsg-apostlite.herokuapp.com/api/v1/wings?limit=20&last_id=de23ab32009a2b&order=asc
 *          The request retrieves only the first twenty items created before the {last_id} that you have provided.
 *
 * @apiExample {js} Get wings With options
 * https://nupsg-apostlite.herokuapp.com/api/v1/wings?limit=20&last_id=de23ab32009a2b&order=des
 *          The request retrieves only the first twenty items created after the {last_id} that you have provided.
 *
 * @apiSuccess {String} title   The Headline or Title for the wings
 * @apiSuccess {String} content The actual content of the wings
 * @apiSuccess {Date}   createdAt The date when the wings was created
 * @apiSuccess {Date}   updatedAt The date for the most current update of the wings.
 * @apiSuccessExample   Example data on success
 * {
 *  {
 *    name :"Organazing",
 *    slogan:"E just dey biii",
 *    description :"This is the central organizing wing of the union",
 *    logo:"images/logo.png",
 *    createdAt:"24-04-2018"
 *  }
 *
 * }
 *
 */

router.get('/v1/wings', function(req, res) {
  const query = req.query;
  const options = {
    pageNo: 1,
    limit: 100,
    last_id: null,
    include: '',
    /**
      the default sorting order is descending (returns the new items first)
      this can be in two possible states "1 = asc" for ascending and "-1 = des" for descending
    **/
    order: -1,
  };

  if (query.order) {
    if (query.order.trim().toLowerCase() == 'asc') {
      options.order = 1;
    }
  }

  if (parseInt(req.query.pageNo) > 0)
    options.pageNo = parseInt(req.query.pageNo);

  if (parseInt(req.query.limit) > 0)
    options.limit = parseInt(req.query.limit.trim());

  if (query.last_id) {
    options.last_id = query.last_id;
  }

  wingController.getAllWings(options, function(err, wings) {
    if (err) {
      res.status(500).json({
        message: 'an error occured while fetcing wings',
        error: err
      });
    } else {
      res.status(200).json({
        message: 'success',
        meta: options,
        wings: wings,
      });
    }
  });
});

/**
 * @api {get} https://apostlite-mongo.herokuapp.com/api/v1/wings/{id}  Retrieve  wing by ID
 * @apiDescription Get information for only one wing using the wing id.This routes will always return one object if found
 * else it will return an object of wings(i.e one object containing several wings).
 * @apiName GetWingByID
 * @apiGroup Wings
 * @apiVersion 1.0.0
 * @apiParam   {String} id The unique id of the wing you want information about.
 * @apiParam   {String} include This is provided as an optional query parameter.
 * To fetch a wing with all executives' information embeded in the request, you have to provided this parameter.
 * @apiSuccess {String} name The name of the wings
 * @apiSuccess {String} slogan The slogan for the wings
 * @apiSuccess {String} description The description of the wing
 * @apiSuccess {String} logo The logo of the wing
 * @apiSuccess {Date} createAt The date on which the wing was created in the system
 * @apiSuccess {Date} updatedAt The date for the most current update of the wing
 * @apiExample {js} Get Wing By Id
 * https://apostlite-mongo.herokuapp.com/api/v1/wings/{id}
 * Returns only one wing with a reference to the executives
 *
 * @apiSuccessExample Example data on Success
 * {
 *   "wing": {
 *       "executives": [
 *           "5c67bdefaf9fa35230f0503e",
 *           "5c67bdefaf9fa35230f050d4"
 *       ],
 *       "createdAt": "2019-02-16T07:34:11.486Z",
 *       "_id": "5c67bcf48a621a51098c6495",
 *       "slogan": "In His Service",
 *       "name": "Organizing Wing",
 *       "description": " this wing handles the churhc central organzing wing, web team, technical team and publicity team.",
 *       "logo": "https://pngtree.com/free-logo-png",
 *       "__v": 0
 *   },
 * }
 *
 * @apiExample {js} Get Wing with executives
 * https://apostlite-mongo.herokuapp.com/api/v1/wings/{id}?include=executives
 * Returns one wing with all executives information included
 *
 * @apiExample Example data on success
 * {
    "wing": {
        "executives": [
            {
                "_id": "5c67bdefaf9fa35230f0503e",
                "profile_photo": "https://pbs.twimg.com/profile_images/1055795133185179651/HMBnFwQ0_400x400.jpg",
                "firsname": "Samira",
                "lastNmae": "Bawumia",
                "office_position": "General Secretary",
                "bio": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
                "year_group": "2014/2015",
                "__v": 0
            },
            {
                "_id": "5c67bdefaf9fa35230f0503e",
                "profile_photo": "https://pbs.twimg.com/profile_images/1055795133185179651/HMBnFwQ0_400x400.jpg",
                "firsname": "Marie",
                "lastNmae": "Currie",
                "office_position": "President",
                "bio": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
                "year_group": "2014/2015",
                "__v": 0
            }
        ],
        "createdAt": "2019-02-16T07:34:11.486Z",
        "_id": "5c67bcf48a621a51098c6495",
        "slogan": "In His Service",
        "name": "Organizing Wing",
        "description": " this wing handles the churhc central organzing wing, web team, technical team and publicity team.",
        "logo": "https://pngtree.com/free-logo-png",
        "__v": 0
    },
}

* @apiExample {js} Get Wing Programs
* https://apostlite-mongo.herokuapp.com/api/v1/wings/{id}?include=programs
* Returns one Wing with all programs records included in the response
*
*  @apiExample Example data on success
* {
*
* }
*
* @apiExample {js} Get Wing Executives and Programs
* https://apostlite-mongo.herokuapp.com/api/v1/wings/{id}?include=programs&include=executives
* Returns one wing with all executives and programs included in the response.
* To include, multiple records in the response the api expects the include to be in an array.
* Apart from doing it like the example above, you can also supply the include an an array with the names of
* the records that you want to be included in the response.
* @apiError DatabaseError An error occured while fetching the records from the database
*
*/

router.get('/v1/wings/:id', (req, res) => {
  const query = req.query;
  const options = {
    id: req.params.id,
    // include: ''
    include: ['meetings']
  };

  if (query.include != null) {
    if (typeof(query.include) != 'object') {
      options.include.push(query.include.toLowerCase());
    } else if (typeof(query.include) == 'object') {
      options.include = [];
      query.include.map((data) => {
        options.include.push(data.toLowerCase());
      });
    }
  }

  wingController.getWingById(options, (err, wing) => {
    if (err) {
      res.json({
        message: 'an error occured while fetcing wing',
        error: err,
        status: Boolean(false)
      });
    } else {
      res.json({
        message: 'success',
        wing: wing,
        status: Boolean(true)
      });
    }
  });
});

/**
 * @api {post} https://apostlite-mongo.herokuapp.com/api/v1/wings  Create a new wing
 * @apiName CreateWing
 * @apiGroup Wings
 * @apiSuccess {object} wing wing_object The wing to create
 * @apiParam (name) {String} name Name of the wing
 * @apiParam (enail) {string} email The email for the wing
 * @apiParam (slogan) {string} slogan Slogan of the wing
 * @apiParam (description) {string} description Description of the wing
 * @apiParam (meeting) {object} meeting Includes everything about the wings meeting
 * @apiParamExample {json} Example meeting object
 * meeting: {
 *   day: {
 *     type: String
 *   },
 *
 *   time: {
 *     type: String
 *   },
 *
 *   venue: {
 *     type: String
 *   }
 * }
 * @apiPrams (executives) {object} The executives of the wing
 * @apiParamExample  {json} Sample-Request:
 * {
 *    name: "name of the wing",
 *    email: "example@email.com"
 *    slogan: "slogan of the wing",
 *    description: "write something to describe the wing",
 *    logo: "{file}"
 *    meeting: {
 *      day: "Monday"
 *      time: "7:00pm"
 *      venue: "Parade Grounds"
 *    }
 * }
 * @apiDescription Do not provide Date with the request. Date is provided by
 * default by the api.
 */
router.post('/v1/wings', (req, res) => {
  console.log('This is query', req.query);
  apostliteS3.apostliteUpload(req, res, 'wing[logo]')
    .then((resp) => {
      const wing = req.body.wing;
      wing.logo = resp.Location;

      wingController.createNewWing(req.body.wing, (err, cb) => {
        if (err) {
          res.status(500).json({
            message: 'could not create a new wing',
            error: err,
            status: Boolean(false)
          });
        } else {
          res.status(200).json({
            message: 'success! wing has been created',
            wing: cb
          });
        }
      });
    });
});

/**
 * @api {put} https://apostlite-mongo.herokuapp.com/api/v1/wings/{id} Edit a wing
 * @apiName EditWing
 * @apiGroup Wings
 * @apiParam (Wings) {Number} id Wing unique id
 * @apiDescription To update a wing, please provide the new wing in a json
 * object and parse it to the api.
 * @apiParam (Wings) {object} wing - The new wing to be replaced
 * @apiParamExample Example Data The new wing
 *
 *{
 *    name: "name of the wing",
 *    slogan: "slogan of the wing",
 *    description:"write something to describe the wing",
 *    logo:"an image or logo for the wing"
 * }
 *
 */
router.put('/v1/wings/:id', (req, res) => {
  wingController.updateWing(req.params.id, req.body.wing, (err, cb) => {
    if (err) {
      res.json({
        message: 'an error occured while updating the wing',
        error: err,
        status: Boolean(false)
      });
    } else {
      res.json({
        message: 'success',
        status: Boolean(true)
      });
    }
  });
});

/**
 * @api {delete} https://apostlite-mongo.herokuapp.com/api/v1/wings/{id} Delete a wing
 * @apiName DeleteWing
 * @apiGroup Wings
 * @apiVersion 1.0.0
 * @apiDescription To delete an object, you need to provide the unique id of the object.
 * @apiSampleRequestExample /api/v1/wings/df45adghs5h14
 * @apiSuccess {String} message Returns success if the delete was successful
 * @apiSuccess {Boolean} status Returns true if the delete was successful.
 *   {
 *    message:"success",
 *    status: "true"
 *  }
 * @apiError An error occured while adding the wing.
 * @apiErrorExample {object} Error-Response: an error occured while adding the wing.
 *
 * {
 *    message: "an error occured while adding the wing",
 *    error: "the type  of error that occured",
 *    status: "false"
 * }
 */

router.delete('/v1/wings/:id', (req, res) => {
  wingController.deleteWing(req.params.id, (err, cb) => {
    if (err) {
      res.json({
        message: 'an error occured while removing the wing',
        error: err,
        status: Boolean(false)
      });
    } else {
      res.json({
        message: 'success',
        status: cb
      });
    }
  });
});

module.exports = router;
