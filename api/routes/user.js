'use strict';

const express = require('express');

const router = express.Router();

const jwt = require('jsonwebtoken');

const config = require('../config/config');

const userController = require('../controllers/user');

/**
 * show  all the users here
 */
router.get('/', (req, res) => {
  const query = req.query;
  const errorMsg = {
    message: 'an error occured while fetcing the user',
    error: null,
  };

  const successMsg = {
    message: 'success',
    user: null,
  };

  if (query.username && query.password) {
    // get user by username
    userController
      .getUser(query.username, query.password)
      .then(user => {
        successMsg.user = user;
        res.json(successMsg);
      })
      .catch(error => {
        errorMsg.error = error;
        res.json(errorMsg);
      });
  } else if (query.name) {
    // get user by name
    userController.getUserByName(query.name, (err, user) => {
      if (err) {
        errorMsg.error = err;
        res.json(errorMsg);
      } else {
        successMsg.user = user;
        res.json(successMsg);
      }
    });
  } else {
    userController.getAllUsers(function(err, user) {
      if (err) {
        errorMsg.error = err;
        res.json(errorMsg);
      } else {
        successMsg.user = user;
        res.json(successMsg);
      }
    });
  }
});

router.post('/', (req, res) => {
  userController.createUser(req.body.user, (err, cb) => {
    if (err) {
      res.json({
        message: 'an error occured while creating the user',
        error: err,
      });
    } else {
      const token = jwt.sign({id: cb.id}, config.secret, {
        expiresIn: 86400, // expires in 24 hours
      });

      res.json({
        message: 'success',
        status: cb,
        token: token,
      });
    }
  });
});

module.exports = router;
