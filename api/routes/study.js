'use strict';

const express = require('express');

const router = express.Router();

const bible_studyController = require('../controllers/bible_study');

/**
 * @api {get} https://nupsg-apostlite.herokuapp.com/api/v1/studies  Retrieve all bibleStudies
 *
 * @apiDescription When found, the api will return every bibleStudies record found in the database.
 * If you want to retrieve only specific items, add query parameters to the api request.
 * You can obtain old bibleStudies items by specifying the limit, last id and adding the order=asc
 * This allows you to retrieve bibleStudies items up to the limit created before the ID that has been specified.
 * @apiName GetBibleStudies
 * @apiGroup BibleStudy
 * @apiVersion 1.0.1
 * @apiParam (limit)   {Number} limit  (optional query param  This is the amount of records that should be returned by the api.
 * It defaults to 100 and the minimum limit that you can parse is 1.
 *
 * @apiParam (order)   {Number} order  (optional query param) This parameter tells the api to sort the records in ascending or descending order.
 * Use {asc} for ascending and {des} for descending
 *
 * When not provided the order defaults to ascending.
 * @apiParam (last_id) {Object_Id} last_id The id of the last item in the previous search result.
 * When you add the last_id option, the api starts the search from the last_id that you provide.
 *
 * You can use the {last_id} with {asc} or {des}.
 * When you use {asc} the api starts from the {last_id} you have provided and search for items created earlier than the last_id you have provided.
 * Wehn you use {des} the api starts from the {last_id} you have provided and search for items created after the last_id that you have provided.
 *
 *
 *
 * @apiExample {js} Get bibleStudies By Asc
 *https://nupsg-apostlite.herokuapp.com/api/v1/studies?order=asc
 *          Retrieves all items from the database in ascending order.
 *
 * @apiExample {js} Get bibleStudies By Des
 * https://nupsg-apostlite.herokuapp.com/api/v1/studies?order=des
 *          The request retrieves all items from the database in descending order.
 *
 * @apiExample {js}  Get bibleStudies By Limit
 * https://nupsg-apostlite.herokuapp.com/api/v1/studies?limit=20
 *          The request retrieves only the first twenty items from the database in ascending order.
 *
 * @apiExample {js}  Get bibleStudies By Last_id
 * https://nupsg-apostlite.herokuapp.com/api/v1/studies?limit=20&last_id=de23ab32009a2b&order=asc
 *          The request retrieves only the first twenty items created before the {last_id} that you have provided.
 *
 * @apiExample {js} Get bibleStudies With options
 * https://nupsg-apostlite.herokuapp.com/api/v1/studies?limit=20&last_id=de23ab32009a2b&order=des
 *          The request retrieves only the first twenty items created after the {last_id} that you have provided.
 *
 * @apiSuccess {ObjectID} _id This is a unique object id for the bible study item
 * @apiSuccess {String} title The title for the bibleStudies *
 * @apiSuccess {Number} study_number The study number
 * @apiSuccess {String} content The actual content of the bibleStudies
 * @apiSuccess {String} memory_verse The memory verse for the semester focus
 * @apiSuccess {Array}  scriptures All the scriptures for that bible study
 * @apiSuccess {Array}  questions All the questions about the bible study
 * @apiSuccess {ObjectId} An ID that references the book that the study belongs to
 * @apiSuccess {Date}   createdAt The deate when the bibleStudies was created
 * @apiSuccess {Date}   updatedAt The date for the most current update of the bibleStudies.
 *
 * @apiDescription Please follow the example object below.
 * @apiSuccessExample Example data on success
 *
 *  {
 *    "scriptures": [],
 *    "questions": [
 *        "john 4v3",
 *        "Luke 4v5",
 *        "Mathew 5v4"
 *    ],
 *    "createdAt": "2019-02-05T13:14:45.661Z",
 *    "_id": "5c598c45cd8dda57b87c4fb3",
 *    "title": "In His vine yard 2",
 *    "study_number": 4,
 *    "content": "This is a bible study",
 *    "memory_verse": "Luke 2v13",
 *     "objective": "To be Like christ",
}
 */

router.get('/v1/studies', (req, res) => {
  const query = req.query;
  const options = {
    pageNo: 1,
    limit: 100,
    last_id: null,
    year: null,
    sem: null,
    meta: false,
    /**
      the default sorting order is descending (returns the new items first)
      this can be in two possible states "1 = asc" for ascending and "-1 = des" for descending
    **/
    order: -1,
    query
  };

  // if (query.order) {
  //   if (query.order.trim().toLowerCase() == 'asc') {
  //     options.order = 1;
  //   }
  // }

  // if (parseInt(req.query.pageNo) > 0)
  //   options.pageNo = parseInt(req.query.pageNo);

  // if (parseInt(req.query.limit) > 0)
  //   options.limit = parseInt(req.query.limit.trim());

  // if (query.last_id) {
  //   options.last_id = query.last_id;
  // }

  // if (query.sem) {
  //   if ((query.sem == 1) ||
  //   (query.sem.toLowerCase() == 'one')) {
  //     options.sem = 1;
  //   } else if (query.sem == 2 ||
  //     (query.sem.toLowerCase() == 'two')) {
  //     options.sem = 2;
  //   }
  // }

  // if (query.year) {
  //   options.year = query.year;
  // }

  // if (query.meta) {
  //   if (query.meta.toLowerCase() == 'true') {
  //     options.meta = true;
  //   }
  // }

  bible_studyController.getAllBible_Study(options, function(err, data) {
    if (err) {
      res.status(500).json({
        message: 'an error occured while fetching bible studies',
        error: err
      });
    } else {
      res.status(200).json({
        message: 'success',
        meta: options,
        studies: data,
      });
    }
  });
});

/**
 * @api {get} https://apostlite-mongo.herokuapp.com/api/v1/studies/{id}  Retrieve  bible study information by Id
 * @apiDescription Get information for only one wing using the bible_study id.This routes will always return one object if found
 * else it will return an object of bible_study (i.e one object containing several bible_study).
 * @apiName GetBibleStudy
 * @apiGroup BibleStudy
 * @apiVersion 1.0.0
 * @apiError DatabaseError An error occured while fetching the records from the database
 */

router.get('/v1/studies/:id', (req, res) => {
  bible_studyController.getBible_StudyById(
    req.params.id,
    (err, bible_study) => {
      if (err) {
        res.json({
          message: 'an error occured while fetcing bible_study',
          error: err,
          status: Boolean(false),
        });
      } else {
        res.json({
          status: Boolean(true),
          message: 'success',
          study: bible_study,
        });
      }
    }
  );
});

/**
 * @api {post} https://apostlite-mongo.herokuapp.com/api/v1/studies  Create Bible Study
 * @apiName CreateBibleStudy
 * @apiGroup BibleStudy
 * @apiVersion 1.0.0
 * @apiSuccess {object} bible_Study The BibleStudy to create
 * @apiParamExample  {json} Sample-Request:
 *
 *  {
 *    study_number :1,
 *    scriptures:[
 *      "mathew 5v11",
 *      "john 11 v 35"
 *    ],
 *    title :"Something to describe the Bible study",
 *    content:"Walking in a spirit filled life. Life that will cause you to impact in the lives of other. That is what the bible teaches and encourages.",
 *    questions:{
 *      "question one", "question two", "question three"
 *     }
 *  }
 * @apiDescription Do not provide Date with the request. Date is provided by
 * default by the api.
 */
// create bible study
router.post('/v1/studies/', (req, res) => {
  bible_studyController.createNewBible_Study(
    req.body.study,
    (err, cb) => {
      if (err) {
        res.json({
          message: 'could not create a new bible_study',
          error: err,
          status: Boolean(false),
        });
      } else {
        res.json({
          status: Boolean(true),
          message: 'success! bible_study has been created',
          study: cb,
        });
      }
    }
  );
});

/**
 * @api {put} https://apostlite-mongo.herokuapp.com/api/v1/studies/{id} Edit Bible Study
 * @apiName EditBibleStudy
 * @apiGroup BibleStudy
 * @apiVersion 1.0.0
 * @apiParam (BibleStudy) {Number} id The BibleStudy's unique id
 * @apiDescription To update a Bible Study, please provide the new bible_study in a json
 * object and parse it to the api.
 * @apiParam (BibleStudy) {object} bible_study - The new bible_study to be replaced
 * @apiParamExample Example Data The new bible_study
 *
 *{
 *    study_number :1,
 *    scriptures:[
 *      "mathew 5v11",
 *      "john 11 v 35"
 *    ],
 *    title :"Something to describe the Bible study",
 *    content:"Walking in a spirit filled life. Life that will cause you to impact in the lives of other. That is what the bible teaches and encourages.",
 *    questions:{
 *      "question one", "question two", "question three"
 *     }
 *
 *  }
 *
 */
router.put('/v1/studies/:id', (req, res) => {
  const options = {
    study: req.body.study,
    id: req.params.id
  };

  bible_studyController.updateBible_Study(
    options, (err, cb) => {
      if (err) {
        res.json({
          message: 'an error occured while updating the bible_study',
          error: err,
          status: Boolean(false),
        });
      } else {
        res.json({
          message: 'success',
          status: Boolean(true),
        });
      }
    }
  );
});

/**
 * @api {delete} https://apostlite-mongo.herokuapp.com/api/v1/studies/{id} Delete Bible Study
 * @apiName DeleteBibleStudy
 * @apiGroup BibleStudy
 * @apiVersion 1.0.0
 * @apiDescription To delete an object, you need to provide the unique id of the object.
 * @apiSampleRequestExample /api/v1/studies/df45adghs5h14
 * @apiSuccess {String} message Returns success if the delete was successful
 * @apiSuccess {Boolean} status Returns true if the delete was successful.
 * @apiSuccessExample {json} success response
 * {
 *    message:"success",
 *    status: "true"
 * }
 * @apiError An error occured while adding the wing.
 * @apiErrorExample {object} Error-Response: an error occured while adding the wing.
 *
 * {
 *    message: "an error occured while adding the bible_study",
 *    error: "the type  of error that occured",
 *    status: "false"
 * }
 */
router.delete('/v1/studies/:id', (req, res) => {
  bible_studyController.deleteBible_Study(req.params.id, (err, cb) => {
    if (err) {
      res.json({
        message: 'an error occured while removing the bible_study',
        error: err,
        status: Boolean(false),
      });
    } else {
      res.json({
        message: 'success',
        status: cb,
      });
    }
  });
});

module.exports = router;
