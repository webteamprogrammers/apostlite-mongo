'use strict';
const express = require('express');

const router = express.Router();

const meetingController = require('../controllers/meeting');

router.post('/v1/meetings', (req, res) => {
  const meeting = req.body.meeting;
  meetingController.createMeeting(meeting)
    .then((data) =>{
      res.status(200).json({meeting: data});
    })

    .catch((err) =>{
      res.status(500).json(err);
    });
});

module.exports = router;
