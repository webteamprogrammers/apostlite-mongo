'use strict';

const express = require('express');
const router = express.Router();
const bibleStudyBookController = require('../controllers/bible_study_book');

/**
 * @api {get} https://nupsg-apostlite.herokuapp.com/api/v1/books Get All Books
 * @apiDescription When found, the api will return all books.
 * @apiName GetAllBooks
 * @apiGroup Book
 *
 *@apiExample {js} Get All Books
 *https://nupsg-apostlite.herokuapp.com/api/v1/books
 *Returns a single book record with a reference to it's related items
 *
 * @apiSucess  {string}  title
 * @apiSuccess {string}  scripture
 * @apiSuccess {string}  semester
 * @apiSuccess {string}  academic_year
 * @apiSuccess {array}   studies  This is an array of objects that contains all the studies related to the book
 * @apiSuccess {Date}    createdAt
 * @apiSuccess {Date}    updatedAt
 *
 *
 * @apiSuccessExample   Example data on success
 *  {
      "studies": [
        "5c5503cb67f647560e892b56",
        "5c5503cb67f647560e892ba4a"
        "5c5503cb67f647560e892aa23"
      ],
      "_id": "5c5503cb67f647560e892b56",
      "title": "New Test Book 2",
      "scripture": "Revelations 1 v 5",
      "academic_year": "2018/2019",
      "semester": "2018/2019",
      "__v": 1
    }
 */
router.get('/v1/books', (req, res) => {
  const query = req.query;
  const options = {
    academic_year: null,
    semester: null
  };

  bibleStudyBookController.getAllBooks(options, (err, data) => {
    if (err) {
      res.status(500).json({
        err: 'Something went wrong on the server' + err
      });
    } else {
      res.status(200).json({
        books: data
      });
    }
  });
});

/**
 * @deprecated
 * @todo get all the studies related to a book
 * @api {get} https://nupsg-apostlite.herokuapp.com/api/v1/books
 */
router.get('/v1/books/:id/studies', (req, res) => {
  const options = {
    book_id: req.params.id
  };

  if (options.book_id == null || options.book_id == '') {
    res.status(400).json({
      err: 'request cannot be processed'
    });
  } else {
    bibleStudyBookController.getAllBookStudies(options, (err, data) =>{
      if (err) {
        res.status(500).json({
          err: 'Could not process your request'
        });
      } else {
        res.status(200).json({
          studies: data
        });
      }
    });
  }

  // fetch all the studies related to this book
});

/**
 * @api {get} https://nupsg-apostlite.herokuapp.com/api/v1/books/id Get only a specific book
 * @apiDescription When found, the api will return only a single record. By default the api only returns
 * the book record with a reference to it's related items. For example, a book can have one or more studies.
 * In order to fetch the studies as part of the book, you have to specify it in the request query parameters.
 * @apiName GetBook
 * @apiGroup Book
 * @apiParam {string} include This is provided as a query parameter which tells the api the items it should
 * include in the request. When not provided, no items will be embedded in the request but there will be a reference
 * to the items inside the request.
 *
 *@apiExample {js} Get Book
 *https://nupsg-apostlite.herokuapp.com/api/v1/books/id
 *Returns a single book record with a reference to it's related items
 *
 * @apiExample {js} Get Book and Studies
 * https://nupsg-apostlite.herokuapp.com/api/v1/books/id?include=studies
 * Returns a single book record and all related studies
 *
 * @apiSucess  {string}  title
 * @apiSuccess {string}  scripture
 * @apiSuccess {string}  semester
 * @apiSuccess {string}  academic_year
 * @apiSuccess {array}   studies  This is an array of objects that contains all the studies related to the book
 * @apiSuccess {Date}    createdAt
 * @apiSuccess {Date}    updatedAt
 *
 *
 *@apiSuccessExample   Example data on success
 *  {
      "studies": [
          {
              "scriptures": [],
              "questions": [
                  "john 4v3",
                  "Luke 4v5",
                  "Mathew 5v4"
              ],
              "createdAt": "2019-02-02T02:43:18.102Z",
              "_id": "5c5503c667f647560e892b55",
              "title": "In His vine yard 2",
              "study_number": 4,
              "content": "This is a bible study",
              "memory_verse": "Luke 2v13",
              "objective": "To be Like christ",
              "__v": 0
          }
      ],
      "_id": "5c5503cb67f647560e892b56",
      "title": "New Test Book 2",
      "scripture": "Revelations 1 v 5",
      "academic_year": "2018/2019",
      "semester": "2018/2019",
      "__v": 1
    }
 */
router.get('/v1/books/:id', (req, res) => {
  const query = req.query;
  const options = {
    book_id: req.params.id,
    include: query.include == null ? '' : query.include.toLowerCase()
  };

  /**
   * @todo check if the person wants to retrieve the book together with the related studies
   */

  if (options.include == 'studies') {
    bibleStudyBookController.getAllBookStudies(options)
      .then((book) =>{
        showSucessMessage(book);
      })
      .catch((err) =>{
        showError();
      });
  } else {
    bibleStudyBookController.getBook(options.book_id)
      .then((book) => {
        showSucessMessage(book);
      })
      .catch((err) =>{
        showError();
      });
  }

  const showSucessMessage = (data) => {
    res.status(200).json({
      book: data
    });
  };

  const showError = () => {
    res.status(500).json({
      err: 'an error occured'
    });
  };
});

/**
 * @todo create a new book
 * @api {post} https://apostlite-mongo.herokuapp.com/api/v1/books
 *@apiName CreateBook
 *@apiGroup Book
 *@apiParamExample {json} Sample Request:
 *
 *{
 * title: 'New Test Book 2',
 * scripture: 'Revelations 1 v 5',
 * academic_year: '2018/2019',
 * semester: '2018/2019',
 * };
 *
 */
router.post('/v1/books', (req, res) => {
  const book = req.body.book;
  bibleStudyBookController.createBook(book, (err, data) => {
    if (err) {
      res.status(500).json({
        err: 'could process your request'
      });
    } else {
      console.log('we have created a new book', data);
      res.status(200).json({
        book: data
      });
    }
  });
});

/**
 * @todo create studies for an existing book
 */
router.post('/v1/books/:id/study', (req, res) => {
  const study = req.body.study;
  const book_id = req.params.id;
});

/**
 * @todo edit a book
 * @api {put} https://apostlite-mongo.herokuapp.com/api/v1/books/{id} Edit Book
 * @apiName EditBook
 * @apiGroup Book
 * @apiParam (id) {ObjectId} This is a unique identifier for the record you want to update
 * @apiDescription To update a book, you have parse a book object with the necessary fields to update
 */
router.put('/v1/books/:id', (req, res) => {
  console.log('we want to edit a book', req.body.book);
  bibleStudyBookController.updateBook(req.params.id, req.body.book,
    (err, updated) => {
      if (err) {
        res.status(500).json({
          err: 'Something went wrong on the server.'
        });
      } else {
        res.status(200).json({
          book: updated
        });
      }
    });
});

/**
 * @todo delete a book
 * @api {delete} https://apostlite-mongo.herokuapp.com/api/v1/books/{id} Delete Book
 * @apiName DeleteBook
 * @apiGroup Book
 * @apiDescription To delete a book, you have to provide a unique id of the record that you want to delete
 * @apiSampleRequest https://apostlite-mongo.herokuapp.com/api/v1/bibleStudies/abcedfadaebbadcaed
 *
 */
router.delete('/v1/books/:id', (req, res) => {
  console.log('you want to delete', req.body.book + 'with id:', req.params.id);
  // bibleStudyBookController.deleteBook(req.params.id, (err, deleted) =>{
  //   if (err) {
  //     res.status(500).json({
  //       err: 'Something went wrong on the server.'
  //     });
  //   } else {
  //     res.status(200).json({
  //       bibleStudyBook: deleted
  //     });
  //   }
  // });
});

module.exports = router;
