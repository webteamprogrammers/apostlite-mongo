'use strict';

const express = require('express');

const router = express.Router();

const apostliteS3 = require('../controllers/apostliteS3');

const ExecutiveController = require('../controllers/executives');

/**
 * @api {get} https://nupsg-apostlite.herokuapp.com/api/v1/executives  Retrieve all executives
 *
 * @apiDescription When found, the api will return every executives record found in the database.
 * If you want to retrieve only specific items, add query parameters to the api request.
 * You can obtain old executives items by specifying the limit, last id and adding the order=asc
 * This allows you to retrieve executives items up to the limit created before the ID that has been specified.
 * @apiName GetNews
 * @apiGroup Executive
 * @apiVersion 1.0.1
 * @apiParam (limit)   {Number} limit  (optional query param  This is the amount of records that should be returned by the api.
 * It defaults to 100 and the minimum limit that you can parse is 1.
 *
 * @apiParam (order)   {Number} order  (optional query param) This parameter tells the api to sort the records in ascending or descending order.
 * Use {asc} for ascending and {des} for descending
 *
 * When not provided the order defaults to ascending.
 * @apiParam (last_id) {Object_Id} last_id The id of the last item in the previous search result.
 * When you add the last_id option, the api starts the search from the last_id that you provide.
 *
 * You can use the {last_id} with {asc} or {des}.
 * When you use {asc} the api starts from the {last_id} you have provided and search for items created earlier than the last_id you have provided.
 * Wehn you use {des} the api starts from the {last_id} you have provided and search for items created after the last_id that you have provided.
 *
 *
 *
 * @apiExample {js} Get executives By Asc
 *https://nupsg-apostlite.herokuapp.com/api/v1/executives?order=asc
 *          Retrieves all items from the database in ascending order.
 *
 * @apiExample {js} Get executives By Des
 * https://nupsg-apostlite.herokuapp.com/api/v1/executives?order=des
 *          The request retrieves all items from the database in descending order.
 *
 * @apiExample {js}  Get executives By Limit
 * https://nupsg-apostlite.herokuapp.com/api/v1/executives?limit=20
 *          The request retrieves only the first twenty items from the database in ascending order.
 *
 * @apiExample {js}  Get executives By Last_id
 * https://nupsg-apostlite.herokuapp.com/api/v1/executives?limit=20&last_id=de23ab32009a2b&order=asc
 *          The request retrieves only the first twenty items created before the {last_id} that you have provided.
 *
 * @apiExample {js} Get executives With options
 * https://nupsg-apostlite.herokuapp.com/api/v1/executives?limit=20&last_id=de23ab32009a2b&order=des
 *          The request retrieves only the first twenty items created after the {last_id} that you have provided.
 *
 * @apiSuccess {String} title   The Headline or Title for the executives
 * @apiSuccess {String} content The actual content of the executives
 * @apiSuccess {Date}   createdAt The date when the executives was created
 * @apiSuccess {Date}   updatedAt The date for the most current update of the executives.
 * @apiSuccessExample   Example data on success
 *
 *
 * {
 *  "executives": [
 *      {
 *          "_id": "5c67bdefaf9fa35230f0503e",
 *          "profile_photo": "https://pbs.twimg.com/profile_images/1055795133185179651/HMBnFwQ0_400x400.jpg",
 *          "firsname": "Samira",
 *          "lastNmae": "Bawumia",
 *          "office_position": "General Secretary",
 *          "bio": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
 *          "year_group": "2014/2015",
 *          "__v": 0
 *      }
 *  ]
 * }
*/

router.get('/v1/executives', function(req, res) {
  const query = req.query;
  const options = {
    pageNo: 1,
    limit: 100,
    last_id: null,
    /**
      the default sorting order is descending (returns the new items first)
      this can be in two possible states "1 = asc" for ascending and "-1 = des" for descending
    **/
    order: -1
  };
  if (query.order) {
    if (query.order.trim().toLowerCase() == 'asc') {
      options.order = 1;
    }
  }

  if (parseInt(req.query.pageNo > 0))
    options.pageNo = parseInt(req.query.pageNo);

  if (parseInt(req.query.limit) > 0)
    options.limit = parseInt(req.query.limit);

  if (query.last_id)
    options.last_id = query.last_id;

  ExecutiveController.getAllExecutives(options)
    .then((data) => {
      res.status(200).json({
        executives: data
      });
    })
    .catch((err) =>{
      res.status(500).json({
        message: 'an error occured while fetching the executives',
        error: err
      });
    });
});

/**
 * @api {post} https://apostlite-mongo.herokuapp.com/api/v1/executives  Create Executive
 * @apiName CreateExecutive
 * @apiGroup Executive
 * @apiVersion 1.0.0
 * @apiSuccess {object} bible_Study The Executive to create
 * @apiParamExample  {json} Sample-Request:
 *
 *  {
 *    wing_id: wing_id,
 *    profile_photo: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEBUSEhMVFRUVFRUVFRUVFRUVFRcVFxcXFxUVFRYYHSggGB0lGxcVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGi0fHyUtLS0tKy0tLS0tLSstLS0tLS0tKy0tKy0tLS0tLS0tLS0tKy0tLS0tNy0tLSstLSstLf/AABEIAQMAwgMBIgACEQEDEQH/xAAcAAABBAMBAAAAAAAAAAAAAAAAAQMGBwIEBQj/xABDEAABAwEEBgcFBQcCBwAAAAABAAIDEQQFITEGEkFRYXEHEyKBkaGxMmLB0fAjQlJykhQzY4KywuGi8RUXJENTVJP/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQIDBAX/xAAjEQEBAAICAgIDAAMAAAAAAAAAAQIRAyESMQRREzJBFDNx/9oADAMBAAIRAxEAPwCy0IQmYQkQkCoQkQCoSIQCpCsHyAZlac96RtzcPrggN9FVzW3owt1g5pG0gjdX0XLtmmFnZ94k8BUeKD0k1UVUKHSBZ9of4Yeq2bPpxZXffp+bD/HmjY1UsQudYr1ilFWPa4cCFvMeCgmaEJEAqEiEAqEiEAqVIhAKhIhACEJEAqEiEAqEiEwVc28rzbG049wz5k7Bx4Jb6vFkMRe80A8TwHGgPgVSmkmksloecaMrg3ZzO/IKbTkS2+9NW+zHRx2kHs9xzdzUPtd9yONdanIZLkNtA3V9PBZSPJGXySUfdfElKax1doBpU7K8Vgba44udQbhmVnYrLrRyZVHaGZNRy3/Bc6rgSDgRhmgN8WgbcPris2yLSimG+vj81ttlG7xQD0NqLHAtcWkbQTUeCmFwadysIbN9qzInASAb6/e8lBzKw5lNmoxzHj5oD0HdV7xTt1o3hw8DyIOIK6AK893RfUsEgkjdQjMH2XDc4bR6K4tFNJmWpmwPHtNrWh9aJypsSJCEiZBKhCAEqRCYKhCEgxQkSoAQkQmCpCULg6Y3wLNZXur23DUYOJ29wqUgr/pMv/rJepYewzOm1+35eKr9z9iettoLiScSfqq0iVKzolon4ZSRlVa0UJcaAeKlej2jJkI1yQO+pSyymM7PHC5XUciwRuL6tGAxNVr3iBrmgIFdvxVwWLRqKNvZbgc61JXHvHQ2Nzzge5Y/njo/xstKqNoIwanGP357tqmV46Gtb7JNVoP0QkAwa7mtJy41neDOfxF5paHGld1cUsNsoc6c8l2Jrge37vgFzLRYqDI+HxVzKVlcbPZ+N+tlmNm8cF0LmvR8EokYSCM+I2hR5hcw8B30XQbJUVwQHobR+9m2mBsjTwI3FdNUz0dX+YbQI3H7OTsmux2w/W9XICqibGaFilTIqEiEAqEiVAYoWNUqQKhJVCYI4qnOk6+ettHVtPZi7PDW+8fGg/lKs/SW8Oos0j60NKN5nb3YnuXny3zlzyd5U1UazzUrr3FcD5jgMPJa9x3eZ5Wt2Vx5K57msLY2BrWgAABYcvJ49R0cHF5d304V0aItaAXBTGx3cxgo0UTsTVuMauW2327Op6IyALI2cHYnmBZgKpE3KtGSwtOxH7C2mS6ACwciwvKuNa7rjcMWhQ7SDRSNwJaCrEeFo2qIEZJbs9K1L1VA3zdRiJwNOC5sBoeBVv6RXSxzTUKqrVZdR7mccF1cXJ5OTm4vC7hyB1D5hXrobegnsrHVxpQ8xgR3EFUEXHsnuVk9FF5Uc+A5Htt8gfge4raOerSWSwBWQKpJUJEIBUJEIDCqKpEiQZVRVIhAQbpPtREGrXD1J2fpDvFUy41PkrV6XZaMibvLz4AAeqquLOu6p+SlSYaD2btYfXBWtYm0aAoL0fWP7PXO3Jd+977MXYjGs/hjTmuLk7yehxTWESeNbMarhumUjT2281I7q0phkw1tU8UvCxflKlYWbVoxWkHIgrZbIiVNjYTUiDImXTp2lIVy1ZSm7VbmtBqQOa4Ft0ohaaVJPBTrbT0zv1tWFU/fmE5VnvvuOcENONMQc1WmkzKTV3rTh6y1WXyO8Nxznite8j1Ui0EtvV2uN1aAnVPJ2Hlmo412P1uC2rtfqvbT8Q9cF1uF6NidUfW5Zha13ya0bXbwD4gLYCuIZJVihAZISVQmDSKpEKQVKVjVKUBVHTBP9tC3cxx/U6g/pUAjbh3/AF8VYOmlgba73EL3FrGQh7y32tVuNG14vHmozeN1sitBjjcXsFC1xIrjmMBRRcpvTWYXx8v4svRqyllkjAzLQe8hb1mssbMKCpxJ2k7yVnZG0hYPdHoo5pJeRjbtpvGfIcVw3dvT0cNSduzb7LY34SBmO2oB8VGLdozFWsEoPCoK0OtthiZMzVhaXUbRoc80B7T3OrmNnmtu5bXPM8R6wmaQf3kXVuDmt1nAHMUJLdbgtZhnJ7ZXlwt1pI9Gusj7DzUbDVSiORRWwa2Ix7JoQfaYdzt/A7VKLsxGKx73226kZvkUc0hvd8YpG0uPCqk14ABpKiN46wx3mg24nYBtPD0RfYnaNRwW20uo46gOZJ+C79g0UjbQveXnblQqP3vNNFJqu6uMBtdeUOkNSHFgoBQE0AwFAXDHBa8V5WprDI5jSxpAJiLmOFRUGlS07cKLfxzs6YeeEvaTX3o6wt14ey9uI3clWukpOuCcDu4qwLrvsyx447jSleY2FQvTCI62txS4rfLVHNJ4biORn0C2AaVI4Eea1IjieXzW0x3gR9fBdTieg9F5g+yROGRY2nKmS6qi/RxPrXfFvbVp7iVJ1cRfZUqxSoBUiEJg0hIhSCockCH5ICpb/tNL+1a4PAids9qMED9Qan7yuZoayQdmlQQTxIUc0+tJZeskjcTHJG4DiwMIHlRTi83Nls7JWYtcQW8ndr4rl5tzKWPQ+NrLC4X/AKlViZVg5D0TV43UyRpaQE5dMn2bPyj0XTY2qwaokbmlawxVD2HIOGXEEEGq3Lkuh0R1sNYihcakgYVAqaDu3KTtjCeDAFcyy+0XHH6ch1gYwEgdoihNTlWvqVs2FuKcnWNhzUW9rnplbm1C0pbubIGkjFtaEcaV9AujbMk3ZnJpRy/boMtD94CgcCAaY0BBwP8AlacVySdUYjRrSak4EuO9xJPkFNnQg5ps2VoVeWReOP0ilguNsbdWleO3vUM6SbOGtBG1yteeMBVl0qfuWn3x5lGH7wZ/pVYwHE8lswnALTsx7Xd81t2faN2PgafHyXY8+Lg6J7RWzvZh2Xg+IB+uSnirToklwlbu1Dz1q/JWUqhX2VCRCZFSpEIBkoSISBQkkOCFi84ICgdN3Vt9oPvn1optoY0yXWwZlr5Kdz3ED/VRQHSeTWtcp3ud6lTjognbIyazOfRzSZWjew6rXU5H1Cw5cd4ungzmOe6lt0S0jaDsFPDBduzyqNwkNc5oNQ17hXkV1rPKuPb0JNx3GOT1cFzY51nJNUUCe0XFhapquoMqVKdsMjaZrh3vaXRdrYRnTLgVwroveWry9oAzbqk94IKW1/j3FgWqRoGa0oJaOG0FRO9r3ldETG0E1w1sqbSaZro3HbHSNaDStKmmQTtL8ekuDlg5y1myUGKwfMntHiatkqrHpQm+xaPfCsG2S4KrOkWXWLG8SVfH3lC5uuOoJF7S3Ijj9bVp/eW1Hs5rseas3olf25G72A86Op9c1aQKqXomk/6hw/hHx1/91bIThZBCRKqIqEiEAyhCRIMkxbH0Y47gT4CqeXN0il1bLMf4b/6SgPPlsfV7idpWVht8tneyWF5jkbUhzcxXAgg4EEbDgmbQcTzSECvIKVrG0AvF80cokdrOa9p2V1XNp6tcpjZ5FV3R/btS2OYcpWUH5mdoDwLlZTHYrh58dZPS+Pn5YdurHItyJ65DZKYpm2X22MVdWgzIBPospW1iRyMDs1zLbdgcDQDwXIfpeynYDjx1T6LCLSymNcNoeDTwIWlxVhx53uOtd10BoNcV14rO1uQAUZfpW2mBA3BuXzKwZpbvjeRv1XfJExLPjz91JpZKLUfIuXZL0dONdjHhm9zS2vIHFbZyUX2iRr22TBVTp9ay2ZoFPZJx54KzbU/NU1pfaustbyMm0aO7PzJW/BN5Of5WWsXLqtmMrWAyWxEutwRYfRRX9pcdgjp4mmPeFbwKqPomZWeQ7meZcFbTCnCyZIQhUQQhCAZQkQkCrgaczatgmO9oaP5nBvxK7yiPSZNSxEb3N+J+BSpz2pWTF3mkrmgnErEFI2bLQ6ORr24OY4OHMFW9dN5smibK04OFeIO0HiDgqakOKkugVsc18jKnVoHU41pULHnw3jt0fG5PHLX2teCVb9npQggY5qN2K2Ald2B2sMFw+nomLRE1mQHfksIrW0AgsYa/iANPFdB0GsKFMjR2N2Z8FcyrScupqtfr42D2YhxaxoOOONAs7NL1jssPJbA0aiG/xT7bLqCgVXKllyyzo7I4atFzbVLQUT8zqKO3tebWgknJR7ZemlpPe4hhc450oBvccgqgc4uJJxJJJ5nErqaSXw60SnYxpIaPUniuWwLu4sPGPN5+Tzy69HBsT8aYYap5pwWjKLH6JHfaSn3W/H67lbDDgqo6Ih2pTwb8aenmrXanCrJCEKiCEIQDCVIhIFKrvpZtX2UcdcyXEeQ+KsF7sFT3Sdbw+0ag+6MeFaJU4g5OHf5BIEjnJK4pGwecVI9AWVmkH8Mf1BRpxUv6M46yyncxo8Sfko5f0rTg/wBkSWQFpwXVuu9qGjk3bbKuY+JcHt6idQW9pCfFvCr6O0vbkU+283/RRqjpOmXhVYS24b1C/wDij93mm32t7ttE9UXTrXzfQ9luJULvR7nVLiuw2Jc2946MceBWmHVY8npXjjU13mqUZLELM5LueYcZkPranHHBNt2JXlI1o9EH/d/k/uVpNKqbogl7crfdb6/5VrsThU4kqkSlMiIQhMGkJKoJSBi1uoMcvliqA0ltnW2iR/4nnwrh5K6NM7d1Vke4GhI1R/Ngad3oqGtMlXKaqejL80gzSOOKD5nJAYP2qzOjq7jHCXOFDIdbkKdn59642i2iRcNedpzBaw7BgauHwPGuYpY1ms+qAubn5Jrxjs+PxWXyrOSPBaE9lC7OpUJh8a5HdEcmshTHUkbF3po8clrvi4J7PTldWsmRrfMKdisyraaYbDguPfsPYd+U+ilHU0C495Q1B5FVje2eU3FPrOmK271sDonkEYVq07Kbua02HFd+9x5VmrqngFi4rIGqbAQKn/RLPS1Ob+KM+ILT81ckblQ/RzPq26PiHDvpVXpCcE4VbCCkQSmQQsaoQDaRxXJvfSOzWf8AeyDW/A3tP/SMuZooheXSW3KKE03veAf0tB9UBrdK9611IGn3nDjkPj4qsCTWmNTgBtKkV6W/9omM7hQuoA0GoaAKUHzVj9G10WV1mbMyNvW1c2RxxdrA7zkCKGg3rPPLxm2uGHldbV1cmg9rno5zeqZveO13Mz8aKfXLoVBZqOoXv/G/E92xvcp+yzALXnhxXLnnlk7OPjwxcdlnNcanAAVNaAbOA3BOPYt4xpiRixsdErXjTpZVM5FbLCpU0pIMVryQrpSBMFiDaQiKejiK2hGleKJk0pgtU2bWC23CpW3HDQKoiode1xtlaWkVw+qcVAL/ANG5bM6pBdHscNn5tyu1kJ1iBgNvHh6LO03a2QapHitsOS4sOTixyed43rJ2B4FWZpB0dhxLoeyd33T3KA2+55oXlkrdU7NoI3hdOOcy9OPPjyx9t7Q1+rbYT74+I+K9BWYYBebrukMUrH/ge13gQSF6Huy1tkja9pBa4AgggjyVxnXQSLHWWQTIIQlQTzQZCsdZIhCjsMmxTnoyv4We0GGQ0jmoAScGyjBpPBwwrvDVAVtRuqPVTlNzSsctXb0wE1IxRbo40j/abP1cjqyw0a4nNzPuP57DxFdqlj+C5csddO3HLfbTfGtV7KHgfVdIuWL4wRhUcjQ9xGSz013XKngwwTUS6T4C0YYjz/ytFjRrU8tvgs7GmN6I5ixER3LdaxO6gojxO5OexiwnGC3X4LTkaSU9DZiCNbYbgso2jIYnh8dy2WQ0xJOOwE04VG3M+KqRFpqCBb0UIRHRZhyqM8qwtGq1jnOIDWgkk5AAVJKoHSO8/wBptMk2TSaMG5gwbhxz5kqbdJ2k+sTY4jgKGYg5nMR+hPcN4VayuXVxY67cfLnvo29+K6Vy37aLM6sMhaDm04sdzacO/PiuWhasVs3F0jwvAbaWmJ34mgujPHDtN8xxU1sVtjlbrxPa9v4muDh5LzlVbt23pNA/Xhkcx29pz4OBwcOBQT0RVCqePpOtIABihJoKmjxU76a2CEBAKoSIQYTkTqJsIQHYuK9pLLO2aPNuBByc05tPA+RAOxXxcd9RWqBssZqDmD7TXbWuGwj5HIrzpE/Yuzo7f81jl6yI1BprsPsvG47juOzxBzzw8mvHyeL0A6NYFpXP0Y0lgtjKxu7QA143YPYeI2j3hh6LtloXNcXZjntqFxyTMkNVuFiHNUWNJWi6Mg4EjzCzbrDMBw26px7gdvenHtxT7WbUaFrReDTKnmmWWauLsfTwC6MjarDVQcpkADABGKe6tZhiCujAaozpxpU2yRdXGQZ3jsjPUH/kd8BtPAFMaY6cx2fWigpJNiCc2Rn3vxO93x41LarS6R7pJHFznGrnE1JK34+Lfdc3LzfyG5JDUkmpJJJOJJOJJO0lazile6qxXS4whCRAKhBQgFqlSUQgGwgoCDmgFCEBCAE6yTemilCA3rJaXxvEkb3Mc01a5pII5EKxtH+lEgBlsjLsh1sVAeb4zQd7T3KrWvonRIpuMqsc7PT0bc9+2e0t1oJWvyqAaPH5mGjm94XQcF5kZIQQ5pIIxBBoQd4IxCkV26fXhCKdd1jRkJmiT/Vg8/qWV4vpvjz/AGvUhJU5KpoOlucDt2aJx2lr3sHgQ71Tv/NmT/1Gf/Zx/sUfiya/nwWnVZ6qp+09KtqdgyKFnMPee7tAeS4l4aY26eofaHhp+7HSMcuxQnvJTnDf6m8+P8XPfOkFlso+3la07GDtSHkwY9+SrPSnpClnrHZw6GI4F1ftXDmPYHAEnjsUJc7M78Sd53lNulWuPHIxz5rWZdRMPfVISkWjEIQgpgJAlSDNIBKsSlKYKlWNUIDFqEISDJIhCAUoQhACEIQGSQFKhAKgJUIBQhzkiEAiQIQgBIhCAEIQgESbUIQCHNZFCEAIQhAf/9k=',
 *    firsname: 'Maria',
 *    lastNmae: 'Curie',
 *    office_position: 'General Secretary',
 *    bio: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.',
 *    year_group: '2014/2015'
 *  }
 *
 * @apiDescription Do not provide Date with the request. Date is provided by
 * default by the api.
 */
router.post('/v1/executives', (req, res) => {
  // console.log("we want to create an executive")
  apostliteS3
    .apostliteUpload(req, res, 'executive[profilePhoto]')
    .then((resp) => {
      const executive = resp.body.executive;
      executive.profilePhoto = resp.data.Location;
      // console.log("updated with file", executive);

      ExecutiveController.createExcecutive(executive)
        .then((data) => {
          res.status(200).json({
            executive: data,
          });
        })
        .catch((err) => {
          res.status(500).json({
            message: 'could not create a new Executive',
            error: err,
          });
        });
    })
    .catch(err => {
      // console.log('caught an error');
      res.status(500).json({
        error: err,
      });
    });
});

/**
 * @api {put} https://apostlite-mongo.herokuapp.com/api/v1/executives/{id} Edit Executive
 * @apiName EditExecutive
 * @apiGroup Executive
 * @apiVersion 1.0.0
 * @apiParam (executives) {Number} id The Executive's  unique id
 * @apiDescription To update a Executive, please provide the new bible_study in a json
 * object and parse it to the api.
 * @apiParam (executives) {object} Executive - The new Executive to be replaced
 * @apiParamExample Example Data The new Executive
 *
 *  {
 *    name:"GRAB FOR KEEPS",
 *    banner :"images/grabs",
 *    theme:"You chance to grab your life time partner",
 *    start_date:"30-04-2018",
 *    end_date:  "31-04-2018",
 *    createdAt: "24-04-2018",
 *    updatedAt: "30-30-2018"
 *  }
 *
*/
router.put('/v1/executives/:id', (req, res) => {
  apostliteS3
    .apostliteUpload(req, res, 'executive[banner]')
    .then(resp => {
      const executive = req.body.executive;

      if (resp.data.Location) {
        executive.banner = resp.data.Location;
      }
      ExecutiveController.updateExecutive(req.params.id,
        executive, (err, cb) => {
          if (err) {
            res.status(500).json({
              message: 'an error occured while updating the Executive' + err,
              error: err,
              status: Boolean(false),
            });
          } else {
            res.status(200).json({
              message: 'success',
              status: Boolean(true),
              executive: cb,
            });
          }
        });
    })
    .catch(err => {
      res.status(500).json({
        error: err,
      });
    });
});

/**
 * @api {get} https://apostlite-mongo.herokuapp.com/api/v1/executives/{id}  Retrieve  Executive information by Id
 * @apiDescription Get information for only one Executive using the bible_study id.This routes will always return one object if found
 * else it will return an object of Executive (i.e one object containing several Executive).
 * @apiName GetExecutiveById
 * @apiGroup Executive
 * @apiVersion 1.0.1
 *
 * @apiExample {js} Get Executive By Id
 * https://apostlite-mongo.herokuapp.com/api/v1/executives/{id}
 *
 * Get one record with the ID that has been provided. If not found, the api returns with failure.
 *
 *@apiSuccessExample Example data on success for Executive

* {
*   "executive": {
*       "_id": "5c67bdefaf9fa35230f0503e",
*       "profile_photo": "https://pbs.twimg.com/profile_images/1055795133185179651/HMBnFwQ0_400x400.jpg",
*       "firsname": "Samira",
*      "lastName": "Bawumia",
*      "office_position": "General Secretary",
*      "bio": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
*      "year_group": "2014/2015",
*   }
 * }
 *
 *@apiExample {js} Get Executive With wing
 *https://apostlite-mongo.herokuapp.com/api/v1/executives/{id}?include=wing
 *
 *Get one record with the wing details pre-populated with the request. When found, the api
 *returns the Executive details and the wing details in the same request.
 *
 *
 * @apiSuccess {Object} wing This is an object containing the details of the wing that owns the Executive.
 * If the Executive does not belong to any wing, wing is 'null'
 *  @apiSuccessExample   Example data for Executive With wing
 * "executive": {
*       "_id": "5c67bdefaf9fa35230f0503e",
*       "profile_photo": "https://pbs.twimg.com/profile_images/1055795133185179651/HMBnFwQ0_400x400.jpg",
*       "firsname": "Samira",
*      "lastName": "Bawumia",
*      "office_position": "General Secretary",
*      "bio": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
*      "year_group": "2014/2015",
*        "wing": {
*            "createdAt": "2019-02-04T10:29:47.508Z",
*            "_id": "5c58141bb0c18a26af9fa678",
*            "name": "Webteam",
*           "slogan": "testing",
*           "description": "testiing",
*           "logo": "",
*        },
 * }
 * @apiError DatabaseError An error occured while fetching the records from the database
 */
router.get('/v1/executives/:id', (req, res) => {
  const query = req.query;

  const options = {
    id: req.params.id,
    include: ''
  };

  if (query.include && query.include != '' && query.include != null) {
    options.include = query.include;
  }

  ExecutiveController.getExecutiveById(options)
    .then((data) => {
      res.status(200).json({
        executive: data,
      });
    })
    .catch((err) => {
      res.status(500).json({
        message: 'an error occured while fetching the Executive' + err,
        error: err,
        status: Boolean(false),
      });
    });
});

/**
 * @api {delete} https://apostlite-mongo.herokuapp.com/api/v1/executives/{id} Delete Executive
 * @apiName DeleteExecutive
 * @apiGroup Executive
 * @apiVersion 1.0.0
 * @apiDescription To delete an object, you need to provide the unique id of the object.
 * @apiSampleRequestExample /api/v1/executives/df45adghs5h14
 * @apiSuccess {String} message Returns success if the delete was successful
 * @apiSuccess {Boolean} status Returns true if the delete was successful.
 * @apiSuccessExample {json} success response
 * {
 *    message:"success",
 *    status: "true"
 * }
 * @apiError Error An error occured while adding the Executive.
 * @apiErrorExample {object} Error-Response: an error occured while deleting the Executive.
 *
 * {
 *   message: "an error occured while adding the Executive",
 *   error: "the type  of error that occured",
 *   status: "false"
 * }
 */
router.delete('/v1/executives/:id', (req, res) => {
  ExecutiveController.deleteExecutive(req.params.id, (err, cb) => {
    if (err) {
      res.json({
        message: 'an error occured while removing the Executive',
        error: err,
        status: Boolean(false),
      });
    } else {
      res.json({
        message: 'success',
        status: cb,
        statusCode: res.statusCode,
      });
    }
  });
});

module.exports = router;
