'use strict';
const express = require('express');
const router = express.Router();

const PatronController = require('../controllers/patron');

router.get('/v1/patrons', function(req, res) {
  PatronController.getAllPatrons()
    .then((patrons) => {
      res.status(200).json(patrons);
    })
    .catch((err) => {
      res.status(501).json({err});
    });
});

router.post('/v1/patrons', function(req, res) {
  const {patron} = req.body;
  if (patron !== null || patron != 'undefined') {
    PatronController.createPatron()
      .then((data) => {
        res.status(200).json({patron: data});
      })
      .catch((err) => res.status(500).json({err}));
  }
});

module.exports = router;
