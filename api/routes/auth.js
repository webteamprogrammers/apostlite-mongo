'use strict';

const express = require('express');

const router = express.Router();

const jwt = require('jsonwebtoken');

const config = require('../config/config');

const passport = require('passport');

const userController = require('../controllers/user');

/* POST login */
// router.post('/login',(req,res,next)=>{
//   passport.authenticate('local',{session:false}, (err,user,info)=>{
//     if(err){
//       return res.status(400).json({
//         message:'something is not right',
//         user:user
//       });
//     }else if(!user){
//       return res.status(400).json({
//         message:'sorry! user not found',
//         user:user
//       });
//     }
//     req.login(user,{session:false},(err)=>{
//       if(err)
//       res.send(err);
//       //generate a signed json web token with the contents ofthe user
//       //object and return it in the response.

//       const token  =  jwt.sign(user,'use_my_secret.eben_kb');
//       return res.json({user,token});
//     });
//   })(req,res);
// });

router.post('/login', (req, res) => {
  console.log('This is the request body:', req.body);
  const email = req.body.email;
  const password = req.body.password;
  let token = null;
  if (email && password) {
    userController
      .getUser(email, password)
      .then(user => {
        if (user) {
          token = jwt.sign({id: user.id}, config.secret, {
            expiresIn: 86400, // expires in 24 hours
          });

          userController.saveToken(user._id, token).then(() => {
            res.status(200).json({
              msg: 'User found',
              auth: true,
              token: token,
              // user:user
            });
          });
        } else {
          res.status(401).json({
            msg: 'No user was found...',
          });
        }
      })
      .catch(err => {
        res.json({err: 'An error occured: ' + err});
      });
  } else {
    res.status(501).json('Error : Undefined parameters');
  }
});

// router.post('/sign-up',(req,res) => {

// })

module.exports = router;
