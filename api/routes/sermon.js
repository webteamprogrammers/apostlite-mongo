'use strict';

const express = require('express');
const router = express.Router();
const apostliteS3 = require('../controllers/apostliteS3');
const sermonController = require('../controllers/sermon');

/**
 * @api {get} https://nupsg-apostlite.herokuapp.com/api/v1/sermons  Retrieve all sermons
 *
 * @apiDescription When found, the api will return every sermons record found in the database.
 * If you want to retrieve only specific items, add query parameters to the api request.
 * You can obtain old sermons items by specifying the limit, last id and adding the order=asc
 * This allows you to retrieve sermons items up to the limit created before the ID that has been specified.
 * @apiName GetSermon
 * @apiGroup Sermon
 * @apiVersion 1.0.1
 * @apiParam (limit)   {Number} limit  (optional query param  This is the amount of records that should be returned by the api.
 * It defaults to 100 and the minimum limit that you can parse is 1.
 *
 * @apiParam (order)   {Number} order  (optional query param) This parameter tells the api to sort the records in ascending or descending order.
 * Use {asc} for ascending and {des} for descending
 *
 * When not provided the order defaults to ascending.
 * @apiParam (last_id) {Object_Id} last_id The id of the last item in the previous search result.
 * When you add the last_id option, the api starts the search from the last_id that you provide.
 *
 * You can use the {last_id} with {asc} or {des}.
 * When you use {asc} the api starts from the {last_id} you have provided and search for items created earlier than the last_id you have provided.
 * Wehn you use {des} the api starts from the {last_id} you have provided and search for items created after the last_id that you have provided.
 *
 *
 *
 * @apiExample {js} Get sermons By Asc
 *https://nupsg-apostlite.herokuapp.com/api/v1/sermons?order=asc
 *          Retrieves all items from the database in ascending order.
 *
 * @apiExample {js} Get sermons By Des
 * https://nupsg-apostlite.herokuapp.com/api/v1/sermons?order=des
 *          The request retrieves all items from the database in descending order.
 *
 * @apiExample {js}  Get sermons By Limit
 * https://nupsg-apostlite.herokuapp.com/api/v1/sermons?limit=20
 *          The request retrieves only the first twenty items from the database in ascending order.
 *
 * @apiExample {js}  Get sermons By Last_id
 * https://nupsg-apostlite.herokuapp.com/api/v1/sermons?limit=20&last_id=de23ab32009a2b&order=asc
 *          The request retrieves only the first twenty items created before the {last_id} that you have provided.
 *
 * @apiExample {js} Get sermons With options
 * https://nupsg-apostlite.herokuapp.com/api/v1/sermons?limit=20&last_id=de23ab32009a2b&order=des
 *          The request retrieves only the first twenty items created after the {last_id} that you have provided.
 *
 * @apiSuccess {String} title   The Headline or Title for the sermons
 * @apiSuccess {String} content The actual content of the sermons
 * @apiSuccess {Date}   createdAt The date when the sermons was created
 * @apiSuccess {Date}   updatedAt The date for the most current update of the sermons.
 * @apiSuccessExample   Example data on success
 * {
 *  {
 *    title:"Leavers' service",
 *    minister:"Rev. Dr. J.E.T",
 *    scriptures:[
 *          "Genesis 1v4", "Mathew 6:31" , "Mathew 5 v 8"
 *      ],
 *    message :"You are being encourged to attend the service' on sunday at ccb. come and be blessed.",
 *    banner:"images/sermon/1.jpg"
 *    createdAt:"24-04-2018",
 *    updatedAt:"30-30-2018"
 *  }
 * }
 *
 */

router.get('/v1/sermons', function(req, res) {
  const query = req.query;
  const options = {
    pageNo: 1,
    limit: 100,
    last_id: null,
    /**
      the default sorting order is descending (returns the new items first)
      this can be in two possible states "1 = asc" for ascending and "-1 = des" for descending
    **/
    order: -1,
  };

  if (query.order) {
    if (query.order.trim().toLowerCase() == 'asc') {
      options.order = 1;
    }
  }

  if (parseInt(req.query.pageNo) && parseInt(req.query.pageNo) > 0)
    options.pageNo = parseInt(req.query.pageNo);

  if (parseInt(req.query.limit) > 0)
    options.limit = parseInt(req.query.limit.trim());

  if (query.last_id) {
    options.last_id = query.last_id;
  }

  sermonController.getAllSermon(options, function(err, sermons) {
    if (err) {
      res.status(500).json({
        message: 'an error occured while fetcing sermon',
        error: err,
      });
    } else {
      res.status(200).json({
        message: 'success',
        sermons: sermons,
      });
    }
  });
});

/**
 * @api {get} https://apostlite-mongo.herokuapp.com/api/v1/sermons/{id}  Retrieve  sermon information by Id
 * @apiDescription Get information for only one wing using the sermon id.This routes will always return one object if found
 * else it will return an object of sermon (i.e one object containing several sermon).
 * @apiName GetSermon
 * @apiGroup Sermon
 * @apiVersion 1.0.0
 * @apiError DatabaseError An error occured while fetching the records from the database
 */
router.get('/v1/sermons/:id', (req, res) => {
  sermonController.getSermonById(req.params.id, (err, sermon) => {
    if (err) {
      res.json({
        message: 'an error occured while fetcing sermon',
        error: err,
        status: Boolean(false),
      });
    } else {
      res.json({
        message: 'success',
        sermon: sermon,
        status: Boolean(true),
      });
    }
  });
});

/**
 * @api {post} https://apostlite-mongo.herokuapp.com/api/v1/sermons  Create Sermon
 * @apiName CreateSermon
 * @apiGroup Sermon
 * @apiVersion 1.0.0
 * @apiSuccess {object} bible_Study The Sermon to create
 * @apiParamExample  {json} Sample-Request:
 *
 *  {
 *    title:"Leavers' service",
 *    minister:"Rev. Dr. J.E.T",
 *    scriptures':[
 *          "Genesis 1v4", "Mathew 6:31" , "Mathew 5 v 8"
 *      ],
 *    message :"You are being encourged to attend the service' on sunday at ccb. come and be blessed.",
 *    banner:"images/sermon/1.jpg"
 *    createdAt:"24-04-2018",
 *    updatedAt:"30-30-2018"
 *  }
 *
 *
 * @apiDescription Do not provide Date with the request. Date is provided by
 * default by the api.
 */
router.post('/v1/sermons', (req, res) => {
  apostliteS3
    .apostliteUpload(req, res, 'sermon[banner]')
    .then(resp => {
      const sermon = req.body.sermon;
      sermon.banner = resp.data.Location;

      sermonController.createNewSermon(sermon, (err, cb) => {
        if (err) {
          res.json({
            message: 'could not create a new sermon',
            error: err,
            status: Boolean(false),
          });
        } else {
          res.json({
            message: 'success! sermon has been created',
            status: Boolean(true),
            sermon: cb,
          });
        }
      });
    })
    .catch(err => {
      res.status(500).json({
        error: err,
      });
    });
});

/**
 * @api {put} https://apostlite-mongo.herokuapp.com/api/v1/sermons/{id} Edit Sermon
 * @apiName EditSermon
 * @apiGroup Sermon
 * @apiVersion 1.0.0
 * @apiParam (Sermon) {Number} id The Sermon's unique id
 * @apiDescription To update a Sermon, please provide the new bible_study in a json
 * object and parse it to the api.
 * @apiParam (Sermon) {object} bible_study - The new sermon to be replaced
 * @apiParamExample Example Data The new sermon
 *
 *{
 *    title:"Leavers' service",
 *    minister:"Rev. Dr. J.E.T",
 *    scriptures:[
 *          "Genesis 1v4", "Mathew 6:31" , "Mathew 5 v 8"
 *      ],
 *    message :"You are being encourged to attend the service' on sunday at ccb. come and be blessed.",
 *    banner:"images/sermon/1.jpg"
 *
 *  }
 *
 */
router.put('/v1/sermons/:id', (req, res) => {
  apostliteS3.apostliteUpload(req, res, 'sermon[banner]').then(resp => {
    const sermon = req.body.sermon;
    if (resp.data.Location) {
      sermon.banner = resp.data.Location;
    }
    sermonController.updateSermon(req.params.id, sermon, (err, cb) => {
      if (err) {
        res.status(500).json({
          message: 'an error occured while updating sermon',
          error: err,
        });
      } else {
        res.status(200).json({
          sermon: cb,
        });
      }
    });
  });
});

/**
 * @api {delete} https://apostlite-mongo.herokuapp.com/api/v1/sermons/{id} Delete Sermon
 * @apiName DeleteSermon
 * @apiGroup Sermon
 * @apiVersion 1.0.0
 * @apiDescription To delete an object, you need to provide the unique id of the object.
 * @apiSampleRequestExample /api/v1/sermons/df45adghs5h14
 * @apiSuccess {String} message Returns success if the delete was successful
 * @apiSuccess {Boolean} status Returns true if the delete was successful.
 * @apiSuccessExample {json} success response
 * {
 *    message:"success",
 *    status : "true"
 * }
 * @apiError An error occured while adding the Sermon.
 * @apiErrorExample {object} Error-Response: an error occured while deleting the sermon.
 *
 * {
 *    message: "an error occured while adding the sermon",
 *    error  : "the type  of error that occured",
 *    status : "false"
 * }
 */
router.delete('/v1/sermons/:id', (req, res) => {
  sermonController.deleteSermon(req.params.id, (err, cb) => {
    if (err) {
      res.json({
        message: 'an error occured while removing the sermon',
        error: err,
        status: Boolean(false),
      });
    } else {
      res.json({
        message: 'success',
        status: cb,
      });
    }
  });
});

module.exports = router;
