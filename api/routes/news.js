'use strict';

const express = require('express');

const router = express.Router();

const url = require('url');

const newsController = require('../controllers/news');

const authController = require('../controllers/auth');

/**
 * @api {get} https://nupsg-apostlite.herokuapp.com/api/v1/news  Retrieve all News
 *
 * @apiDescription When found, the api will return every news record found in the database.
 * If you want to retrieve only specific items, add query parameters to the api request.
 * You can obtain old news items by specifying the limit, last id and adding the order=asc
 * This allows you to retrieve news items up to the limit created before the ID that has been specified.
 * @apiName GetNews
 * @apiGroup News
 * @apiVersion 1.0.1
 * @apiParam (limit)   {Number} limit  (optional query param  This is the amount of records that should be returned by the api.
 * It defaults to 100 and the minimum limit that you can parse is 1.
 *
 * @apiParam (order)   {Number} order  (optional query param) This parameter tells the api to sort the records in ascending or descending order.
 * Use {asc} for ascending and {des} for descending
 *
 * When not provided the order defaults to ascending.
 * @apiParam (last_id) {Object_Id} last_id The id of the last item in the previous search result.
 * When you add the last_id option, the api starts the search from the last_id that you provide.
 *
 * You can use the {last_id} with {asc} or {des}.
 * When you use {asc} the api starts from the {last_id} you have provided and search for items created earlier than the last_id you have provided.
 * Wehn you use {des} the api starts from the {last_id} you have provided and search for items created after the last_id that you have provided.
 *
 *
 *
 * @apiExample {js} Get News By Asc
 *https://nupsg-apostlite.herokuapp.com/api/v1/news?order=asc
 *          Retrieves all items from the database in ascending order.
 *
 * @apiExample {js} Get News By Des
 * https://nupsg-apostlite.herokuapp.com/api/v1/news?order=des
 *          The request retrieves all items from the database in descending order.
 *
 * @apiExample {js}  Get News By Limit
 * https://nupsg-apostlite.herokuapp.com/api/v1/news?limit=20
 *          The request retrieves only the first twenty items from the database in ascending order.
 *
 * @apiExample {js}  Get News By Last_id
 * https://nupsg-apostlite.herokuapp.com/api/v1/news?limit=20&last_id=de23ab32009a2b&order=asc
 *          The request retrieves only the first twenty items created before the {last_id} that you have provided.
 *
 * @apiExample {js} Get News With options
 * https://nupsg-apostlite.herokuapp.com/api/v1/news?limit=20&last_id=de23ab32009a2b&order=des
 *          The request retrieves only the first twenty items created after the {last_id} that you have provided.
 *
 * @apiSuccess {String} title   The Headline or Title for the news
 * @apiSuccess {String} content The actual content of the news
 * @apiSuccess {Date}   createdAt The date when the news was created
 * @apiSuccess {Date}   updatedAt The date for the most current update of the news.
 * @apiSuccessExample   Example data on success
 * {
 *  message     : "success",
 *  {
 *    title     : "Leavers' service",
 *    content   : "You are being encourged to attend the leavers' service this sunday at ccb. come and be blessed.", *
 *    createdAt : "24-04-2018",
 *    updatedAt : "30-30-2018"
 *  },
 *  statusCode  : 200
 * }
 *
 */

router.get('/v1/news', function(req, res) {
  // console.log("This is the token: " + req.headers.token);
  authController.verifyToken(req.headers.token);
  const query = req.query;
  const options = {
    pageNo: 1,
    limit: 100,
    last_id: null,
    /**""
      the default sorting order is descending (returns the new items first)
      this can be in two possible states "1 = asc" for ascending and "-1 = des" for descending
    **/
    order: -1,
  };

  if (query.order) {
    if (query.order.trim().toLowerCase() == 'asc') {
      options.order = 1;
    }
  }

  if (parseInt(req.query.pageNo) > 0)
    options.pageNo = parseInt(req.query.pageNo);

  if (parseInt(req.query.limit) > 0)
    options.limit = parseInt(req.query.limit.trim());

  if (query.last_id) {
    options.last_id = query.last_id;
  }

  newsController.getAllNews(options, function(err, news) {
    if (err) {
      res.status(500).json({
        message: 'an error occured while fetcing news',
        error: err
      });
    } else {
      res.status(200).json({
        message: 'success',
        meta: options,
        news: news,
      });
    }
  });
});

/**
 * @api {get} https://apostlite-mongo.herokuapp.com/api/v1/news/{id}  Retrieve  news information by Id
 * @apiDescription Get information for only one news using the news id.This routes will always return one object if found
 * else it will return an object of news (i.e one object containing several news).
 * @apiName GetNewsById
 * @apiGroup News
 * @apiVersion 1.0.0
 * @apiError DatabaseError An error occured while fetching the records from the database
 */
router.get('/v1/news/:id', (req, res) => {
  const id = req.params.id;
  newsController.getNewsById(id, (err, news) => {
    if (err) {
      res.status(500).json({
        message: 'an error occurred while fetcing news',
        error: err,
        status: Boolean(false),
        statusCode: res.statusCode,
      });
    } else {
      res.status(200).json({
        message: 'success',
        news: news,
        statusCode: res.statusCode,
      });
    }
  });
});

/**
 * @api {post} https://apostlite-mongo.herokuapp.com/api/v1/news  Create News
 * @apiName CreateNews
 * @apiGroup News
 * @apiVersion 1.0.0
 * @apiSuccess {object} bible_Study The News to create
 * @apiParamExample  {json} Sample-Request:
 *
 *  {
 *    title :"SCC SUNDAY", *
 *   content :"Coming sunday is SCC sunday.There will be no service at CCB.Thank you",
 *
 *  }
 *
 *
 * @apiDescription Do not provide Date with the request. Date is provided by
 * default by the api.
 *
 */
router.post('/v1/news', (req, res) => {
  const {news} = req.body;
  newsController.createNews(news, (err, cb) => {
    if (err) {
      res.json({
        message: 'could not create news' + err,
        error: err,
        status: Boolean(false),
      });
    } else {
      res.json({
        message: 'success! news has been created',
        status: Boolean(true),
        news: cb
      });
    }
  });
});

/**
 * @api {put} https://apostlite-mongo.herokuapp.com/api/v1/news/{id} Edit News
 * @apiName EditNews
 * @apiGroup News
 * @apiVersion 1.0.0
 * @apiParam (News) {Number} id The News' unique id
 * @apiDescription To update a News, please provide the new bible_study in a json
 * object and parse it to the api.
 * @apiParam (News) {object} bible_study - The new news to be replaced
 * @apiParamExample Example Data The new news
 *
 *{
 *    title :"Something to describe the Bible study",
 *    content:"Walking in a spirit filled life. Life that will cause you to impact in the lives of other. That is what the bible teaches and encourages.",
 *
 *  }
 *
 */

router.put('/v1/news/:id', (req, res) => {
  newsController.updateNews(req.params.id, req.body.news, (err, cb) => {
    if (err) {
      res.status(500).json({
        message: 'an error occured while updating the news',
        error: err,
        status: Boolean(false),
        news: cb,
      });
    } else {
      res.json({
        message: 'success',
        status: Boolean(true),
      });
    }
  });
});

/**
 * @api {delete} https://apostlite-mongo.herokuapp.com/api/v1/news/{id} Delete News
 * @apiName DeleteNews
 * @apiGroup News
 * @apiVersion 1.0.0
 * @apiDescription To delete an object, you need to provide the unique id of the object.
 * @apiSampleRequestExample /api/v1/news/df45adghs5h14
 * @apiSuccess {String} message Returns success if the delete was successful
 * @apiSuccess {Boolean} status Returns true if the delete was successful.
 * @apiSuccessExample {json} success response
 * {
 *      message:"success",
 *      status: "true"
 * }
 * @apiError Error An error occured while adding the News.
 * @apiErrorExample {object} Error-Response: an error occured while deleting the news.
 *
 * {
 *    message : "an error occured while adding the news",
 *    error   : "the type  of error that occured",
 *    status  : "false"
 * }
 */
router.delete('/v1/news/:id', (req, res) => {
  newsController.deleteNews(req.params.id, (err, cb) => {
    if (err) {
      res.json({
        message: 'an error occured while removing the news',
        error: err,
        status: Boolean(false),
      });
    } else {
      res.json({
        message: 'success',
        status: cb,
      });
    }
  });
});

module.exports = router;
