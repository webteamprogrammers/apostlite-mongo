'use strict';

const express = require('express');

const router = express.Router();

const apostliteS3 = require('../controllers/apostliteS3');

const programController = require('../controllers/programs');

/**
 * @api {get} https://nupsg-apostlite.herokuapp.com/api/v1/programs  Retrieve all programs
 *
 * @apiDescription When found, the api will return every programs record found in the database.
 * If you want to retrieve only specific items, add query parameters to the api request.
 * You can obtain old programs items by specifying the limit, last id and adding the order=asc
 * This allows you to retrieve programs items up to the limit created before the ID that has been specified.
 * @apiName GetPrograms
 * @apiGroup Program
 * @apiVersion 1.0.1
 * @apiParam (limit)   {Number} limit  (optional query param  This is the amount of records that should be returned by the api.
 * It defaults to 100 and the minimum limit that you can parse is 1.
 *
 * @apiParam (order)   {Number} order  (optional query param) This parameter tells the api to sort the records in ascending or descending order.
 * Use {asc} for ascending and {des} for descending
 *
 * When not provided the order defaults to ascending.
 * @apiParam (last_id) {Object_Id} last_id The id of the last item in the previous search result.
 * When you add the last_id option, the api starts the search from the last_id that you provide.
 *
 * You can use the {last_id} with {asc} or {des}.
 * When you use {asc} the api starts from the {last_id} you have provided and search for items created earlier than the last_id you have provided.
 * Wehn you use {des} the api starts from the {last_id} you have provided and search for items created after the last_id that you have provided.
 *
 * @apiParam (withOwner) {Boolean} Type  (optional query param) User this when you want to fetch programs that belong to a particular wing.
 *    This option defaults to false when not provided.
 *
 *
 * @apiParam (owner) {String} owner (optional query param) Use this option when you want to fetch a program that belons to a particular wing.
 * The data provided should be either church or a string representation of the id of the wing that you want to fetch the program for.
 *  For example, if you want to fetch all programs that belong to the church, you can use owner=church
 *  This option defaults to null when not provided.
 * Note that, anytime you use this option, you have to set "withOwner" to be true
 * @apiExample {js} Get programs By Asc
 *https://nupsg-apostlite.herokuapp.com/api/v1/programs?order=asc
 *          Retrieves all items from the database in ascending order.
 *
 * @apiExample {js} Get programs By Des
 * https://nupsg-apostlite.herokuapp.com/api/v1/programs?order=des
 *          The request retrieves all items from the database in descending order.
 *
 * @apiExample {js}  Get programs By Limit
 * https://nupsg-apostlite.herokuapp.com/api/v1/programs?limit=20
 *          The request retrieves only the first twenty items from the database in ascending order.
 *
 * @apiExample {js}  Get programs By Last_id
 * https://nupsg-apostlite.herokuapp.com/api/v1/programs?limit=20&last_id=de23ab32009a2b&order=asc
 *          The request retrieves only the first twenty items created before the {last_id} that you have provided.
 *
 * @apiExample {js} Get programs With options
 * https://nupsg-apostlite.herokuapp.com/api/v1/programs?limit=20&last_id=de23ab32009a2b&order=des&withOwner={true/false}&owner={owner}
 *          User this endpoint to provide a bunch of options to the query.
 *
 * @apiSuccess {String} title   The Headline or Title for the programs
 * @apiSuccess {String} content The actual content of the programs
 * @apiSuccess {Date}   createdAt The date when the programs was created
 * @apiSuccess {Date}   updatedAt The date for the most current update of the programs.
 * @apiSuccessExample   Example data on success
 * {
 *  {
 *    name       :"GRAB FOR KEEPS",
 *    banner     :"images/grabs",
 *    theme      :"You chance to grab your life time partner",
 *    start_date :"30-04-2018",
 *    end_date   :  "31-04-2018",
 *    createdAt  : "24-04-2018",
 *    updatedAt  : "30-30-2018"
 *  },
 * {
 *    name:"LEAVERS DINNER",
 *    banner :"images/leavers",
 *    theme:"CREATING A LASTING MEMOERY",
 *    start_date:"30-04-2018",
 *    end_date:  "31-04-2018",
 *    createdAt: "24-04-2018",
 *    updatedAt: "30-30-2018"
 *  }
 * }
 *
 */

router.get('/v1/programs', function(req, res) {
  const query = req.query;
  const options = {
    pageNo: 1,
    limit: 100,
    last_id: null,
    /**
      the default sorting order is descending (returns the new items first)
      this can be in two possible states "1 = asc" for ascending and "-1 = des" for descending
    **/
    order: -1,
    owner: 'church',
    withOwner: false,
    month: null
  };
  if (query.order) {
    if (query.order.trim().toLowerCase() == 'asc') {
      options.order = 1;
    }
  }

  if (parseInt(req.query.pageNo > 0))
    options.pageNo = parseInt(req.query.pageNo);

  if (parseInt(req.query.limit) > 0)
    options.limit = parseInt(req.query.limit);

  if (query.last_id)
    options.last_id = query.last_id;

  if (query.owner) {
    if (query.owner.toLowerCase() == 'church')
      options.owner = 'church';
    else options.owner = query.owner;
  }

  if (query.withOwner) {
    if (query.withOwner.toLowerCase() == 'true') {
      options.withOwner = true;
    }
  }

  if (query.month) {
    options.month = parseInt(query.month);
  }

  programController.getAllProgram(options, function(err, programs) {
    if (err) {
      console.log(err);
      res.status(500).json({
        message: 'an error occured while fetching the programs',
        error: err
      });
    } else {
      res.status(200).json({
        message: 'success',
        meta: options,
        programs: programs
      });
    }
  });
});

/**
 * @api {post} https://apostlite-mongo.herokuapp.com/api/v1/programs  Create Program
 * @apiName CreateProgram
 * @apiGroup Program
 * @apiVersion 1.0.0
 * @apiSuccess {object} bible_Study The Program to create
 * @apiParamExample  {json} Sample-Request:
 *
 *  {
 *    name:"GRAB FOR KEEPS",
 *    banner :"images/grabs",
 *    theme:"You chance to grab your life time partner",
 *    start_date:"30-04-2018",
 *    end_date:  "31-04-2018",
 *    createdAt: "24-04-2018",
 *    updatedAt: "30-30-2018"
 *  }
 *
 *
 * @apiDescription Do not provide Date with the request. Date is provided by
 * default by the api.
 */
router.post('/v1/programs/', (req, res) => {
  apostliteS3
    .apostliteUpload(req, res, 'program[banner]')
    .then(resp => {
      const program = resp.body.program;
      program.banner = resp.data.Location;
      if (!program.owner) {
        program.owner = 'church';
      }

      programController.createNewProgram(program, (err, cb) => {
        if (err) {
          res.json({
            message: 'could not create a new program' + err,
            error: err,
            status: Boolean(false),
          });
        } else {
          res.status(200).json({
            program: cb,
          });
        }
      });
    })
    .catch(err => {
      res.status(500).json({
        error: err,
      });
    });

  // programController.createNewProgram(req.body.program,(err,cb)=>{
  //     if(err){
  //       res.json({
  //         'message':"could not create a new program"+err,
  //         'error':err,
  //         'status':Boolean(false)
  //       });
  //     }else{
  //       res.json({
  //         'status':Boolean(true),
  //         'message':"success! program has been created"
  //       });
  //     }
  // });
});

/**
 * @api {put} https://apostlite-mongo.herokuapp.com/api/v1/programs/{id} Edit Program
 * @apiName EditProgram
 * @apiGroup Program
 * @apiVersion 1.0.0
 * @apiParam (programs) {Number} id The program's  unique id
 * @apiDescription To update a program, please provide the new bible_study in a json
 * object and parse it to the api.
 * @apiParam (programs) {object} program - The new program to be replaced
 * @apiParamExample Example Data The new program
 *
 *  {
 *    name:"GRAB FOR KEEPS",
 *    banner :"images/grabs",
 *    theme:"You chance to grab your life time partner",
 *    start_date:"30-04-2018",
 *    end_date:  "31-04-2018",
 *    createdAt: "24-04-2018",
 *    updatedAt: "30-30-2018"
 *  }
 *
*/
router.put('/v1/programs/:id', (req, res) => {
  apostliteS3
    .apostliteUpload(req, res, 'program[banner]')
    .then(resp => {
      const program = req.body.program;

      if (resp.data.Location) {
        program.banner = resp.data.Location;
      }
      programController.updateProgram(req.params.id, program, (err, cb) => {
        if (err) {
          res.status(500).json({
            message: 'an error occured while updating the program' + err,
            error: err,
            status: Boolean(false),
          });
        } else {
          res.status(200).json({
            message: 'success',
            status: Boolean(true),
            program: cb,
          });
        }
      });
    })
    .catch(err => {
      res.status(500).json({
        error: err,
      });
    });
});

/**
 * @api {get} https://apostlite-mongo.herokuapp.com/api/v1/programs/{id}  Retrieve  program information by Id
 * @apiDescription Get information for only one program using the bible_study id.This routes will always return one object if found
 * else it will return an object of program (i.e one object containing several program).
 * @apiName GetProgramById
 * @apiGroup Program
 * @apiVersion 1.0.1
 *
 * @apiExample {js} Get Program By Id
 * https://apostlite-mongo.herokuapp.com/api/v1/programs/{id}
 *
 * Get one record with the ID that has been provided. If not found, the api returns with failure.
 *
 *@apiSuccessExample Example data on success for Program
 *program": {
 *  "createdAt": "2019-02-04T14:56:10.580Z",
 *  "_id": "5c58528abb880351007e6618",
 *  "name": "Watch Night",
 *  "theme": "All are invited for the first watch night service",
 *  "end_date": "2019-02-04T14:56:10.580Z",
 *  "start_date": "2019-02-04T14:56:10.580Z",
 *  "updatedAt":"2019-02-04T14:56:10.580Z",
 *  "owner": "5c58141bb0c18a26af9fa678",
 * }
 *
 *@apiExample {js} Get Program With Owner
 *https://apostlite-mongo.herokuapp.com/api/v1/programs/{id}?include=owner
 *
 *Get one record with the owner details pre-populated with the request. When found, the api
 *returns the program details and the owner details in the same request.
 *
 *
 * @apiSuccess {Object} owner This is an object containing the details of the wing that owns the program.
 * If the program does not belong to any wing, owner is 'null'
 *  @apiSuccessExample   Example data for Program With Owner
 * "program": {
*        "createdAt": "2019-02-04T14:56:10.580Z",
*        "_id": "5c58528abb880351007e6618",
*        "name": "Watch Night",
*        "theme": "All are invited for the first watch night service",
*        "end_date": "2019-02-04T10:29:47.508Z",
*        "start_date": "2019-02-04T10:29:47.508Z",
*        "updatedAt": "2019-02-04T10:29:47.508Z",
*        "owner": {
*            "createdAt": "2019-02-04T10:29:47.508Z",
*            "_id": "5c58141bb0c18a26af9fa678",
*            "name": "Webteam",
*           "slogan": "testing",
*           "description": "testiing",
*           "logo": "",
*        },
 * }
 * @apiError DatabaseError An error occured while fetching the records from the database
 */
router.get('/v1/programs/:id', (req, res) => {
  const query = req.query;

  const options = {
    id: req.params.id,
    includeOwner: false
  };

  if (query.include && query.include != '' && query.include != null) {
    if (query.include.toLowerCase() == 'owner') {
      options.includeOwner = true;
    }
  }

  programController.getProgramById(options, (err, program) => {
    if (err) {
      res.json({
        message: 'an error occured while updating the program' + err,
        error: err,
        status: Boolean(false),
      });
    } else {
      res.json({
        message: 'success',
        status: Boolean(true),
        program: program,
      });
    }
  });
});

/**
 * @api {delete} https://apostlite-mongo.herokuapp.com/api/v1/programs/{id} Delete Program
 * @apiName DeleteProgram
 * @apiGroup Program
 * @apiVersion 1.0.0
 * @apiDescription To delete an object, you need to provide the unique id of the object.
 * @apiSampleRequestExample /api/v1/programs/df45adghs5h14
 * @apiSuccess {String} message Returns success if the delete was successful
 * @apiSuccess {Boolean} status Returns true if the delete was successful.
 * @apiSuccessExample {json} success response
 * {
 *    message:"success",
 *    status: "true"
 * }
 * @apiError Error An error occured while adding the Program.
 * @apiErrorExample {object} Error-Response: an error occured while deleting the program.
 *
 * {
 *   message: "an error occured while adding the program",
 *   error: "the type  of error that occured",
 *   status: "false"
 * }
 */
router.delete('/v1/programs/:id', (req, res) => {
  programController.deleteProgram(req.params.id, (err, cb) => {
    if (err) {
      res.json({
        message: 'an error occured while removing the program',
        error: err,
        status: Boolean(false),
      });
    } else {
      res.json({
        message: 'success',
        status: cb,
        statusCode: res.statusCode,
      });
    }
  });
});

module.exports = router;
