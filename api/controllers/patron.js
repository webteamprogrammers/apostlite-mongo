'use strict';

const Patron = require('../models/patron');

const patronObj = {};

patronObj.getAllPatrons = () => {
  return new Promise((resolve, reject) => {
    Patron.find()
      .then((patrons) => {
        resolve(patrons);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

patronObj.createPatron = (patron) => {
  return new Promise((resolve, reject) => {
    if (patron === null || patron === 'undefined') {
      reject('Patron is empty or undefined');
    } else {
      Patron.create(patron)
        .then((created) => {
          resolve(created);
        }).catch((err) => reject(err));
    }
  });
};

module.exports = patronObj;
