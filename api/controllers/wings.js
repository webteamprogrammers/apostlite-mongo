'use strict';

const mongoose = require('mongoose');

const Wing = require('../models/wing');

const wingObj = {};

const notify = require('../controllers/fcm/fcm');

wingObj.createNewWing = (wing, callback) => {
  Wing.isExisting(wing, (err, existing, cb) => {
    if (err) {
      return callback(err, null);
    } else if (!cb) {
      wing.createdAt = Date.now();
      Wing.create(wing, function(err, created) {
        if (err) return callback(err, null);
        notify('New Wing', `${created.name} has been added to wings`, 'wings');
        return callback(null, created);
      });
    } else if (wing) {
      callback('Wing already exists', false);
    }
  });
};

wingObj.getAllWings = (options, callback) => {
  if (options.last_id) {
    wingObj.getOrderedWings(options)
      .then(data => {
        return callback(null, data);
      })
      .catch(err => {
        return callback(err, null);
      });
  } else {
    Wing.find()
      .sort({_id: options.order})
      .limit(options.limit)
      .then(wings => {
        return callback(null, wings);
      })
      .catch(err => {
        return callback(err, null);
      });
  }
};

wingObj.getOrderedWings = function(options) {
  return new Promise((resolve, reject) => {
    /**
     * @param last_id The id that is used as a pivot or reference to move the db cursor
     * @param order The way in which the items should be sorted. 1 = 'ascending' 2= 'descending'
     * @todo return all wings that were created before the last id provided.
     */
    if (options.order == 1) {
      Wing.find({_id: {$lt: options.last_id}})
        .sort({_id: -1})
        .limit(options.lim)
        .populate(options.include)
        .exec()
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    } else {
      Wing.find({_id: {$gt: options.last_id}})
        .sort({_id: 1})
        .limit(options.lim)
        .populate(options.include)
        .exec((err, data) => {
          if (err) {
            reject(err);
          } else if (!err && data) {
            resolve(data);
          } else resolve('Request couldn\'t get any data');
        });
    }
  });
};

wingObj.getWingWithLimit = (lim, callback) => {
  Wing.find()
    .sort({_id: -1})
    .limit(lim)
    .then(wings => {
      callback(null, wings);
    })
    .catch(err => {
      callback(err, null);
    });
};

wingObj.getWingById = (options, callback) => {
  if (options.id == null || options.id == '')
    return callback('Id cannot be empty', null);
  Wing.findById(options.id)
    .populate(options.include)
    .exec((err, wing) => {
      if (err) {
        callback(err, null);
      } else return callback(null, wing);
    });
};

wingObj.getWingByTitle = function(title, callback) {
  Wing.findByTitle(function(err, wing) {
    if (err) return callback(err, null);

    return callback(null, wing);
  });
};

wingObj.updateWing = (id, newwing, callback) => {
  if (id == null) {
    callback('ID is empty or undefined', null);
  } else {
    Wing.update({_id: id}, newwing, {new: true}, (err, updated) => {
      if (err) {
        callback(err, null);
      } else if (updated.nModified > 0) {
        callback(null, updated);
      } else {
        callback('nothing found to update', null);
      }
    });
  }
};

wingObj.deleteWing = (id, callback) => {
  if (id == null) {
    callback('ID is empty or undefined', null);
  } else {
    Wing.findByIdAndRemove(id, (err, removed) => {
      if (err) {
        callback(err, null);
      } else if (removed) {
        callback(null, true);
      } else {
        callback('No wing found to delete', null);
      }
    });
  }
};

module.exports = wingObj;
