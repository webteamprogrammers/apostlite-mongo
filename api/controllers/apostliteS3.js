'use strict';

// const express = require('express');

const AWS = require('aws-sdk');

const config = require('../config/config');

const fs = require('fs');

const options = {
  // region: "US East (Ohio)",
  accessKeyId: config.accessKeyId,
  secretAccessKey: config.secretAccessKey,
};

const s3 = new AWS.S3(options);

const path = require('path');

const multer = require('multer');

const _new_dest = 'static/upload';

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, _new_dest);
  },
  filename: function(req, file, cb) {
    if (file.originalname.length <= 30) cb(null, file.originalname);
    else {
      const ext = path.extname(file.originalname);
      cb(null, file.originalname.substr(0, 29) + ext);
    }
  },
});

const uploadToS3 = (file, filename) => {
  return new Promise((resolve, reject) => {
    const params = {
      Bucket: 'apostlite',
      Key: filename,
      Body: file,
    };
    s3.upload(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        try {
          fs.unlinkSync('static/upload/' + filename);
        } catch (err) {
          reject('an error occured while deleting the file' + err);
        }
        resolve(data);
      }
    });
  });
};

const apostliteUpload = (req, res, fieldName) => {
  return new Promise((resolve, reject) => {
    const upload = initMulter(fieldName);
    upload(req, res, err => {
      if (!req.file) {
        resolve({
          data: 'There is no file',
          body: req.body,
          hasFile: Boolean(false),
        });
      } else {
        if (err) reject(err);
        else {
          fs.readFile(req.file.path, (err, data) => {
            if (err) reject(err);
            else
              uploadToS3(data, req.file.filename).then((data) => {
                resolve({
                  data: data,
                  body: req.body,
                  hasFile: Boolean(true),
                });
              });
          });
        }
      }
    });
  });
};

const deleteFile = file => {
  return new Promise((resolve, reject) => {
    if (!file) {
      reject(new Error('There is no file provided'));
    } else {
      file = file.split('/').slice(-1)[0];
      s3.deleteObject(
        {
          Bucket: 'apostlite',
          Key: file,
        },
        (err, data) => {
          if (err) {
            reject(err);
          } else {
            resolve(`The file has been deleted. ${data}`);
          }
        }
      );
    }
  });
};

function initMulter(fieldName) {
  const upload = multer({
    storage: storage,
    limits: {fileSize: 10 * 1024 * 1024},

    fileFilter: function(req, file, callback) {
      const ext = path.extname(file.originalname);
      if (ext == '.pdf') {
        return callback('Only image files are allowed', null);
      }
      callback(null, true);
    },
  }).single(fieldName);
  return upload;
}

const S3Obj = {
  apostliteUpload: apostliteUpload,
  deleteFile: deleteFile,
};
module.exports = S3Obj;
