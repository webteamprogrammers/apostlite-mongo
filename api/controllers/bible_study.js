'use strict';

const mongoose = require('mongoose');

const Bible_Study = require('../models/study');
const Bible_Study_Book = require('../models/book');

const notify = require('../controllers/fcm/fcm');

const bible_studyObj = {};
/**
 * @todo create new bible studies
 */
bible_studyObj.createNewBible_Study = (bible_study, callback) => {
  Bible_Study.isExisting(bible_study, (err, existing, cb) => {
    if (err) {
      return callback(err, null);
    } else if (!cb) {
      bible_study.createdAt = Date.now();
      Bible_Study.create(bible_study, (err, created) => {
        if (err) {
          callback(err, null);
        } else {
          // update the book that owns this study
          Bible_Study_Book.findById(bible_study.book_id, (err, book) => {
            if (!err && book) {
              book.studies.push(created._id);
              book.save()
                .then(() => {
                  callback(null, created);
                });
              // send notification to devices
              notify('New Bible Study', created.title, 'bible-studies');
            }
          });
        }
      });
    } else if (existing) {
      callback('Bible_Study already exists', false);
    }
  });
};

/**
 * @todo retrieve all bible studies
 */
bible_studyObj.getAllBible_Study = (options, callback) => {
  if (options.query.month) {
    const month = parseInt(options.query.month);
    Bible_Study.aggregate([
      {
        $addFields: {
          month: {$month: '$createdAt'},
        }
      },
      {
        $match: {
          $and: [{
            'month': month,
          }]
        }
      },
    ])
      .sort({_id: options.order})
      .limit(1)
      .then((data) => callback(null, data));
  } else {
    Bible_Study.find({})
      .sort({_id: options.order})
      .limit(options.limit)
      .then(data => {
        return callback(null, data);
      })
      .catch(err => {
        return callback(err, null);
      });
  }
};

/**
 * @todo paginate bible studies
 */
bible_studyObj.getOrderedBibleStudy = function(order, lim, last_id) {
  return new Promise((resolve, reject) => {
    if (order == 1) {
      Bible_Study.find({_id: {$lt: last_id}})
        .sort({_id: -1})
        .limit(lim)
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    } else {
      Bible_Study.find({_id: {$gt: last_id}})
        .sort({_id: 1})
        .limit(lim)
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    }
  });
};

/**
 * Retrieve bible studies by limit
 */
bible_studyObj.getAllBible_StudyWithLimit = (lim, callback) => {
  Bible_Study.find()
    .sort({_id: -1})
    .limit(lim)
    .then(data => {
      callback(null, data);
    })
    .catch(err => {
      callback(err, null);
    });
};

/**
 * Retrieve bible studies by id
 */
bible_studyObj.getBible_StudyById = (id, callback) => {
  if (id == null) return callback('ID is null or undefined', null);
  Bible_Study.findById({_id: id}, (err, found) => {
    if (err) return callback(err, null);

    return callback(null, found);
  });
};

/**
 * Retreive bible studies by title
 */
bible_studyObj.getBible_StudyByTitle = function(title, callback) {
  Bible_Study.findByTitle(function(err, bible_study) {
    if (err) return callback(err, null);

    return callback(null, bible_study);
  });
};

/**
 * Get information about bible studies
 */
bible_studyObj.getMetaData = function() {
  return new Promise((resolve, reject) =>{
    Bible_Study.find({}, 'sem year')
      .then((data) =>{
        resolve(data);
      })
      .catch((err) => {
        reject((err));
      });
  });
};

/**
 * update an existing bible study by providing an id
 */
bible_studyObj.updateBible_Study = (options, callback) => {
  if (options.id == null) {
    callback('ID is empty or undefined', null);
  } else {
    Bible_Study.findByIdAndUpdate({_id: options.id}, options.study, {new: true},
      (err, updated) => {
        if (err) {
          callback(err, null);
        } else if (updated.nModified > 0) {
          callback(null, updated);
        } else {
          callback('nothing found to update', null);
        }
      });
  }
};

/**
 * Delete an existing bible study by providing an id
 */
bible_studyObj.deleteBible_Study = (id, callback) => {
  if (id == null) {
    callback('ID is empty or undefined', null);
  } else {
    Bible_Study.findByIdAndRemove(id, (err, removed) => {
      if (err) {
        callback(err, null);
      } else if (removed) {
        Bible_Study_Book.findById({_id: removed.book_id}, (err, book) => {
          book.studies.pop(id);
          book.save()
            .then(()=> callback(null, removed));
        });
      } else {
        callback('No bible_study found to delete', null);
      }
    });
  }
};

module.exports = bible_studyObj;
