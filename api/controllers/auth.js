'use strict';

const jwt = require('jsonwebtoken');
const config = require('../config/config');
const authController = {
  verifyToken(token) {
    return new Promise((resolve, reject) =>{
      jwt.verify(token, config.secret, (err, data) => {
        if ((data != 'undefined') || (data != null)) {
          resolve(true);
        } else reject(false);
      });
    });
  }
};

module.exports = authController;
