'use strict';

const Meeting = require('../models/meeting');
const Wing = require('../models/wing');

const meetingObj = {};

meetingObj.createMeeting = ((meeting) => {
  return new Promise((resolve, reject) => {
    console.log("this is the meeting we want to create", meeting);
    Meeting.create(meeting)
      .then((data) => {
        console.log("we have created a new meeting", data);
        // update the wing that owns the meeting
        Wing.findById({_id: meeting.wing_id}, (err, foundWing) =>{
          foundWing.meetings.push(data._id);
          foundWing.save()
            .then(() => {
              resolve(data);
            });
        });
      })
      .catch((err) => {
        reject(err);
      });
  });
});

module.exports = meetingObj;
