'use strict';

const News = require('../models/news');

const notify = require('../controllers/fcm/fcm');

const newsObj = {};

newsObj.createNews = (news, callback) => {
  if (news == null) return callback('News is empty or undefined', null);
  News.isExisting(news, (err, existing, cb) => {
    if (err) {
      callback(err, null);
    } else if (!cb) {
      news.createdAt = Date.now();
      News.create(news, function(err, created) {
        if (err) return callback(err, null);

        // send notification to subscribed devices
        if (news.deliveryStatus == 1) {
          notify(created.title, 'News item has been added', 'news');
        }
        // notify(created.title, 'News item has been added', 'news');
        return callback(null, created);
      });
    } else if (existing) {
      callback('News already exists', false);
    }
  });
};

newsObj.getNewsById = (id, callback) => {
  if (id == null) return callback('ID is empty or undefined', null);
  News.findById({_id: id})
    .then(news => {
      return callback(null, news);
    })
    .catch(err => {
      return callback(err, null);
    });
};

newsObj.getAllNews = (options, callback) => {
  if (options.last_id) {
    newsObj.getOrderedNews(options.order, options.limit, options.last_id)
      .then(data => {
        return callback(null, data);
      })
      .catch(err => {
        return callback(err, null);
      });
  } else {
    News.find({deliveryStatus: '1', status: '1'})
      .sort({_id: options.order})
      .limit(options.limit)
      .then(news => {
        return callback(null, news);
      })
      .catch(err => {
        return callback(err, null);
      });
  }
};

newsObj.getOrderedNews = function(order, lim, last_id) {
  return new Promise((resolve, reject) => {
    // find all items that were created before the last_id
    if (order == 1) {
      News.find({_id: {$lt: last_id}, deliveryStatus: '1', status: '1'})
        .sort({_id: -1}) // return the items in the reverse order in which they were found
        .limit(lim)
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    } else {
      // find all items that are created after the last_id
      News.find({_id: {$gt: last_id}, deliveryStatus: '1', status: '1'})
        .sort({_id: 1}) // return the items in the natural sorted order in which you found them
        .limit(lim) // return only the number of items in the limit
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    }
  });
};

newsObj.getNewsByWithLimit = function(lim, callback) {
  News.find({deliveryStatus: '1', status: '1'})
    .sort({_id: -1})
    .limit(lim)
    .then(news => {
      callback(null, news);
    })
    .catch(err => {
      callback(err, null);
    });
};

newsObj.getNewsByTitle = function(title, callback) {
  News.findByTitle(function(err, news) {
    if (err) return callback(err, null);

    return callback(null, news);
  });
};

newsObj.updateNews = (id, newnews, callback) => {
  if (id == null) {
    callback('ID is empty or undefined', null);
  } else {
    News.findByIdAndUpdate({_id: id}, newnews, {new: true}, (err, updated) => {
      if (err) {
        callback(err, null);
      } else {
        callback(null, updated);
      }
    });
  }
};

newsObj.deleteNews = (id, callback) => {
  if (id == null) {
    callback('ID is empty or undefined', null);
  } else {
    News.findByIdAndRemove(id, (err, removed) => {
      if (err) {
        callback(err, null);
      } else if (removed) {
        callback(null, true);
      } else {
        callback('No news found to delete', null);
      }
    });
  }
};

module.exports = newsObj;
