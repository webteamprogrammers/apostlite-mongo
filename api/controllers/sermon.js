'use strict';

const mongoose = require('mongoose');

const Sermon = require('../models/sermon');

const sermonObj = {};

const apostliteS3 = require('../controllers/apostliteS3');

const notify = require('../controllers/fcm/fcm');

sermonObj.createNewSermon = (sermon, callback) => {
  // check if sermon is already existing
  Sermon.isExisting(sermon, (err, existing, cb) => {
    if (err) {
      return callback(err, null);
    } else if (!cb) {
      sermon.createdAt = Date.now();
      Sermon.create(sermon, function(err, created) {
        if (err) return callback(err, null);
        // send notification to firebase
        notify(created.title, created.message, 'sermons')
          .then(() => {
            return callback(null, true);
          });
      });
    } else if (sermon) {
      callback('Sermon already exists', false);
    }
  });
};

sermonObj.getSermonById = (id, callback) => {
  if (id == null) return callback('ID  is null or undefined', null);

  Sermon.findById({_id: id}, (err, sermon) => {
    if (err) return callback(err, null);

    return callback(null, sermon);
  });
};

sermonObj.getAllSermon = (options, callback) => {
  if (options.last_id) {
    sermonObj.getOrderedSermon(options.order, options.limit, options.last_id)
      .then(data => {
        return callback(null, data);
      })
      .catch(err => {
        return callback(err, null);
      });
  } else {
    Sermon.find()
      .sort({_id: options.order})
      .limit(options.limit)
      .then(data => {
        return callback(null, data);
      })
      .catch(err => {
        return callback(err, null);
      });
  }
};

sermonObj.getSermonWithLimit = (lim, callback) => {
  Sermon.find()
    .sort({_id: -1})
    .limit(lim)
    .then(sermons => {
      callback(null, sermons);
    })
    .catch(err => {
      callback(err, null);
    });
};

sermonObj.getSermonByTitle = function(title, callback) {
  Sermon.findByTitle(function(err, sermon) {
    if (err) return callback(err, null);

    return callback(null, sermon);
  });
};

sermonObj.getOrderedSermon = function(order, lim, last_id) {
  return new Promise((resolve, reject) => {
    if (order == 1) {
      Sermon.find({_id: {$lt: last_id}})
        .sort({_id: -1})
        .limit(lim)
        .then((data) =>{
          resolve(data);
        })
        .catch((err) =>{
          reject(err);
        });
    } else {
      Sermon.find({_id: {$gt: last_id}})
        .sort({_id: 1})
        .limit(lim)
        .then((data) =>{
          resolve(data);
        })
        .catch((err) =>{
          reject(err);
        });
    }
  });
};

sermonObj.updateSermon = (id, newsermon, callback) => {
  if (id == null) {
    callback('ID is empty or undefined', null);
  } else {
    Sermon.findByIdAndUpdate({_id: id}, newsermon, {new: true},
      (err, updated) => {
        if (err) {
          callback(err, null);
        } else {
          callback(null, updated);
        }
      });
  }
};

sermonObj.deleteSermon = (id, callback) => {
  if (id == null) {
    callback('ID is empty or undefined', null);
  } else {
    Sermon.findByIdAndRemove(id, (err, removed) => {
      if (err) {
        callback(err, null);
      } else if (removed) {
        if (removed.banner) apostliteS3.deleteFile(removed.banner);
        callback(null, true);
      } else {
        callback('No sermon found to delete', null);
      }
    });
  }
};

module.exports = sermonObj;
