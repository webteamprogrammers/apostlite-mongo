'use strict';
const Executive = require('../models/executive');
const Wing = require('../models/wing');
const executiveObj = {};

const apostliteS3 = require('../controllers/apostliteS3');
const notify = require('../controllers/fcm/fcm');

/**
 * @todo create executive
 */
executiveObj.createExcecutive = (newPerson) => {
  console.log("calling the function to create.. ", newPerson);
  return new Promise((resolve, reject) => {
    Executive.isExisting(newPerson, (err, existing, cb) => {
      console.log("this is the new person", newPerson);
      if (err) {
        console.log('caught an error in the controller', err);
        reject(err);
      } else if (!cb) {
        newPerson.createdAt = Date.now();
        Executive.create(newPerson)
          .then((createdPerson) => {
            // update the wing that the executive belongs
            Wing.findById(newPerson.wing)
              .then((wing) => {
                wing.executives.push(createdPerson._id);
                wing.save()
                  .then(() => resolve(createdPerson));
              });
          })
          .catch((err) => {
            reject(err);
          });
      } else if (existing) {
        reject('Executive already exists');
      }
    });
  });
};

/**
 * @todo retrieve executive
 */
executiveObj.getAllExecutives = (options) => {
  return new Promise((resolve, reject) => {
    Executive.find({})
      .sort({_id: options.order})
      .limit(options.limit)
      .then((data) => {
        resolve(data);
      })
      .catch(() => {
        reject('Sorry an error occured');
      });
  });
};

/**
 * @todo get executive by id
 * @param id a unique id for the executive
 */
executiveObj.getExecutiveById = (options) => {
  return new Promise((resolve, reject) =>{
    if (options.id == null || options.id == '') {
      reject('Id cannot be empty');
    } else {
      Executive.findById({_id: options.id})
        .populate(options.include)
        .exec((err, data) => {
          if (err) {
            reject('an error occured');
          } else if (!err && data) {
            resolve(data);
          }
        });
    }
  });
};
/**
 * @todo update executive
 */
executiveObj.updateExecutive = (options) => {
  return new Promise((resolve, reject) => {
    if (options.id == null || options.id == '') {
      reject('You didnt provide an id');
    } else if (options.executive == null) {
      reject('You didn\'t provide a new item to update');
    } else if (options.id && options.executive) {
      Executive.findByIdAndUpdate({_id: options.id}, options.executive,
        {new: true}, (err, updated) => {
          if (err) reject('Sorry an error occur');

          else if (updated.nModified > 0) resolve(updated);

          else reject('Nothing found to update');
        });
    }
  });
};

/**
 * @todo delete executive
 */
executiveObj.deleteExecutive = (id) => {
  return new Promise((resolve, reject) => {
    if (id == null || id == '') {
      reject('ID cannot be empty');
    } else {
      Executive.findByIdAndRemove(id, (err, removed) => {
        if (err) reject('Sorry! an error occured');

        else if (removed) {
          // remove the executive from the wing they belong to
          Wing.findById({_id: removed.wing_id})
            .then((wing) => {
              wing.executives.pop(id);
              wing.save();
              resolve(removed);
            });
        }
      });
    }
  });
};

module.exports = executiveObj;
