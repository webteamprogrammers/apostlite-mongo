'use strict';

const https = require('https');
const fs = require('fs');
const {google} = require('googleapis');
const MESSAGING_SCOPE = 'https://www.googleapis.com/auth/firebase.messaging';
const SCOPES = [MESSAGING_SCOPE];
const PROJECT_ID = 'apostlite-nupsg';
const HOST = 'fcm.googleapis.com';
const PATH = '/v1/projects/' + PROJECT_ID + '/messages:send';

function getAccessToken() {
  return new Promise((resolve, reject) =>{
    const key = require(_basedir + '/static/apostlite-nupsg-firebase-adminsdk-pipr0-f175184dc0.json');
    const jwtClient = new google.auth.JWT(
      key.client_email,
      null,
      key.private_key,
      SCOPES,
      null
    );
    jwtClient.authorize(function(err, tokens) {
      if (err) {
        reject(err);
        return;
      }
      resolve(tokens.access_token);
    });
  });
}

const notify = (title, body, topic) => {
  return new Promise((resolve, reject)=>{
    if (!title || !body) {
      reject('The msg is empty');
      return;
    }
    getAccessToken()
      .then(accessToken => {
        const options = {
          hostname: HOST,
          path: PATH,
          method: 'POST',
          // [START use_access_token]
          headers: {
            'Authorization': 'Bearer ' + accessToken
          }
        // [END use_access_token]
        };

        const request = https.request(options, resp => {
          resp.setEncoding('utf8');
          resp.on('data', data => {
            resolve(data);
          });
        });
        // request = request;
        request.on('error', err => {
          reject('Unable to send message to Firebase' + err);
        });

        request.write(JSON.stringify(formateMessage(title, body, topic)));
        request.end();
      });
  });
};

function formateMessage(title, content, topic) {
  const msg = {
    message: {
      notification: {
        title: title,
        body: content,
      },
      topic: topic,
    },
  };
  return msg;
}
module.exports = notify;

