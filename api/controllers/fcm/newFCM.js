'use strict';

let admin           =   require('firebase-admin'),
    path            =   require('path')
    serviceAccount  = require('apostlite-nupsg-firebase-adminsdk-pipr0-f175184dc0.json'),
    fcmObj          = {};

    let app =admin.messaging.app;

    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount)
      });

fcmObj.sendMsg = function(){

      let options = {
        priority: "high",
        timeToLive: 60 * 60 *24
      };

      // See documentation on defining a message payload.
      let message = {
        data: {
          content: 'Hello.. we are testing our api and fcm.',
          sender: 'Eben_K.B'
        },
        topic: "news"
      };

      let payload = {
        notification: {
          title: "Testing notification",
          body: "If you see this, then it's coming from the server."
        },
        topic:'news'
      };

// Send a message to devices subscribed to the provided topic.
admin.messaging().send(payload)
  .then((response) => {
    // Response is a message ID string.
    console.log('Successfully sent message:', JSON.stringify(response));
  })
  .catch((error) => {
    console.log('Error sending message:', error);
  });
}

module.exports = fcmObj;
