'use strict';

const mongoose = require('mongoose');

const User = require('../models/user');

const userObj = {};

const bcrypt = require('bcryptjs');

userObj.getUserByName = function(name, callback) {
  User.findByName(name, function(err, user) {
    if (err) {
      return callback(err, null);
    }
    return callback(null, user);
  });
};

userObj.getUser = (email, password) => {
  return new Promise((resolve, reject) => {
    User.findOne({email})
      .then(user => {
        if (bcrypt.compareSync(password, user.password)) {
          // save token for the user
          resolve(user);
        } else {
          resolve(null);
        }
      })
      .catch(error => {
        reject(error);
      });
  });
};

userObj.getAllUsers = function(callback) {
  User.find(function(err, users) {
    if (err) {
      return callback(err, null);
    }
    return callback(null, users);
  });
};

userObj.createUser = (user, callback) => {
  if (user) {
    user.createdAt = Date.now();
    User.create(user, function(err, cb) {
      if (err) {
        return callback(err, null);
      }
      return callback(null, cb);
    });
  } else {
    callback('User is not defined', null);
  }
};

userObj.saveToken = function(_id, currentToken) {
  return new Promise((resolve, reject) => {
    User.findByIdAndUpdate({_id}, {currentToken}, {new: true})
      .then(data => {
        resolve(data);
      })
      .catch(err => {
        reject(new Error(err));
      });
  });
};
// return new Promise ((resolve,reject) => {
//   if(!user) {
//     reject (new Error({
//       err: "User is not defined"
//     }));
//   }
//   User.create(user)
//   .then ((data) =>{
//     resolve(data);
//   })
//   .catch((err) => {
//     reject (err)
//   })
// })

module.exports = userObj;
