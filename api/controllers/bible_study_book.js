'use strict';

const Bible_Study_Book = require('../models/book');
const notify = require('../controllers/fcm/fcm');

const bible_study_bookObj = {};

// create a new bible study book
bible_study_bookObj.createBook = (book, callback) => {
  Bible_Study_Book.isExisting(book, (err, existing, cb) => {
    if (err) {
      return callback(err, null);
    } else if (!cb) {
      book.createdAt = Date.now();
      Bible_Study_Book.create(book, (err, created) => {
        if (err) return callback(err, null);
        // send notification to devices
        notify('New Bible Study Outline', book.title, 'bible-study-books')
          .then(() => {
            return callback(null, created);
          });
      });
    } else if (book) {
      callback('Bible Study Book already exists', false);
    }
  });
};

bible_study_bookObj.getAllBooks = (options, callback) => {
  Bible_Study_Book.find({}, (err, books) =>{
    if (err) {
      callback(err, null);
    } else {
      callback(null, books);
    }
  });
  // Bible_Study_Book.find({})
  //   .populate('studies')
  //   .exec((err, books) => {
  //     if (err) {
  //       return callback(err, null);
  //     } else {
  //       return callback(null, books);
  //     }
  //   });
};

bible_study_bookObj.getAllBookStudies = async (options) => {
  return new Promise((resolve, reject) => {
    if (options.book_id == null || options.book_id == '')
      reject('err: Id cannot be empty', null);

    Bible_Study_Book.find({_id: options.book_id})
      .populate('studies')
      .exec((err, book) =>{
        if (err) {
          reject(err);
        } else {
          resolve(book);
        }
      });
  });
};

bible_study_bookObj.getBook = async (_id) => {
  return new Promise((resolve, reject) => {
    if (_id == null || _id == '')
      reject('err: Id cannot be empty', null);

    Bible_Study_Book.find({_id}, (err, data) => {
      if (err) {
        reject(err, null);
      } else {
        resolve(data);
      }
    });
  });
};

bible_study_bookObj.updateBook = (id, newBook, callback) => {
  if (id == null || id == '')
    return callback('Id cannot be empty');

  Bible_Study_Book.findByIdAndUpdate(id, newBook, {new: true},
    (err, updated) => {
      if (err) {
        callback(err, null);
      } else {
        callback(null, updated);
      }
    });
};

bible_study_bookObj.deleteBook = (id, callback) => {
  if (id == null || id == '')
    return callback('Id cannot be empty');

  Bible_Study_Book.findByIdAndDelete(id, (err, deleted) => {
    if (err) {
      callback(err, null);
    } else {
      callback(null, deleted);
    }
  });
};

module.exports = bible_study_bookObj;
