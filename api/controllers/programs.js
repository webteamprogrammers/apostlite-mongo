'use strict';

const mongoose = require('mongoose');

const Program = require('../models/program');

const Wing = require('../models/wing');

const programObj = {};

const apostliteS3 = require('../controllers/apostliteS3');

const notify = require('../controllers/fcm/fcm');
const ObjectId = mongoose.Types.ObjectId;

programObj.createNewProgram = (program, callback) => {
  Program.isExisting(program, (err, existing, cb) => {
    if (err) {
      return callback(err, null);
    } else if (!cb) {
      program.createdAt = Date.now();
      Program.create(program, function(err, created) {
        if (err) return callback(err, null);

        // get the wing who owns the program and update it
        if (created.owner) {
          Wing.findById({_id: created.owner})
            .then((foundWing) => {
              foundWing.programs.push(created._id);
              foundWing.save();
              return callback(null, created);
            });
        }
        // send notification to devices
        notify('New Program', created.theme, 'programs');
      });
    } else if (program) {
      callback(existing, false);
    }
  });
};

programObj.getProgramById = (options, callback) => {
  if (options.id == null) return callback('ID is null or undefined', null);

  if (options.includeOwner) {
    Program.findById({_id: options.id})
      .populate('owner')
      .exec((err, program) => {
        if (err) return callback(err, null);
        return callback(null, program);
      });
  } else {
    Program.findById({_id: options.id}, (err, program) => {
      if (err) return callback(err, null);
      return callback(null, program);
    });
  }
};

programObj.getAllProgram = (options, callback) => {
  // search based on db cursor position (last_id) and sort by the owner of the program
  if ((options.last_id) || options.withOwner) {
    programObj.getOrderedPrograms(options)
      .then((data) => {
        return callback(null, data);
      })
      .catch((err) => {
        return callback(err, null);
      });
  } else { // find all programs that are in the database. Filter them by month
    if (options.month) { // check if the query should be filtered by month
      Program.aggregate([
        {$addFields: {month: {$month: '$start_date'}}},
        {$match: {month: options.month}},
        {$sort: {_id: options.order}},
        {$limit: options.limit}
      ])
        .then((data) => callback(null, data));
    } else { // find all programs that are in the database. Do not perfrom any sorting or trimming of the data
      Program.find()
        .sort({_id: options.order})
        .limit(options.limit)
        .then((programs) => {
          return callback(null, programs);
        })
        .catch(err => {
          return callback(err, null);
        });
    }
  }
};

programObj.getOrderedPrograms = function(options) {
  return new Promise((resolve, reject) => {
    /**
     * check if the user wants to search using the owner of the program
     * search for programs using their owner as a query param.
     * it retrives only programs with the owner specified
     */
    if (options.withOwner) {
      // check if the search has a last id
      if (options.last_id) {
        if (options.order == 1) { // search in ascending order
          if (options.month) { // check if the data should be filtered by month
            // filter prorams by month
            Program.aggregate([
              {
                $addFields: {
                  month: {$month: '$start_date'},
                }
              },
              {
                $match: {
                  _id: {$lt: ObjectId(options.last_id)},
                  owner: options.owner,
                  month: options.month,
                },
              },
              {$sort: {_id: -1}},
              {$limit: options.limit},
            ]).then((data) => resolve(data));
          } else {
            programObj.findProgramWithId(options, -1)
              .then((d) => resolve(d))
              .catch((err) => reject(err));
            // Program.find({_id: {$lt: options.last_id}, owner: options.owner}) // DEPRACATED
            //   .sort({_id: -1})
            //   .limit(options.limit)
            //   .then(data => {
            //     resolve(data);
            //   })
            //   .catch(err => {
            //     reject(err);
            //   });
          }
        } else { // search in descending order
          if (options.month) {
            Program.aggregate([
              {$addFields: {month: {$month: '$start_date'}}},
              {
                $match: {
                  _id: {$gt: ObjectId(options.last_id)},
                  owner: options.owner,
                  month: options.month,
                }
              },
              {$sort: {_id: 1}},
              {$limit: options.limit}
            ])
              .then((data) => resolve(data))
              .catch((err) => reject(err));
          } else {
            programObj.findProgramWithId(options, 1)
              .then((d) => resolve(d))
              .catch((err) => reject(err));
          }
          // Program.find({_id: {$gt: options.last_id}, owner: options.owner}) // DEPRACATED
          //   .sort({_id: 1})
          //   .limit(options.limit)
          //   .then(data => {
          //     resolve(data);
          //   })
          //   .catch(err => {
          //     reject(err);
          //   });
        }
      } else {
        if (options.order == 1) { // search in ascending order
          if (options.month) {
            Program.aggregate([
              {$addFields: {month: {$month: '$start_date'}}},
              {
                $match: {
                  owner: options.owner,
                  month: options.month,
                }
              },
              {$sort: {_id: -1}},
              {$limit: options.limit}
            ])
              .then((data) => resolve(data))
              .catch((err) => reject(err));
          } else {
            programObj.findProgram(-1)
              .then((d) => resolve(d))
              .catch((err) => reject(err));
          }

          // Program.find({owner: options.owner})
          //   .sort({_id: -1})
          //   .limit(options.limit)
          //   .then(data => {
          //     resolve(data);
          //   })
          //   .catch(err => {
          //     reject(err);
          //   });
        } else { // search in descending order
          if (options.month) {
            Program.aggregate([
              {$addFields: {month: {$month: '$start_date'}}},
              {
                $match: {
                  owner: options.owner,
                  month: options.month,
                }
              },
              {$sort: {_id: 1}},
              {$limit: options.limit}
            ])
              .then((data) => resolve(data))
              .catch((err) => reject(err));
          } else {
            programObj.findProgram(options, 1)
              .then((d) => resolve(d))
              .catch((err) => reject(err));
          }
          // Program.find({owner: options.owner})
          //   .sort({_id: 1})
          //   .limit(options.limit)
          //   .then(data => {
          //     resolve(data);
          //   })
          //   .catch(err => {
          //     reject(err);
          //   });
        }
      }
    } else { // find all programs in the database that matches the search and sort desc. It does not check for the owner of the program
      if (options.order == 1) {
        if (options.month) {
          Program.aggregate([
            {$addFields: {month: {$month: '$start_date'}}},
            {
              $match: {
                _id: {$gt: ObjectId(options.last_id)},
                month: options.month,
              }
            },
            {$sort: {_id: 1}},
            {$limit: options.limit}
          ])
            .then((data) => resolve(data))
            .catch((err) => reject(err));
        } else {
          Program.find({_id: {$lt: options.last_id}})
            .sort({_id: -1})
            .limit(options.limit)
            .then(data => {
              resolve(data);
            })
            .catch(err => {
              reject(err);
            });
        }
      } else { // find all programs in the database that matches the search and sort asc. It does not check for the owner of the program
        if (options.month) {
          Program.aggregate([
            {$addFields: {month: {$month: '$start_date'}}},
            {
              $match: {
                _id: {$gt: ObjectId(options.last_id)},
                month: options.month,
              }
            },
            {$sort: {_id: 1}},
            {$limit: options.limit}
          ])
            .then((data) => resolve(data))
            .catch((err) => reject(err));
        } else {
          Program.find({_id: {$gt: options.last_id}})
            .sort({_id: 1})
            .limit(options.limit)
            .then(data => {
              resolve(data);
            })
            .catch(err => {
              reject(err);
            });
        }
      }
    }
  });
};

programObj.getProgramWithLimit = (lim, callback) => {
  Program.find()
    .sort({_id: -1})
    .limit(lim)
    .then(programs => {
      callback(null, programs);
    })
    .catch(err => {
      callback(err, null);
    });
};

programObj.getProgramByTitle = function(title, callback) {
  Program.findByTitle(function(err, program) {
    if (err) return callback(err, null);

    return callback(null, program);
  });
};

programObj.updateProgram = (id, newprogram, callback) => {
  if (id == null) {
    callback('ID is empty or undefined', null);
  } else {
    Program.findByIdAndUpdate({_id: id}, newprogram, {new: true},
      (err, updated) => {
        if (err) {
          callback(err, null);
        } else {
          callback(null, updated);
        }
      });
  }
};

programObj.deleteProgram = (id, callback) => {
  if (id == null) {
    callback('ID is empty or undefined', null);
  } else {
    Program.findByIdAndRemove(id, (err, removed) => {
      if (err) {
        callback(err, null);
      } else if (removed) {
        apostliteS3.deleteFile(removed.banner);
        callback(null, true);
      } else {
        callback('No program found to delete', null);
      }
    });
  }
};

programObj.findProgramWithId = (options, order) => {
  return new Promise((resolve, reject) => {
    Program.find({_id: {$gt: options.last_id}, owner: options.owner})
      .sort({_id: order})
      .limit(options.limit)
      .then(data => {
        resolve(data);
      })
      .catch(err => {
        reject(err);
      });
  });
};

programObj.findProgram = (options, order) => {
  return new Promise((resolve, reject) => {
    Program.find({owner: options.owner})
      .sort({_id: order})
      .limit(options.limit)
      .then(data => {
        resolve(data);
      })
      .catch(err => {
        reject(err);
      });
  });
};
module.exports = programObj;
