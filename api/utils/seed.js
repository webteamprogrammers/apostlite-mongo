'use strict';

const Program = require('../models/program');

// fix owner property on programs
async function updateProg() {
  return new Promise((resolve, reject) => {
    Program.find({
      $or: [
        {owner: null},
        {owner: ''},
      ],
    })
      .then((programs) => {
        programs.map((p) => {
          p.owner = 'church';
          p.save();
          resolve('upodated programs', programs);
        });
      })
      .catch(() => reject('an error occured'));
  });
}

async function getProgramWithoutOwner() {
  return new Promise((resolve, reject) => {
    Program.find({
      owner: {$exists: false}
    })
      .then((progs) => {
        console.log('these are the progrs', progs);
        resolve(progs);
      }).catch(() => 'an error occured');
  });
}
// updateProg();
// getProgramWithoutOwner();
