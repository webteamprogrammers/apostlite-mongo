'use strict';

// establish a databse connection
const mongoose = require('mongoose');
const options = {
  connectTimeoutMS: 30000
};

module.exports = function(config) {
  return new Promise((resolve, reject) => {
    mongoose
      .connect(config.db, options)
      .then((data) => {
        console.log('Database Connection is successful...');
        resolve(data);
      })
      .catch((err) => {
        console.log('An error occured while connecting to the database.');
        reject('an error occurred ...');
      });
  });
};

