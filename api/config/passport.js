'use strict';

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const passportJWT = require('passport-jwt');
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

const User = require('../models/user');

passport.use(
  new LocalStrategy(
    {
      username: 'username',
      password: 'password',
    },
    function(username, password, cb) {
      // search for the user by username and password and return the user
      return User.findOne({username, password})
        .then(user => {
          if (!user) {
            return cb(null, false, {message: 'Incorrect email or password'});
          }
          return cb(null, user, {message: 'Logged in successfully'});
        })
        .catch(err => cb(err));
    }
  )
);

passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: 'use_my_secret.eben_kb',
    },
    function(jwtPayload, cb) {
      // find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload
      return User.findOneById(jwtPayload.id)
        .then(user => {
          return cb(null, user);
        })
        .catch(err => {
          return cb(err);
        });
    }
  )
);

module.exports = passport;
