'use strict';

module.exports = function(app) {
  // api require routers
  const authRoute = require('./routes/auth');
  const indexRoute = require('./routes/index');
  const userRoutes = require('./routes/user');
  const sermonRoutes = require('./routes/sermon');
  const programRoutes = require('./routes/programs');
  const wingRoutes = require('./routes/wings');
  const newsRoutes = require('./routes/news');
  const bible_studyRoutes = require('./routes/study');
  const bible_study_bookRoute = require('./routes/book');
  const executiveRoute = require('./routes/executive');
  const meetingRoute = require('./routes/meeting');
  const PatronRouter = require('./routes/patron');

  // require routes for the web server
  const adminRoute = require('../webserver/admin/routes/index');

  // user the routes
  app.use('/', indexRoute);
  app.use('/api/auth', authRoute);
  // app.use('/api/user',passport.authenticate('jwt',{session:false}),userRoutes);
  app.use('/api', userRoutes, PatronRouter);
  app.use('/api', sermonRoutes);
  app.use('/api', programRoutes);
  app.use('/api', wingRoutes);
  app.use('/api', newsRoutes);
  app.use('/api', bible_studyRoutes);
  app.use('/api', bible_study_bookRoute);
  app.use('/api', executiveRoute);
  app.use('/api', meetingRoute);
  app.use('/webserver/admin', adminRoute);
};

