define({ "api": [
  {
    "type": "post",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/studies",
    "title": "Create Bible Study",
    "name": "CreateBibleStudy",
    "group": "BibleStudy",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "bible_Study",
            "description": "<p>The BibleStudy to create</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Sample-Request:",
          "content": "\n{\n  study_number :1,\n  scriptures:[\n    \"mathew 5v11\",\n    \"john 11 v 35\"\n  ],\n  title :\"Something to describe the Bible study\",\n  content:\"Walking in a spirit filled life. Life that will cause you to impact in the lives of other. That is what the bible teaches and encourages.\",\n  questions:{\n    \"question one\", \"question two\", \"question three\"\n   }\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>Do not provide Date with the request. Date is provided by default by the api.</p>",
    "filename": "api/routes/study.js",
    "groupTitle": "BibleStudy"
  },
  {
    "type": "delete",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/studies/{id}",
    "title": "Delete Bible Study",
    "name": "DeleteBibleStudy",
    "group": "BibleStudy",
    "version": "1.0.0",
    "description": "<p>To delete an object, you need to provide the unique id of the object.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Returns success if the delete was successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Returns true if the delete was successful.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "success response",
          "content": "{\n   message:\"success\",\n   status: \"true\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "An",
            "description": "<p>error occured while adding the wing.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response: an error occured while adding the wing.",
          "content": "\n{\n   message: \"an error occured while adding the bible_study\",\n   error: \"the type  of error that occured\",\n   status: \"false\"\n}",
          "type": "object"
        }
      ]
    },
    "filename": "api/routes/study.js",
    "groupTitle": "BibleStudy"
  },
  {
    "type": "put",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/studies/{id}",
    "title": "Edit Bible Study",
    "name": "EditBibleStudy",
    "group": "BibleStudy",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "BibleStudy": [
          {
            "group": "BibleStudy",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The BibleStudy's unique id</p>"
          },
          {
            "group": "BibleStudy",
            "type": "object",
            "optional": false,
            "field": "bible_study",
            "description": "<ul> <li>The new bible_study to be replaced</li> </ul>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example Data The new bible_study",
          "content": "\n{\n   study_number :1,\n   scriptures:[\n     \"mathew 5v11\",\n     \"john 11 v 35\"\n   ],\n   title :\"Something to describe the Bible study\",\n   content:\"Walking in a spirit filled life. Life that will cause you to impact in the lives of other. That is what the bible teaches and encourages.\",\n   questions:{\n     \"question one\", \"question two\", \"question three\"\n    }\n\n }",
          "type": "json"
        }
      ]
    },
    "description": "<p>To update a Bible Study, please provide the new bible_study in a json object and parse it to the api.</p>",
    "filename": "api/routes/study.js",
    "groupTitle": "BibleStudy"
  },
  {
    "type": "get",
    "url": "https://nupsg-apostlite.herokuapp.com/api/v1/studies",
    "title": "Retrieve all bibleStudies",
    "description": "<p>Please follow the example object below.</p>",
    "name": "GetBibleStudies",
    "group": "BibleStudy",
    "version": "1.0.1",
    "parameter": {
      "fields": {
        "limit": [
          {
            "group": "limit",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>(optional query param  This is the amount of records that should be returned by the api. It defaults to 100 and the minimum limit that you can parse is 1.</p>"
          }
        ],
        "order": [
          {
            "group": "order",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>(optional query param) This parameter tells the api to sort the records in ascending or descending order. Use {asc} for ascending and {des} for descending</p> <p>When not provided the order defaults to ascending.</p>"
          }
        ],
        "last_id": [
          {
            "group": "last_id",
            "type": "Object_Id",
            "optional": false,
            "field": "last_id",
            "description": "<p>The id of the last item in the previous search result. When you add the last_id option, the api starts the search from the last_id that you provide.</p> <p>You can use the {last_id} with {asc} or {des}. When you use {asc} the api starts from the {last_id} you have provided and search for items created earlier than the last_id you have provided. Wehn you use {des} the api starts from the {last_id} you have provided and search for items created after the last_id that you have provided.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Get bibleStudies By Asc",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/studies?order=asc\n         Retrieves all items from the database in ascending order.",
        "type": "js"
      },
      {
        "title": "Get bibleStudies By Des",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/studies?order=des\n         The request retrieves all items from the database in descending order.",
        "type": "js"
      },
      {
        "title": "Get bibleStudies By Limit",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/studies?limit=20\n         The request retrieves only the first twenty items from the database in ascending order.",
        "type": "js"
      },
      {
        "title": "Get bibleStudies By Last_id",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/studies?limit=20&last_id=de23ab32009a2b&order=asc\n         The request retrieves only the first twenty items created before the {last_id} that you have provided.",
        "type": "js"
      },
      {
        "title": "Get bibleStudies With options",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/studies?limit=20&last_id=de23ab32009a2b&order=des\n         The request retrieves only the first twenty items created after the {last_id} that you have provided.",
        "type": "js"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "ObjectID",
            "optional": false,
            "field": "_id",
            "description": "<p>This is a unique object id for the bible study item</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>The title for the bibleStudies *</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "study_number",
            "description": "<p>The study number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>The actual content of the bibleStudies</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "memory_verse",
            "description": "<p>The memory verse for the semester focus</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "scriptures",
            "description": "<p>All the scriptures for that bible study</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "questions",
            "description": "<p>All the questions about the bible study</p>"
          },
          {
            "group": "Success 200",
            "type": "ObjectId",
            "optional": false,
            "field": "An",
            "description": "<p>ID that references the book that the study belongs to</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "createdAt",
            "description": "<p>The deate when the bibleStudies was created</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>The date for the most current update of the bibleStudies.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example data on success",
          "content": "\n {\n   \"scriptures\": [],\n   \"questions\": [\n       \"john 4v3\",\n       \"Luke 4v5\",\n       \"Mathew 5v4\"\n   ],\n   \"createdAt\": \"2019-02-05T13:14:45.661Z\",\n   \"_id\": \"5c598c45cd8dda57b87c4fb3\",\n   \"title\": \"In His vine yard 2\",\n   \"study_number\": 4,\n   \"content\": \"This is a bible study\",\n   \"memory_verse\": \"Luke 2v13\",\n    \"objective\": \"To be Like christ\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/study.js",
    "groupTitle": "BibleStudy"
  },
  {
    "type": "get",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/studies/{id}",
    "title": "Retrieve  bible study information by Id",
    "description": "<p>Get information for only one wing using the bible_study id.This routes will always return one object if found else it will return an object of bible_study (i.e one object containing several bible_study).</p>",
    "name": "GetBibleStudy",
    "group": "BibleStudy",
    "version": "1.0.0",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DatabaseError",
            "description": "<p>An error occured while fetching the records from the database</p>"
          }
        ]
      }
    },
    "filename": "api/routes/study.js",
    "groupTitle": "BibleStudy"
  },
  {
    "type": "post",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/books",
    "title": "",
    "name": "CreateBook",
    "group": "Book",
    "parameter": {
      "examples": [
        {
          "title": "Sample Request:",
          "content": "\n{\ntitle: 'New Test Book 2',\nscripture: 'Revelations 1 v 5',\nacademic_year: '2018/2019',\nsemester: '2018/2019',\n};",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/routes/book.js",
    "groupTitle": "Book"
  },
  {
    "type": "delete",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/books/{id}",
    "title": "Delete Book",
    "name": "DeleteBook",
    "group": "Book",
    "description": "<p>To delete a book, you have to provide a unique id of the record that you want to delete</p>",
    "sampleRequest": [
      {
        "url": "https://apostlite-mongo.herokuapp.com/api/v1/bibleStudies/abcedfadaebbadcaed"
      }
    ],
    "version": "0.0.0",
    "filename": "api/routes/book.js",
    "groupTitle": "Book"
  },
  {
    "type": "put",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/books/{id}",
    "title": "Edit Book",
    "name": "EditBook",
    "group": "Book",
    "parameter": {
      "fields": {
        "id": [
          {
            "group": "id",
            "type": "ObjectId",
            "optional": false,
            "field": "This",
            "description": "<p>is a unique identifier for the record you want to update</p>"
          }
        ]
      }
    },
    "description": "<p>To update a book, you have parse a book object with the necessary fields to update</p>",
    "version": "0.0.0",
    "filename": "api/routes/book.js",
    "groupTitle": "Book"
  },
  {
    "type": "get",
    "url": "https://nupsg-apostlite.herokuapp.com/api/v1/books",
    "title": "Get All Books",
    "description": "<p>When found, the api will return all books.</p>",
    "name": "GetAllBooks",
    "group": "Book",
    "examples": [
      {
        "title": "Get All Books",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/books\nReturns a single book record with a reference to it's related items",
        "type": "js"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "scripture",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "semester",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "academic_year",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "studies",
            "description": "<p>This is an array of objects that contains all the studies related to the book</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "createdAt",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updatedAt",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Example data on success",
          "content": "{\n     \"studies\": [\n       \"5c5503cb67f647560e892b56\",\n       \"5c5503cb67f647560e892ba4a\"\n       \"5c5503cb67f647560e892aa23\"\n     ],\n     \"_id\": \"5c5503cb67f647560e892b56\",\n     \"title\": \"New Test Book 2\",\n     \"scripture\": \"Revelations 1 v 5\",\n     \"academic_year\": \"2018/2019\",\n     \"semester\": \"2018/2019\",\n     \"__v\": 1\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/routes/book.js",
    "groupTitle": "Book"
  },
  {
    "type": "get",
    "url": "https://nupsg-apostlite.herokuapp.com/api/v1/books/id",
    "title": "Get only a specific book",
    "description": "<p>When found, the api will return only a single record. By default the api only returns the book record with a reference to it's related items. For example, a book can have one or more studies. In order to fetch the studies as part of the book, you have to specify it in the request query parameters.</p>",
    "name": "GetBook",
    "group": "Book",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "include",
            "description": "<p>This is provided as a query parameter which tells the api the items it should include in the request. When not provided, no items will be embedded in the request but there will be a reference to the items inside the request.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Get Book",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/books/id\nReturns a single book record with a reference to it's related items",
        "type": "js"
      },
      {
        "title": "Get Book and Studies",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/books/id?include=studies\nReturns a single book record and all related studies",
        "type": "js"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "scripture",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "semester",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "academic_year",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "studies",
            "description": "<p>This is an array of objects that contains all the studies related to the book</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "createdAt",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updatedAt",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Example data on success",
          "content": "{\n     \"studies\": [\n         {\n             \"scriptures\": [],\n             \"questions\": [\n                 \"john 4v3\",\n                 \"Luke 4v5\",\n                 \"Mathew 5v4\"\n             ],\n             \"createdAt\": \"2019-02-02T02:43:18.102Z\",\n             \"_id\": \"5c5503c667f647560e892b55\",\n             \"title\": \"In His vine yard 2\",\n             \"study_number\": 4,\n             \"content\": \"This is a bible study\",\n             \"memory_verse\": \"Luke 2v13\",\n             \"objective\": \"To be Like christ\",\n             \"__v\": 0\n         }\n     ],\n     \"_id\": \"5c5503cb67f647560e892b56\",\n     \"title\": \"New Test Book 2\",\n     \"scripture\": \"Revelations 1 v 5\",\n     \"academic_year\": \"2018/2019\",\n     \"semester\": \"2018/2019\",\n     \"__v\": 1\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/routes/book.js",
    "groupTitle": "Book"
  },
  {
    "type": "post",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/executives",
    "title": "Create Executive",
    "name": "CreateExecutive",
    "group": "Executive",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "bible_Study",
            "description": "<p>The Executive to create</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Sample-Request:",
          "content": "\n{\n  wing_id: wing_id,\n  profile_photo: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEBUSEhMVFRUVFRUVFRUVFRUVFRcVFxcXFxUVFRYYHSggGB0lGxcVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGi0fHyUtLS0tKy0tLS0tLSstLS0tLS0tKy0tKy0tLS0tLS0tLS0tKy0tLS0tNy0tLSstLSstLf/AABEIAQMAwgMBIgACEQEDEQH/xAAcAAABBAMBAAAAAAAAAAAAAAAAAQMGBwIEBQj/xABDEAABAwEEBgcFBQcCBwAAAAABAAIDEQQFITEGEkFRYXEHEyKBkaGxMmLB0fAjQlJykhQzY4KywuGi8RUXJENTVJP/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQIDBAX/xAAjEQEBAAICAgIDAAMAAAAAAAAAAQIRAyESMQRREzJBFDNx/9oADAMBAAIRAxEAPwCy0IQmYQkQkCoQkQCoSIQCpCsHyAZlac96RtzcPrggN9FVzW3owt1g5pG0gjdX0XLtmmFnZ94k8BUeKD0k1UVUKHSBZ9of4Yeq2bPpxZXffp+bD/HmjY1UsQudYr1ilFWPa4cCFvMeCgmaEJEAqEiEAqEiEAqVIhAKhIhACEJEAqEiEAqEiEwVc28rzbG049wz5k7Bx4Jb6vFkMRe80A8TwHGgPgVSmkmksloecaMrg3ZzO/IKbTkS2+9NW+zHRx2kHs9xzdzUPtd9yONdanIZLkNtA3V9PBZSPJGXySUfdfElKax1doBpU7K8Vgba44udQbhmVnYrLrRyZVHaGZNRy3/Bc6rgSDgRhmgN8WgbcPris2yLSimG+vj81ttlG7xQD0NqLHAtcWkbQTUeCmFwadysIbN9qzInASAb6/e8lBzKw5lNmoxzHj5oD0HdV7xTt1o3hw8DyIOIK6AK893RfUsEgkjdQjMH2XDc4bR6K4tFNJmWpmwPHtNrWh9aJypsSJCEiZBKhCAEqRCYKhCEgxQkSoAQkQmCpCULg6Y3wLNZXur23DUYOJ29wqUgr/pMv/rJepYewzOm1+35eKr9z9iettoLiScSfqq0iVKzolon4ZSRlVa0UJcaAeKlej2jJkI1yQO+pSyymM7PHC5XUciwRuL6tGAxNVr3iBrmgIFdvxVwWLRqKNvZbgc61JXHvHQ2Nzzge5Y/njo/xstKqNoIwanGP357tqmV46Gtb7JNVoP0QkAwa7mtJy41neDOfxF5paHGld1cUsNsoc6c8l2Jrge37vgFzLRYqDI+HxVzKVlcbPZ+N+tlmNm8cF0LmvR8EokYSCM+I2hR5hcw8B30XQbJUVwQHobR+9m2mBsjTwI3FdNUz0dX+YbQI3H7OTsmux2w/W9XICqibGaFilTIqEiEAqEiVAYoWNUqQKhJVCYI4qnOk6+ettHVtPZi7PDW+8fGg/lKs/SW8Oos0j60NKN5nb3YnuXny3zlzyd5U1UazzUrr3FcD5jgMPJa9x3eZ5Wt2Vx5K57msLY2BrWgAABYcvJ49R0cHF5d304V0aItaAXBTGx3cxgo0UTsTVuMauW2327Op6IyALI2cHYnmBZgKpE3KtGSwtOxH7C2mS6ACwciwvKuNa7rjcMWhQ7SDRSNwJaCrEeFo2qIEZJbs9K1L1VA3zdRiJwNOC5sBoeBVv6RXSxzTUKqrVZdR7mccF1cXJ5OTm4vC7hyB1D5hXrobegnsrHVxpQ8xgR3EFUEXHsnuVk9FF5Uc+A5Htt8gfge4raOerSWSwBWQKpJUJEIBUJEIDCqKpEiQZVRVIhAQbpPtREGrXD1J2fpDvFUy41PkrV6XZaMibvLz4AAeqquLOu6p+SlSYaD2btYfXBWtYm0aAoL0fWP7PXO3Jd+977MXYjGs/hjTmuLk7yehxTWESeNbMarhumUjT2281I7q0phkw1tU8UvCxflKlYWbVoxWkHIgrZbIiVNjYTUiDImXTp2lIVy1ZSm7VbmtBqQOa4Ft0ohaaVJPBTrbT0zv1tWFU/fmE5VnvvuOcENONMQc1WmkzKTV3rTh6y1WXyO8Nxznite8j1Ui0EtvV2uN1aAnVPJ2Hlmo412P1uC2rtfqvbT8Q9cF1uF6NidUfW5Zha13ya0bXbwD4gLYCuIZJVihAZISVQmDSKpEKQVKVjVKUBVHTBP9tC3cxx/U6g/pUAjbh3/AF8VYOmlgba73EL3FrGQh7y32tVuNG14vHmozeN1sitBjjcXsFC1xIrjmMBRRcpvTWYXx8v4svRqyllkjAzLQe8hb1mssbMKCpxJ2k7yVnZG0hYPdHoo5pJeRjbtpvGfIcVw3dvT0cNSduzb7LY34SBmO2oB8VGLdozFWsEoPCoK0OtthiZMzVhaXUbRoc80B7T3OrmNnmtu5bXPM8R6wmaQf3kXVuDmt1nAHMUJLdbgtZhnJ7ZXlwt1pI9Gusj7DzUbDVSiORRWwa2Ix7JoQfaYdzt/A7VKLsxGKx73226kZvkUc0hvd8YpG0uPCqk14ABpKiN46wx3mg24nYBtPD0RfYnaNRwW20uo46gOZJ+C79g0UjbQveXnblQqP3vNNFJqu6uMBtdeUOkNSHFgoBQE0AwFAXDHBa8V5WprDI5jSxpAJiLmOFRUGlS07cKLfxzs6YeeEvaTX3o6wt14ey9uI3clWukpOuCcDu4qwLrvsyx447jSleY2FQvTCI62txS4rfLVHNJ4biORn0C2AaVI4Eea1IjieXzW0x3gR9fBdTieg9F5g+yROGRY2nKmS6qi/RxPrXfFvbVp7iVJ1cRfZUqxSoBUiEJg0hIhSCockCH5ICpb/tNL+1a4PAids9qMED9Qan7yuZoayQdmlQQTxIUc0+tJZeskjcTHJG4DiwMIHlRTi83Nls7JWYtcQW8ndr4rl5tzKWPQ+NrLC4X/AKlViZVg5D0TV43UyRpaQE5dMn2bPyj0XTY2qwaokbmlawxVD2HIOGXEEEGq3Lkuh0R1sNYihcakgYVAqaDu3KTtjCeDAFcyy+0XHH6ch1gYwEgdoihNTlWvqVs2FuKcnWNhzUW9rnplbm1C0pbubIGkjFtaEcaV9AujbMk3ZnJpRy/boMtD94CgcCAaY0BBwP8AlacVySdUYjRrSak4EuO9xJPkFNnQg5ps2VoVeWReOP0ilguNsbdWleO3vUM6SbOGtBG1yteeMBVl0qfuWn3x5lGH7wZ/pVYwHE8lswnALTsx7Xd81t2faN2PgafHyXY8+Lg6J7RWzvZh2Xg+IB+uSnirToklwlbu1Dz1q/JWUqhX2VCRCZFSpEIBkoSISBQkkOCFi84ICgdN3Vt9oPvn1optoY0yXWwZlr5Kdz3ED/VRQHSeTWtcp3ud6lTjognbIyazOfRzSZWjew6rXU5H1Cw5cd4ungzmOe6lt0S0jaDsFPDBduzyqNwkNc5oNQ17hXkV1rPKuPb0JNx3GOT1cFzY51nJNUUCe0XFhapquoMqVKdsMjaZrh3vaXRdrYRnTLgVwroveWry9oAzbqk94IKW1/j3FgWqRoGa0oJaOG0FRO9r3ldETG0E1w1sqbSaZro3HbHSNaDStKmmQTtL8ekuDlg5y1myUGKwfMntHiatkqrHpQm+xaPfCsG2S4KrOkWXWLG8SVfH3lC5uuOoJF7S3Ijj9bVp/eW1Hs5rseas3olf25G72A86Op9c1aQKqXomk/6hw/hHx1/91bIThZBCRKqIqEiEAyhCRIMkxbH0Y47gT4CqeXN0il1bLMf4b/6SgPPlsfV7idpWVht8tneyWF5jkbUhzcxXAgg4EEbDgmbQcTzSECvIKVrG0AvF80cokdrOa9p2V1XNp6tcpjZ5FV3R/btS2OYcpWUH5mdoDwLlZTHYrh58dZPS+Pn5YdurHItyJ65DZKYpm2X22MVdWgzIBPospW1iRyMDs1zLbdgcDQDwXIfpeynYDjx1T6LCLSymNcNoeDTwIWlxVhx53uOtd10BoNcV14rO1uQAUZfpW2mBA3BuXzKwZpbvjeRv1XfJExLPjz91JpZKLUfIuXZL0dONdjHhm9zS2vIHFbZyUX2iRr22TBVTp9ay2ZoFPZJx54KzbU/NU1pfaustbyMm0aO7PzJW/BN5Of5WWsXLqtmMrWAyWxEutwRYfRRX9pcdgjp4mmPeFbwKqPomZWeQ7meZcFbTCnCyZIQhUQQhCAZQkQkCrgaczatgmO9oaP5nBvxK7yiPSZNSxEb3N+J+BSpz2pWTF3mkrmgnErEFI2bLQ6ORr24OY4OHMFW9dN5smibK04OFeIO0HiDgqakOKkugVsc18jKnVoHU41pULHnw3jt0fG5PHLX2teCVb9npQggY5qN2K2Ald2B2sMFw+nomLRE1mQHfksIrW0AgsYa/iANPFdB0GsKFMjR2N2Z8FcyrScupqtfr42D2YhxaxoOOONAs7NL1jssPJbA0aiG/xT7bLqCgVXKllyyzo7I4atFzbVLQUT8zqKO3tebWgknJR7ZemlpPe4hhc450oBvccgqgc4uJJxJJJ5nErqaSXw60SnYxpIaPUniuWwLu4sPGPN5+Tzy69HBsT8aYYap5pwWjKLH6JHfaSn3W/H67lbDDgqo6Ih2pTwb8aenmrXanCrJCEKiCEIQDCVIhIFKrvpZtX2UcdcyXEeQ+KsF7sFT3Sdbw+0ag+6MeFaJU4g5OHf5BIEjnJK4pGwecVI9AWVmkH8Mf1BRpxUv6M46yyncxo8Sfko5f0rTg/wBkSWQFpwXVuu9qGjk3bbKuY+JcHt6idQW9pCfFvCr6O0vbkU+283/RRqjpOmXhVYS24b1C/wDij93mm32t7ttE9UXTrXzfQ9luJULvR7nVLiuw2Jc2946MceBWmHVY8npXjjU13mqUZLELM5LueYcZkPranHHBNt2JXlI1o9EH/d/k/uVpNKqbogl7crfdb6/5VrsThU4kqkSlMiIQhMGkJKoJSBi1uoMcvliqA0ltnW2iR/4nnwrh5K6NM7d1Vke4GhI1R/Ngad3oqGtMlXKaqejL80gzSOOKD5nJAYP2qzOjq7jHCXOFDIdbkKdn59642i2iRcNedpzBaw7BgauHwPGuYpY1ms+qAubn5Jrxjs+PxWXyrOSPBaE9lC7OpUJh8a5HdEcmshTHUkbF3po8clrvi4J7PTldWsmRrfMKdisyraaYbDguPfsPYd+U+ilHU0C495Q1B5FVje2eU3FPrOmK271sDonkEYVq07Kbua02HFd+9x5VmrqngFi4rIGqbAQKn/RLPS1Ob+KM+ILT81ckblQ/RzPq26PiHDvpVXpCcE4VbCCkQSmQQsaoQDaRxXJvfSOzWf8AeyDW/A3tP/SMuZooheXSW3KKE03veAf0tB9UBrdK9611IGn3nDjkPj4qsCTWmNTgBtKkV6W/9omM7hQuoA0GoaAKUHzVj9G10WV1mbMyNvW1c2RxxdrA7zkCKGg3rPPLxm2uGHldbV1cmg9rno5zeqZveO13Mz8aKfXLoVBZqOoXv/G/E92xvcp+yzALXnhxXLnnlk7OPjwxcdlnNcanAAVNaAbOA3BOPYt4xpiRixsdErXjTpZVM5FbLCpU0pIMVryQrpSBMFiDaQiKejiK2hGleKJk0pgtU2bWC23CpW3HDQKoiode1xtlaWkVw+qcVAL/ANG5bM6pBdHscNn5tyu1kJ1iBgNvHh6LO03a2QapHitsOS4sOTixyed43rJ2B4FWZpB0dhxLoeyd33T3KA2+55oXlkrdU7NoI3hdOOcy9OPPjyx9t7Q1+rbYT74+I+K9BWYYBebrukMUrH/ge13gQSF6Huy1tkja9pBa4AgggjyVxnXQSLHWWQTIIQlQTzQZCsdZIhCjsMmxTnoyv4We0GGQ0jmoAScGyjBpPBwwrvDVAVtRuqPVTlNzSsctXb0wE1IxRbo40j/abP1cjqyw0a4nNzPuP57DxFdqlj+C5csddO3HLfbTfGtV7KHgfVdIuWL4wRhUcjQ9xGSz013XKngwwTUS6T4C0YYjz/ytFjRrU8tvgs7GmN6I5ixER3LdaxO6gojxO5OexiwnGC3X4LTkaSU9DZiCNbYbgso2jIYnh8dy2WQ0xJOOwE04VG3M+KqRFpqCBb0UIRHRZhyqM8qwtGq1jnOIDWgkk5AAVJKoHSO8/wBptMk2TSaMG5gwbhxz5kqbdJ2k+sTY4jgKGYg5nMR+hPcN4VayuXVxY67cfLnvo29+K6Vy37aLM6sMhaDm04sdzacO/PiuWhasVs3F0jwvAbaWmJ34mgujPHDtN8xxU1sVtjlbrxPa9v4muDh5LzlVbt23pNA/Xhkcx29pz4OBwcOBQT0RVCqePpOtIABihJoKmjxU76a2CEBAKoSIQYTkTqJsIQHYuK9pLLO2aPNuBByc05tPA+RAOxXxcd9RWqBssZqDmD7TXbWuGwj5HIrzpE/Yuzo7f81jl6yI1BprsPsvG47juOzxBzzw8mvHyeL0A6NYFpXP0Y0lgtjKxu7QA143YPYeI2j3hh6LtloXNcXZjntqFxyTMkNVuFiHNUWNJWi6Mg4EjzCzbrDMBw26px7gdvenHtxT7WbUaFrReDTKnmmWWauLsfTwC6MjarDVQcpkADABGKe6tZhiCujAaozpxpU2yRdXGQZ3jsjPUH/kd8BtPAFMaY6cx2fWigpJNiCc2Rn3vxO93x41LarS6R7pJHFznGrnE1JK34+Lfdc3LzfyG5JDUkmpJJJOJJOJJO0lazile6qxXS4whCRAKhBQgFqlSUQgGwgoCDmgFCEBCAE6yTemilCA3rJaXxvEkb3Mc01a5pII5EKxtH+lEgBlsjLsh1sVAeb4zQd7T3KrWvonRIpuMqsc7PT0bc9+2e0t1oJWvyqAaPH5mGjm94XQcF5kZIQQ5pIIxBBoQd4IxCkV26fXhCKdd1jRkJmiT/Vg8/qWV4vpvjz/AGvUhJU5KpoOlucDt2aJx2lr3sHgQ71Tv/NmT/1Gf/Zx/sUfiya/nwWnVZ6qp+09KtqdgyKFnMPee7tAeS4l4aY26eofaHhp+7HSMcuxQnvJTnDf6m8+P8XPfOkFlso+3la07GDtSHkwY9+SrPSnpClnrHZw6GI4F1ftXDmPYHAEnjsUJc7M78Sd53lNulWuPHIxz5rWZdRMPfVISkWjEIQgpgJAlSDNIBKsSlKYKlWNUIDFqEISDJIhCAUoQhACEIQGSQFKhAKgJUIBQhzkiEAiQIQgBIhCAEIQgESbUIQCHNZFCEAIQhAf/9k=',\n  firsname: 'Maria',\n  lastNmae: 'Curie',\n  office_position: 'General Secretary',\n  bio: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \\'Content here, content here\\', making it look like readable English.',\n  year_group: '2014/2015'\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>Do not provide Date with the request. Date is provided by default by the api.</p>",
    "filename": "api/routes/executive.js",
    "groupTitle": "Executive"
  },
  {
    "type": "delete",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/executives/{id}",
    "title": "Delete Executive",
    "name": "DeleteExecutive",
    "group": "Executive",
    "version": "1.0.0",
    "description": "<p>To delete an object, you need to provide the unique id of the object.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Returns success if the delete was successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Returns true if the delete was successful.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "success response",
          "content": "{\n   message:\"success\",\n   status: \"true\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Error",
            "description": "<p>An error occured while adding the Executive.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response: an error occured while deleting the Executive.",
          "content": "\n{\n  message: \"an error occured while adding the Executive\",\n  error: \"the type  of error that occured\",\n  status: \"false\"\n}",
          "type": "object"
        }
      ]
    },
    "filename": "api/routes/executive.js",
    "groupTitle": "Executive"
  },
  {
    "type": "put",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/executives/{id}",
    "title": "Edit Executive",
    "name": "EditExecutive",
    "group": "Executive",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "executives": [
          {
            "group": "executives",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The Executive's  unique id</p>"
          },
          {
            "group": "executives",
            "type": "object",
            "optional": false,
            "field": "Executive",
            "description": "<ul> <li>The new Executive to be replaced</li> </ul>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example Data The new Executive",
          "content": "\n{\n  name:\"GRAB FOR KEEPS\",\n  banner :\"images/grabs\",\n  theme:\"You chance to grab your life time partner\",\n  start_date:\"30-04-2018\",\n  end_date:  \"31-04-2018\",\n  createdAt: \"24-04-2018\",\n  updatedAt: \"30-30-2018\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>To update a Executive, please provide the new bible_study in a json object and parse it to the api.</p>",
    "filename": "api/routes/executive.js",
    "groupTitle": "Executive"
  },
  {
    "type": "get",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/executives/{id}",
    "title": "Retrieve  Executive information by Id",
    "description": "<p>Get information for only one Executive using the bible_study id.This routes will always return one object if found else it will return an object of Executive (i.e one object containing several Executive).</p>",
    "name": "GetExecutiveById",
    "group": "Executive",
    "version": "1.0.1",
    "examples": [
      {
        "title": "Get Executive By Id",
        "content": "https://apostlite-mongo.herokuapp.com/api/v1/executives/{id}\n\nGet one record with the ID that has been provided. If not found, the api returns with failure.",
        "type": "js"
      },
      {
        "title": "Get Executive With wing",
        "content": "https://apostlite-mongo.herokuapp.com/api/v1/executives/{id}?include=wing\n\nGet one record with the wing details pre-populated with the request. When found, the api\nreturns the Executive details and the wing details in the same request.",
        "type": "js"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Example data on success for Executive",
          "content": "{\n  \"executive\": {\n      \"_id\": \"5c67bdefaf9fa35230f0503e\",\n      \"profile_photo\": \"https://pbs.twimg.com/profile_images/1055795133185179651/HMBnFwQ0_400x400.jpg\",\n      \"firsname\": \"Samira\",\n     \"lastName\": \"Bawumia\",\n     \"office_position\": \"General Secretary\",\n     \"bio\": \"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.\",\n     \"year_group\": \"2014/2015\",\n  }\n}",
          "type": "json"
        },
        {
          "title": "Example data for Executive With wing",
          "content": "\"executive\": {\n      \"_id\": \"5c67bdefaf9fa35230f0503e\",\n      \"profile_photo\": \"https://pbs.twimg.com/profile_images/1055795133185179651/HMBnFwQ0_400x400.jpg\",\n      \"firsname\": \"Samira\",\n     \"lastName\": \"Bawumia\",\n     \"office_position\": \"General Secretary\",\n     \"bio\": \"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.\",\n     \"year_group\": \"2014/2015\",\n       \"wing\": {\n           \"createdAt\": \"2019-02-04T10:29:47.508Z\",\n           \"_id\": \"5c58141bb0c18a26af9fa678\",\n           \"name\": \"Webteam\",\n          \"slogan\": \"testing\",\n          \"description\": \"testiing\",\n          \"logo\": \"\",\n       },\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "wing",
            "description": "<p>This is an object containing the details of the wing that owns the Executive. If the Executive does not belong to any wing, wing is 'null'</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DatabaseError",
            "description": "<p>An error occured while fetching the records from the database</p>"
          }
        ]
      }
    },
    "filename": "api/routes/executive.js",
    "groupTitle": "Executive"
  },
  {
    "type": "get",
    "url": "https://nupsg-apostlite.herokuapp.com/api/v1/executives",
    "title": "Retrieve all executives",
    "description": "<p>When found, the api will return every executives record found in the database. If you want to retrieve only specific items, add query parameters to the api request. You can obtain old executives items by specifying the limit, last id and adding the order=asc This allows you to retrieve executives items up to the limit created before the ID that has been specified.</p>",
    "name": "GetNews",
    "group": "Executive",
    "version": "1.0.1",
    "parameter": {
      "fields": {
        "limit": [
          {
            "group": "limit",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>(optional query param  This is the amount of records that should be returned by the api. It defaults to 100 and the minimum limit that you can parse is 1.</p>"
          }
        ],
        "order": [
          {
            "group": "order",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>(optional query param) This parameter tells the api to sort the records in ascending or descending order. Use {asc} for ascending and {des} for descending</p> <p>When not provided the order defaults to ascending.</p>"
          }
        ],
        "last_id": [
          {
            "group": "last_id",
            "type": "Object_Id",
            "optional": false,
            "field": "last_id",
            "description": "<p>The id of the last item in the previous search result. When you add the last_id option, the api starts the search from the last_id that you provide.</p> <p>You can use the {last_id} with {asc} or {des}. When you use {asc} the api starts from the {last_id} you have provided and search for items created earlier than the last_id you have provided. Wehn you use {des} the api starts from the {last_id} you have provided and search for items created after the last_id that you have provided.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Get executives By Asc",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/executives?order=asc\n         Retrieves all items from the database in ascending order.",
        "type": "js"
      },
      {
        "title": "Get executives By Des",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/executives?order=des\n         The request retrieves all items from the database in descending order.",
        "type": "js"
      },
      {
        "title": "Get executives By Limit",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/executives?limit=20\n         The request retrieves only the first twenty items from the database in ascending order.",
        "type": "js"
      },
      {
        "title": "Get executives By Last_id",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/executives?limit=20&last_id=de23ab32009a2b&order=asc\n         The request retrieves only the first twenty items created before the {last_id} that you have provided.",
        "type": "js"
      },
      {
        "title": "Get executives With options",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/executives?limit=20&last_id=de23ab32009a2b&order=des\n         The request retrieves only the first twenty items created after the {last_id} that you have provided.",
        "type": "js"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>The Headline or Title for the executives</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>The actual content of the executives</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "createdAt",
            "description": "<p>The date when the executives was created</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>The date for the most current update of the executives.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example data on success",
          "content": "\n\n{\n \"executives\": [\n     {\n         \"_id\": \"5c67bdefaf9fa35230f0503e\",\n         \"profile_photo\": \"https://pbs.twimg.com/profile_images/1055795133185179651/HMBnFwQ0_400x400.jpg\",\n         \"firsname\": \"Samira\",\n         \"lastNmae\": \"Bawumia\",\n         \"office_position\": \"General Secretary\",\n         \"bio\": \"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.\",\n         \"year_group\": \"2014/2015\",\n         \"__v\": 0\n     }\n ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/executive.js",
    "groupTitle": "Executive"
  },
  {
    "type": "post",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/news",
    "title": "Create News",
    "name": "CreateNews",
    "group": "News",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "bible_Study",
            "description": "<p>The News to create</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Sample-Request:",
          "content": "\n{\n  title :\"SCC SUNDAY\", *\n content :\"Coming sunday is SCC sunday.There will be no service at CCB.Thank you\",\n\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>Do not provide Date with the request. Date is provided by default by the api.</p>",
    "filename": "api/routes/news.js",
    "groupTitle": "News"
  },
  {
    "type": "delete",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/news/{id}",
    "title": "Delete News",
    "name": "DeleteNews",
    "group": "News",
    "version": "1.0.0",
    "description": "<p>To delete an object, you need to provide the unique id of the object.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Returns success if the delete was successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Returns true if the delete was successful.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "success response",
          "content": "{\n     message:\"success\",\n     status: \"true\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Error",
            "description": "<p>An error occured while adding the News.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response: an error occured while deleting the news.",
          "content": "\n{\n   message : \"an error occured while adding the news\",\n   error   : \"the type  of error that occured\",\n   status  : \"false\"\n}",
          "type": "object"
        }
      ]
    },
    "filename": "api/routes/news.js",
    "groupTitle": "News"
  },
  {
    "type": "put",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/news/{id}",
    "title": "Edit News",
    "name": "EditNews",
    "group": "News",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "News": [
          {
            "group": "News",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The News' unique id</p>"
          },
          {
            "group": "News",
            "type": "object",
            "optional": false,
            "field": "bible_study",
            "description": "<ul> <li>The new news to be replaced</li> </ul>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example Data The new news",
          "content": "\n{\n   title :\"Something to describe the Bible study\",\n   content:\"Walking in a spirit filled life. Life that will cause you to impact in the lives of other. That is what the bible teaches and encourages.\",\n\n }",
          "type": "json"
        }
      ]
    },
    "description": "<p>To update a News, please provide the new bible_study in a json object and parse it to the api.</p>",
    "filename": "api/routes/news.js",
    "groupTitle": "News"
  },
  {
    "type": "get",
    "url": "https://nupsg-apostlite.herokuapp.com/api/v1/news",
    "title": "Retrieve all News",
    "description": "<p>When found, the api will return every news record found in the database. If you want to retrieve only specific items, add query parameters to the api request. You can obtain old news items by specifying the limit, last id and adding the order=asc This allows you to retrieve news items up to the limit created before the ID that has been specified.</p>",
    "name": "GetNews",
    "group": "News",
    "version": "1.0.1",
    "parameter": {
      "fields": {
        "limit": [
          {
            "group": "limit",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>(optional query param  This is the amount of records that should be returned by the api. It defaults to 100 and the minimum limit that you can parse is 1.</p>"
          }
        ],
        "order": [
          {
            "group": "order",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>(optional query param) This parameter tells the api to sort the records in ascending or descending order. Use {asc} for ascending and {des} for descending</p> <p>When not provided the order defaults to ascending.</p>"
          }
        ],
        "last_id": [
          {
            "group": "last_id",
            "type": "Object_Id",
            "optional": false,
            "field": "last_id",
            "description": "<p>The id of the last item in the previous search result. When you add the last_id option, the api starts the search from the last_id that you provide.</p> <p>You can use the {last_id} with {asc} or {des}. When you use {asc} the api starts from the {last_id} you have provided and search for items created earlier than the last_id you have provided. Wehn you use {des} the api starts from the {last_id} you have provided and search for items created after the last_id that you have provided.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Get News By Asc",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/news?order=asc\n         Retrieves all items from the database in ascending order.",
        "type": "js"
      },
      {
        "title": "Get News By Des",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/news?order=des\n         The request retrieves all items from the database in descending order.",
        "type": "js"
      },
      {
        "title": "Get News By Limit",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/news?limit=20\n         The request retrieves only the first twenty items from the database in ascending order.",
        "type": "js"
      },
      {
        "title": "Get News By Last_id",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/news?limit=20&last_id=de23ab32009a2b&order=asc\n         The request retrieves only the first twenty items created before the {last_id} that you have provided.",
        "type": "js"
      },
      {
        "title": "Get News With options",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/news?limit=20&last_id=de23ab32009a2b&order=des\n         The request retrieves only the first twenty items created after the {last_id} that you have provided.",
        "type": "js"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>The Headline or Title for the news</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>The actual content of the news</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "createdAt",
            "description": "<p>The date when the news was created</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>The date for the most current update of the news.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example data on success",
          "content": "{\n message     : \"success\",\n {\n   title     : \"Leavers' service\",\n   content   : \"You are being encourged to attend the leavers' service this sunday at ccb. come and be blessed.\", *\n   createdAt : \"24-04-2018\",\n   updatedAt : \"30-30-2018\"\n },\n statusCode  : 200\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/news.js",
    "groupTitle": "News"
  },
  {
    "type": "get",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/news/{id}",
    "title": "Retrieve  news information by Id",
    "description": "<p>Get information for only one news using the news id.This routes will always return one object if found else it will return an object of news (i.e one object containing several news).</p>",
    "name": "GetNewsById",
    "group": "News",
    "version": "1.0.0",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DatabaseError",
            "description": "<p>An error occured while fetching the records from the database</p>"
          }
        ]
      }
    },
    "filename": "api/routes/news.js",
    "groupTitle": "News"
  },
  {
    "type": "post",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/programs",
    "title": "Create Program",
    "name": "CreateProgram",
    "group": "Program",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "bible_Study",
            "description": "<p>The Program to create</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Sample-Request:",
          "content": "\n{\n  name:\"GRAB FOR KEEPS\",\n  banner :\"images/grabs\",\n  theme:\"You chance to grab your life time partner\",\n  start_date:\"30-04-2018\",\n  end_date:  \"31-04-2018\",\n  createdAt: \"24-04-2018\",\n  updatedAt: \"30-30-2018\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>Do not provide Date with the request. Date is provided by default by the api.</p>",
    "filename": "api/routes/programs.js",
    "groupTitle": "Program"
  },
  {
    "type": "delete",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/programs/{id}",
    "title": "Delete Program",
    "name": "DeleteProgram",
    "group": "Program",
    "version": "1.0.0",
    "description": "<p>To delete an object, you need to provide the unique id of the object.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Returns success if the delete was successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Returns true if the delete was successful.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "success response",
          "content": "{\n   message:\"success\",\n   status: \"true\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Error",
            "description": "<p>An error occured while adding the Program.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response: an error occured while deleting the program.",
          "content": "\n{\n  message: \"an error occured while adding the program\",\n  error: \"the type  of error that occured\",\n  status: \"false\"\n}",
          "type": "object"
        }
      ]
    },
    "filename": "api/routes/programs.js",
    "groupTitle": "Program"
  },
  {
    "type": "put",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/programs/{id}",
    "title": "Edit Program",
    "name": "EditProgram",
    "group": "Program",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "programs": [
          {
            "group": "programs",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The program's  unique id</p>"
          },
          {
            "group": "programs",
            "type": "object",
            "optional": false,
            "field": "program",
            "description": "<ul> <li>The new program to be replaced</li> </ul>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example Data The new program",
          "content": "\n{\n  name:\"GRAB FOR KEEPS\",\n  banner :\"images/grabs\",\n  theme:\"You chance to grab your life time partner\",\n  start_date:\"30-04-2018\",\n  end_date:  \"31-04-2018\",\n  createdAt: \"24-04-2018\",\n  updatedAt: \"30-30-2018\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>To update a program, please provide the new bible_study in a json object and parse it to the api.</p>",
    "filename": "api/routes/programs.js",
    "groupTitle": "Program"
  },
  {
    "type": "get",
    "url": "https://nupsg-apostlite.herokuapp.com/api/v1/programs",
    "title": "Retrieve all programs",
    "description": "<p>When found, the api will return every programs record found in the database. If you want to retrieve only specific items, add query parameters to the api request. You can obtain old programs items by specifying the limit, last id and adding the order=asc This allows you to retrieve programs items up to the limit created before the ID that has been specified.</p>",
    "name": "GetNews",
    "group": "Program",
    "version": "1.0.1",
    "parameter": {
      "fields": {
        "limit": [
          {
            "group": "limit",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>(optional query param  This is the amount of records that should be returned by the api. It defaults to 100 and the minimum limit that you can parse is 1.</p>"
          }
        ],
        "order": [
          {
            "group": "order",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>(optional query param) This parameter tells the api to sort the records in ascending or descending order. Use {asc} for ascending and {des} for descending</p> <p>When not provided the order defaults to ascending.</p>"
          }
        ],
        "last_id": [
          {
            "group": "last_id",
            "type": "Object_Id",
            "optional": false,
            "field": "last_id",
            "description": "<p>The id of the last item in the previous search result. When you add the last_id option, the api starts the search from the last_id that you provide.</p> <p>You can use the {last_id} with {asc} or {des}. When you use {asc} the api starts from the {last_id} you have provided and search for items created earlier than the last_id you have provided. Wehn you use {des} the api starts from the {last_id} you have provided and search for items created after the last_id that you have provided.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Get programs By Asc",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/programs?order=asc\n         Retrieves all items from the database in ascending order.",
        "type": "js"
      },
      {
        "title": "Get programs By Des",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/programs?order=des\n         The request retrieves all items from the database in descending order.",
        "type": "js"
      },
      {
        "title": "Get programs By Limit",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/programs?limit=20\n         The request retrieves only the first twenty items from the database in ascending order.",
        "type": "js"
      },
      {
        "title": "Get programs By Last_id",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/programs?limit=20&last_id=de23ab32009a2b&order=asc\n         The request retrieves only the first twenty items created before the {last_id} that you have provided.",
        "type": "js"
      },
      {
        "title": "Get programs With options",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/programs?limit=20&last_id=de23ab32009a2b&order=des\n         The request retrieves only the first twenty items created after the {last_id} that you have provided.",
        "type": "js"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>The Headline or Title for the programs</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>The actual content of the programs</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "createdAt",
            "description": "<p>The date when the programs was created</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>The date for the most current update of the programs.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example data on success",
          "content": "{\n {\n   name       :\"GRAB FOR KEEPS\",\n   banner     :\"images/grabs\",\n   theme      :\"You chance to grab your life time partner\",\n   start_date :\"30-04-2018\",\n   end_date   :  \"31-04-2018\",\n   createdAt  : \"24-04-2018\",\n   updatedAt  : \"30-30-2018\"\n },\n{\n   name:\"LEAVERS DINNER\",\n   banner :\"images/leavers\",\n   theme:\"CREATING A LASTING MEMOERY\",\n   start_date:\"30-04-2018\",\n   end_date:  \"31-04-2018\",\n   createdAt: \"24-04-2018\",\n   updatedAt: \"30-30-2018\"\n }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/programs.js",
    "groupTitle": "Program"
  },
  {
    "type": "get",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/programs/{id}",
    "title": "Retrieve  program information by Id",
    "description": "<p>Get information for only one program using the bible_study id.This routes will always return one object if found else it will return an object of program (i.e one object containing several program).</p>",
    "name": "GetProgramById",
    "group": "Program",
    "version": "1.0.1",
    "examples": [
      {
        "title": "Get Program By Id",
        "content": "https://apostlite-mongo.herokuapp.com/api/v1/programs/{id}\n\nGet one record with the ID that has been provided. If not found, the api returns with failure.",
        "type": "js"
      },
      {
        "title": "Get Program With Owner",
        "content": "https://apostlite-mongo.herokuapp.com/api/v1/programs/{id}?include=owner\n\nGet one record with the owner details pre-populated with the request. When found, the api\nreturns the program details and the owner details in the same request.",
        "type": "js"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Example data on success for Program",
          "content": "program\": {\n \"createdAt\": \"2019-02-04T14:56:10.580Z\",\n \"_id\": \"5c58528abb880351007e6618\",\n \"name\": \"Watch Night\",\n \"theme\": \"All are invited for the first watch night service\",\n \"end_date\": \"2019-02-04T14:56:10.580Z\",\n \"start_date\": \"2019-02-04T14:56:10.580Z\",\n \"updatedAt\":\"2019-02-04T14:56:10.580Z\",\n \"owner\": \"5c58141bb0c18a26af9fa678\",\n}",
          "type": "json"
        },
        {
          "title": "Example data for Program With Owner",
          "content": "\"program\": {\n       \"createdAt\": \"2019-02-04T14:56:10.580Z\",\n       \"_id\": \"5c58528abb880351007e6618\",\n       \"name\": \"Watch Night\",\n       \"theme\": \"All are invited for the first watch night service\",\n       \"end_date\": \"2019-02-04T10:29:47.508Z\",\n       \"start_date\": \"2019-02-04T10:29:47.508Z\",\n       \"updatedAt\": \"2019-02-04T10:29:47.508Z\",\n       \"owner\": {\n           \"createdAt\": \"2019-02-04T10:29:47.508Z\",\n           \"_id\": \"5c58141bb0c18a26af9fa678\",\n           \"name\": \"Webteam\",\n          \"slogan\": \"testing\",\n          \"description\": \"testiing\",\n          \"logo\": \"\",\n       },\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "owner",
            "description": "<p>This is an object containing the details of the wing that owns the program. If the program does not belong to any wing, owner is 'null'</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DatabaseError",
            "description": "<p>An error occured while fetching the records from the database</p>"
          }
        ]
      }
    },
    "filename": "api/routes/programs.js",
    "groupTitle": "Program"
  },
  {
    "type": "post",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/sermons",
    "title": "Create Sermon",
    "name": "CreateSermon",
    "group": "Sermon",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "bible_Study",
            "description": "<p>The Sermon to create</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Sample-Request:",
          "content": "\n{\n  title:\"Leavers' service\",\n  minister:\"Rev. Dr. J.E.T\",\n  scriptures':[\n        \"Genesis 1v4\", \"Mathew 6:31\" , \"Mathew 5 v 8\"\n    ],\n  message :\"You are being encourged to attend the service' on sunday at ccb. come and be blessed.\",\n  banner:\"images/sermon/1.jpg\"\n  createdAt:\"24-04-2018\",\n  updatedAt:\"30-30-2018\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>Do not provide Date with the request. Date is provided by default by the api.</p>",
    "filename": "api/routes/sermon.js",
    "groupTitle": "Sermon"
  },
  {
    "type": "delete",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/sermons/{id}",
    "title": "Delete Sermon",
    "name": "DeleteSermon",
    "group": "Sermon",
    "version": "1.0.0",
    "description": "<p>To delete an object, you need to provide the unique id of the object.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Returns success if the delete was successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Returns true if the delete was successful.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "success response",
          "content": "{\n   message:\"success\",\n   status : \"true\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "An",
            "description": "<p>error occured while adding the Sermon.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response: an error occured while deleting the sermon.",
          "content": "\n{\n   message: \"an error occured while adding the sermon\",\n   error  : \"the type  of error that occured\",\n   status : \"false\"\n}",
          "type": "object"
        }
      ]
    },
    "filename": "api/routes/sermon.js",
    "groupTitle": "Sermon"
  },
  {
    "type": "put",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/sermons/{id}",
    "title": "Edit Sermon",
    "name": "EditSermon",
    "group": "Sermon",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Sermon": [
          {
            "group": "Sermon",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The Sermon's unique id</p>"
          },
          {
            "group": "Sermon",
            "type": "object",
            "optional": false,
            "field": "bible_study",
            "description": "<ul> <li>The new sermon to be replaced</li> </ul>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example Data The new sermon",
          "content": "\n{\n   title:\"Leavers' service\",\n   minister:\"Rev. Dr. J.E.T\",\n   scriptures:[\n         \"Genesis 1v4\", \"Mathew 6:31\" , \"Mathew 5 v 8\"\n     ],\n   message :\"You are being encourged to attend the service' on sunday at ccb. come and be blessed.\",\n   banner:\"images/sermon/1.jpg\"\n\n }",
          "type": "json"
        }
      ]
    },
    "description": "<p>To update a Sermon, please provide the new bible_study in a json object and parse it to the api.</p>",
    "filename": "api/routes/sermon.js",
    "groupTitle": "Sermon"
  },
  {
    "type": "get",
    "url": "https://nupsg-apostlite.herokuapp.com/api/v1/sermons",
    "title": "Retrieve all sermons",
    "description": "<p>When found, the api will return every sermons record found in the database. If you want to retrieve only specific items, add query parameters to the api request. You can obtain old sermons items by specifying the limit, last id and adding the order=asc This allows you to retrieve sermons items up to the limit created before the ID that has been specified.</p>",
    "name": "GetSermon",
    "group": "Sermon",
    "version": "1.0.1",
    "parameter": {
      "fields": {
        "limit": [
          {
            "group": "limit",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>(optional query param  This is the amount of records that should be returned by the api. It defaults to 100 and the minimum limit that you can parse is 1.</p>"
          }
        ],
        "order": [
          {
            "group": "order",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>(optional query param) This parameter tells the api to sort the records in ascending or descending order. Use {asc} for ascending and {des} for descending</p> <p>When not provided the order defaults to ascending.</p>"
          }
        ],
        "last_id": [
          {
            "group": "last_id",
            "type": "Object_Id",
            "optional": false,
            "field": "last_id",
            "description": "<p>The id of the last item in the previous search result. When you add the last_id option, the api starts the search from the last_id that you provide.</p> <p>You can use the {last_id} with {asc} or {des}. When you use {asc} the api starts from the {last_id} you have provided and search for items created earlier than the last_id you have provided. Wehn you use {des} the api starts from the {last_id} you have provided and search for items created after the last_id that you have provided.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Get sermons By Asc",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/sermons?order=asc\n         Retrieves all items from the database in ascending order.",
        "type": "js"
      },
      {
        "title": "Get sermons By Des",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/sermons?order=des\n         The request retrieves all items from the database in descending order.",
        "type": "js"
      },
      {
        "title": "Get sermons By Limit",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/sermons?limit=20\n         The request retrieves only the first twenty items from the database in ascending order.",
        "type": "js"
      },
      {
        "title": "Get sermons By Last_id",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/sermons?limit=20&last_id=de23ab32009a2b&order=asc\n         The request retrieves only the first twenty items created before the {last_id} that you have provided.",
        "type": "js"
      },
      {
        "title": "Get sermons With options",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/sermons?limit=20&last_id=de23ab32009a2b&order=des\n         The request retrieves only the first twenty items created after the {last_id} that you have provided.",
        "type": "js"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>The Headline or Title for the sermons</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>The actual content of the sermons</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "createdAt",
            "description": "<p>The date when the sermons was created</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>The date for the most current update of the sermons.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example data on success",
          "content": "{\n {\n   title:\"Leavers' service\",\n   minister:\"Rev. Dr. J.E.T\",\n   scriptures:[\n         \"Genesis 1v4\", \"Mathew 6:31\" , \"Mathew 5 v 8\"\n     ],\n   message :\"You are being encourged to attend the service' on sunday at ccb. come and be blessed.\",\n   banner:\"images/sermon/1.jpg\"\n   createdAt:\"24-04-2018\",\n   updatedAt:\"30-30-2018\"\n }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/sermon.js",
    "groupTitle": "Sermon"
  },
  {
    "type": "get",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/sermons/{id}",
    "title": "Retrieve  sermon information by Id",
    "description": "<p>Get information for only one wing using the sermon id.This routes will always return one object if found else it will return an object of sermon (i.e one object containing several sermon).</p>",
    "name": "GetSermon",
    "group": "Sermon",
    "version": "1.0.0",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DatabaseError",
            "description": "<p>An error occured while fetching the records from the database</p>"
          }
        ]
      }
    },
    "filename": "api/routes/sermon.js",
    "groupTitle": "Sermon"
  },
  {
    "type": "post",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/wings",
    "title": "Create a new wing",
    "name": "CreateWing",
    "group": "Wings",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "wing",
            "description": "<p>The wing to create</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "name": [
          {
            "group": "name",
            "type": "String",
            "optional": false,
            "field": "Name",
            "description": "<p>of the wing</p>"
          }
        ],
        "enail": [
          {
            "group": "enail",
            "type": "string",
            "optional": false,
            "field": "The",
            "description": "<p>email for the wing</p>"
          }
        ],
        "slogan": [
          {
            "group": "slogan",
            "type": "string",
            "optional": false,
            "field": "Slogan",
            "description": "<p>of the wing</p>"
          }
        ],
        "description": [
          {
            "group": "description",
            "type": "string",
            "optional": false,
            "field": "Description",
            "description": "<p>of the wing</p>"
          }
        ],
        "meeting": [
          {
            "group": "meeting",
            "type": "object",
            "optional": false,
            "field": "Includes",
            "description": "<p>everything about the wings meeting</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example meeting object",
          "content": "meeting: {\n  day: {\n    type: String\n  },\n\n  time: {\n    type: String\n  },\n\n  venue: {\n    type: String\n  }",
          "type": "json"
        },
        {
          "title": "Sample-Request:",
          "content": "{\n   name: \"name of the wing\",\n   email: \"example@email.com\"\n   slogan: \"slogan of the wing\",\n   description: \"write something to describe the wing\",\n   logo: \"{file}\"\n   meeting: {\n     day: \"Monday\"\n     time: \"7:00pm\"\n     venue: \"Parade Grounds\"\n   }\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>Do not provide Date with the request. Date is provided by default by the api.</p>",
    "version": "0.0.0",
    "filename": "api/routes/wings.js",
    "groupTitle": "Wings"
  },
  {
    "type": "delete",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/wings/{id}",
    "title": "Delete a wing",
    "name": "DeleteWing",
    "group": "Wings",
    "version": "1.0.0",
    "description": "<p>To delete an object, you need to provide the unique id of the object.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Returns success if the delete was successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Returns true if the delete was successful. { message:&quot;success&quot;, status: &quot;true&quot; }</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "An",
            "description": "<p>error occured while adding the wing.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response: an error occured while adding the wing.",
          "content": "\n{\n   message: \"an error occured while adding the wing\",\n   error: \"the type  of error that occured\",\n   status: \"false\"\n}",
          "type": "object"
        }
      ]
    },
    "filename": "api/routes/wings.js",
    "groupTitle": "Wings"
  },
  {
    "type": "put",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/wings/{id}",
    "title": "Edit a wing",
    "name": "EditWing",
    "group": "Wings",
    "parameter": {
      "fields": {
        "Wings": [
          {
            "group": "Wings",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Wing unique id</p>"
          },
          {
            "group": "Wings",
            "type": "object",
            "optional": false,
            "field": "wing",
            "description": "<ul> <li>The new wing to be replaced</li> </ul>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example Data The new wing",
          "content": "\n{\n   name: \"name of the wing\",\n   slogan: \"slogan of the wing\",\n   description:\"write something to describe the wing\",\n   logo:\"an image or logo for the wing\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>To update a wing, please provide the new wing in a json object and parse it to the api.</p>",
    "version": "0.0.0",
    "filename": "api/routes/wings.js",
    "groupTitle": "Wings"
  },
  {
    "type": "get",
    "url": "https://nupsg-apostlite.herokuapp.com/api/v1/wings",
    "title": "Retrieve all wings",
    "description": "<p>When found, the api will return every wings record found in the database. If you want to retrieve only specific items, add query parameters to the api request. You can obtain old wings items by specifying the limit, last id and adding the order=asc This allows you to retrieve wings items up to the limit created before the ID that has been specified.</p>",
    "name": "GetWing",
    "group": "Wings",
    "version": "1.0.1",
    "parameter": {
      "fields": {
        "limit": [
          {
            "group": "limit",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>(optional query param  This is the amount of records that should be returned by the api. It defaults to 100 and the minimum limit that you can parse is 1.</p>"
          }
        ],
        "order": [
          {
            "group": "order",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>(optional query param) This parameter tells the api to sort the records in ascending or descending order. Use {asc} for ascending and {des} for descending</p> <p>When not provided the order defaults to ascending.</p>"
          }
        ],
        "last_id": [
          {
            "group": "last_id",
            "type": "Object_Id",
            "optional": false,
            "field": "last_id",
            "description": "<p>The id of the last item in the previous search result. When you add the last_id option, the api starts the search from the last_id that you provide.</p> <p>You can use the {last_id} with {asc} or {des}. When you use {asc} the api starts from the {last_id} you have provided and search for items created earlier than the last_id you have provided. Wehn you use {des} the api starts from the {last_id} you have provided and search for items created after the last_id that you have provided.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Get wings By Asc",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/wings?order=asc\n         Retrieves all items from the database in ascending order.",
        "type": "js"
      },
      {
        "title": "Get wings By Des",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/wings?order=des\n         The request retrieves all items from the database in descending order.",
        "type": "js"
      },
      {
        "title": "Get wings By Limit",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/wings?limit=20\n         The request retrieves only the first twenty items from the database in ascending order.",
        "type": "js"
      },
      {
        "title": "Get wings By Last_id",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/wings?limit=20&last_id=de23ab32009a2b&order=asc\n         The request retrieves only the first twenty items created before the {last_id} that you have provided.",
        "type": "js"
      },
      {
        "title": "Get wings With options",
        "content": "https://nupsg-apostlite.herokuapp.com/api/v1/wings?limit=20&last_id=de23ab32009a2b&order=des\n         The request retrieves only the first twenty items created after the {last_id} that you have provided.",
        "type": "js"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>The Headline or Title for the wings</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>The actual content of the wings</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "createdAt",
            "description": "<p>The date when the wings was created</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>The date for the most current update of the wings.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example data on success",
          "content": "{\n {\n   name :\"Organazing\",\n   slogan:\"E just dey biii\",\n   description :\"This is the central organizing wing of the union\",\n   logo:\"images/logo.png\",\n   createdAt:\"24-04-2018\"\n }\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/wings.js",
    "groupTitle": "Wings"
  },
  {
    "type": "get",
    "url": "https://apostlite-mongo.herokuapp.com/api/v1/wings/{id}",
    "title": "Retrieve  wing information",
    "description": "<p>Get information for only one wing using the wing id.This routes will always return one object if found else it will return an object of wings(i.e one object containing several wings).</p>",
    "name": "GetWing",
    "group": "Wings",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>The unique id of the wing you want information about.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "include",
            "description": "<p>This is provided as an optional query parameter. To fetch a wing with all executives' information embeded in the request, you have to provided this parameter.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>The name of the wings</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "slogan",
            "description": "<p>The slogan for the wings</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>The description of the wing</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "logo",
            "description": "<p>The logo of the wing</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "createAt",
            "description": "<p>The date on which the wing was created in the system</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>The date for the most current update of the wing</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example data on Success",
          "content": "{\n  \"wing\": {\n      \"executives\": [\n          \"5c67bdefaf9fa35230f0503e\",\n          \"5c67bdefaf9fa35230f050d4\"\n      ],\n      \"createdAt\": \"2019-02-16T07:34:11.486Z\",\n      \"_id\": \"5c67bcf48a621a51098c6495\",\n      \"slogan\": \"In His Service\",\n      \"name\": \"Organizing Wing\",\n      \"description\": \" this wing handles the churhc central organzing wing, web team, technical team and publicity team.\",\n      \"logo\": \"https://pngtree.com/free-logo-png\",\n      \"__v\": 0\n  },\n}",
          "type": "json"
        }
      ]
    },
    "examples": [
      {
        "title": "Get Wing By Id",
        "content": "https://apostlite-mongo.herokuapp.com/api/v1/wings/{id}\nReturns only one wing with a reference to the executives",
        "type": "js"
      },
      {
        "title": "Get Wing with executives",
        "content": "https://apostlite-mongo.herokuapp.com/api/v1/wings/{id}\nReturns one wing with all executives information included",
        "type": "js"
      },
      {
        "title": "Example data on success",
        "content": "{\n    \"wing\": {\n        \"executives\": [\n            {\n                \"_id\": \"5c67bdefaf9fa35230f0503e\",\n                \"profile_photo\": \"https://pbs.twimg.com/profile_images/1055795133185179651/HMBnFwQ0_400x400.jpg\",\n                \"firsname\": \"Samira\",\n                \"lastNmae\": \"Bawumia\",\n                \"office_position\": \"General Secretary\",\n                \"bio\": \"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.\",\n                \"year_group\": \"2014/2015\",\n                \"__v\": 0\n            },\n            {\n                \"_id\": \"5c67bdefaf9fa35230f0503e\",\n                \"profile_photo\": \"https://pbs.twimg.com/profile_images/1055795133185179651/HMBnFwQ0_400x400.jpg\",\n                \"firsname\": \"Marie\",\n                \"lastNmae\": \"Currie\",\n                \"office_position\": \"President\",\n                \"bio\": \"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.\",\n                \"year_group\": \"2014/2015\",\n                \"__v\": 0\n            }\n        ],\n        \"createdAt\": \"2019-02-16T07:34:11.486Z\",\n        \"_id\": \"5c67bcf48a621a51098c6495\",\n        \"slogan\": \"In His Service\",\n        \"name\": \"Organizing Wing\",\n        \"description\": \" this wing handles the churhc central organzing wing, web team, technical team and publicity team.\",\n        \"logo\": \"https://pngtree.com/free-logo-png\",\n        \"__v\": 0\n    },\n}",
        "type": "json"
      }
    ],
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DatabaseError",
            "description": "<p>An error occured while fetching the records from the database</p>"
          }
        ]
      }
    },
    "filename": "api/routes/wings.js",
    "groupTitle": "Wings"
  },
  {
    "type": "get",
    "url": "https://nupsg-apostlite.herokuapp.com/api/v1/books",
    "title": "",
    "version": "0.0.0",
    "filename": "api/routes/book.js",
    "group": "_home_ebadjei_sources_node_apostlite_mongo_api_routes_book_js",
    "groupTitle": "_home_ebadjei_sources_node_apostlite_mongo_api_routes_book_js",
    "name": "GetHttpsNupsgApostliteHerokuappComApiV1Books"
  }
] });
